# Lambda Wars

Clone this repository with [GIT](https://git-scm.com/) into your Steam commons folder:

```sh
cd C:\<path-to-steam>\steamapps\common
git clone https://gitlab.com/Sandern/lambda-wars.git lambdawars
```

Next add a non steam shortcut to `lambdawars.exe`. Add launch parameters as desired, for example:

`-windowed -noborder -dev -console`.
