#!/usr/bin/env python3
"""
Builds the Linux depot, containing the game binaries for Linux (including dedicated server).
"""
import os
import argparse
from shutil import copyfile

from common import setup_depot_dest, generate_depot_manifest


def build_linux_depot(args):
    depot_dest = setup_depot_dest(args.dest, args.depotid)

    copy_files(args.src, depot_dest)

    generate_depot_manifest(args.dest, args.depotid)


def copy_files(src_path, depot_dest_path):
    """
    Copies all Linux files to destination path
    """
    print('Copying files from "%s" to "%s"...' % (src_path, depot_dest_path))
    for root, folders, files in os.walk(src_path):
        for file in files:
            path = os.path.join(root, file)
            if os.path.splitext(path)[1] not in ['.so', '.so.0', '.sh']:
                continue

            file_rel_path = os.path.relpath(path, src_path)

            file_dest_path = os.path.join(depot_dest_path, file_rel_path)
            if not os.path.exists(os.path.dirname(file_dest_path)):
                os.makedirs(os.path.dirname(file_dest_path))

            copyfile(path, file_dest_path)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Lambda Wars Build Linux depot build script')
    parser.add_argument('--src', help='Root game directory of Lambda Wars', type=str)
    parser.add_argument('--dest', help='Folder containing depots and scripts. The depot itself is output in a '
                                       'sub folder with the id of the depot', type=str)
    parser.add_argument('--depotid', help='Depot id', type=int)
    args = parser.parse_args()

    build_linux_depot(args)
