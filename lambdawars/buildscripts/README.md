# Introduction
Contains scripts for building the depots of Lambda Wars.
The depots are split up in content and OS specific depots.

# Creating the depots
To create the common depot run:

    python ./build_common_depot.py --src="path/to/game/dir/lambdawars" --dest="path/to/depots" --depotid=1

To create the Windows binaries depot:

    python ./build_win_depot.py --src="path/to/game/dir/lambdawars" --dest="path/to/depots" --depotid=2

To create the Linux binaries depot:

    python ./build_linux_depot.py --src="path/to/game/dir/lambdawars" --dest="path/to/depots" --depotid=3

To create the Mac binaries depot:

    python ./build_mac_depot.py --src="path/to/game/dir/lambdawars" --dest="path/to/depots" --depotid=4
