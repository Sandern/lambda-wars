import os
from shutil import rmtree


def setup_depot_dest(depot_folder, depotid):
    """ Setups depot destination folder, ensuring folders exist and are empty.
        Returns the destination path.
    """
    # Ensure the folders exist
    if not os.path.exists(depot_folder):
        os.mkdir(depot_folder)

    depot_dest = os.path.join(depot_folder, str(depotid))
    if os.path.exists(depot_dest):
        print('Cleaning %s' % depot_dest)
        rmtree(depot_dest)

    # Return the destination path
    return depot_dest


depot_build_template = '''"DepotBuildConfig"
{
    // Set your assigned depot ID here
    "DepotID" "%(depotid)d"

    // Set a root for all content.
    // All relative paths specified below (LocalPath in FileMapping entries, and FileExclusion paths)
    // will be resolved relative to this root.
    // If you don't define ContentRoot, then it will be assumed to be
    // the location of this script file, which probably isn't what you want
    "ContentRoot"	"%(depot_root)s"

    // include all files recursively
    "FileMapping"
    {
        // This can be a full path, or a path relative to ContentRoot
        "LocalPath" "*"

        // This is a path relative to the install folder of your game
        "DepotPath" "."

        // If LocalPath contains wildcards, setting this means that all
        // matching files within subdirectories of LocalPath will also
        // be included.
        "recursive" "1"
    }

    // This can be a full path, or a path relative to ContentRoot
    "FileExclusion" "*.pdb" // Exclude symbol files
    "FileExclusion" "*.pyc" // Exclude pyc files
    "FileExclusion" "*.pyo" // Exclude pyc files
    "FileExclusion" "*.mdmp" // Exclude mdmp files (crash on running "exit" from build exec script...)
    "FileExclusion" "__pycache__" // Exclude pycache folders
}'''


def generate_depot_manifest(depots_folder, depotid):
    path = os.path.join(depots_folder, 'depot_build_%d.vdf' % depotid)
    depot_root = os.path.join(depots_folder, str(depotid))
    with open(path, 'w') as fp:
        fp.write(depot_build_template % {'depot_root': depot_root, 'depotid': depotid})


app_build_template = '''"appbuild"
{
    "appid" "270370"
    "desc" "Lambda Wars GO BUILD commit %(commit)s build %(build_number)s. DON'T deploy to default!" // description for this build
    "buildoutput" "output_go" // build output folder for .log, .csm & .csd files, relative to location of this file
    "contentroot" "." // root content folder, relative to location of this file
    "setlive"   "lwgo" // branch to set live after successful build, non if empty
    "preview" "0" // to enable preview builds
    "local" ""  // set to flie path of local content server 
    
    "depots"
    {
        "270371" "depot_build_270371.vdf"
        "270373" "depot_build_270373.vdf"
    }
}'''

def generate_app_build_manifest(
    depots_folder,
    build_number,
    commit,
):
    with open(os.path.join(depots_folder, 'app_build_270370.vdf'), 'w') as fp:
        fp.write(app_build_template % {
            'build_number': build_number,
            'commit': commit or '(not specified)',
        })
