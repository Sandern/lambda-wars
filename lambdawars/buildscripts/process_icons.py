import os
import re

for target_folder in ['./python/core', './python/asw', './python/tf2', './python/wars_game', './python/hl1', './python/dota', './python/keeper', './python/l4d']:
    for root, folders, files in os.walk(os.path.join('..', target_folder)):
        for filename in files:
            path = os.path.join(root, filename)
            if os.path.splitext(path)[1] != '.py':
                continue

            with open(path) as fp:
                content = fp.read()

            new_content = re.sub(r'(image_name\s*=\s*[\'|"].*)\.vmt([\'|"])', r'\1\2', content)

            new_content = re.sub(r'(image_name\s*=\s*[\'|"])vgui/(.*[\'|"])', r'\1\2', new_content)
            new_content = re.sub(r'(image_name\s*=\s*[\'|"])VGUI/(.*[\'|"])', r'\1\2', new_content)

            if content != new_content:
                print('path %s changed' % path)
                with open(path, 'w') as fp:
                    fp.write(new_content)

