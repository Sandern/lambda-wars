#!/usr/bin/env python3
"""
Builds the common depot, containing the game assets.
"""
import os
import argparse
from shutil import copytree, ignore_patterns, rmtree
import subprocess
from datetime import datetime

from common import setup_depot_dest, generate_depot_manifest, generate_app_build_manifest


def build_common_depot(args):
    depot_dest = setup_depot_dest(args.dest, args.depotid)

    copy_files(args.src, depot_dest)
    pack_files(args.src, args.dest, depot_dest)

    update_steam_inf(depot_dest, args.build_number)

    generate_depot_manifest(args.dest, args.depotid)
    generate_app_build_manifest(args.dest, args.build_number, args.commit)


def copy_files(src_path, depot_dest_path):
    """
    Copies all common files from a clean game repository.
    This is basically al files minus any platform specific binary files.
    """
    print('Copying files from "%s" to "%s"...' % (src_path, depot_dest_path))
    copytree(src_path, depot_dest_path,
             ignore=ignore_patterns(
                 # Ignore git history
                 '.git',
                 # Ignore these build scripts
                 'buildscripts',
                 # Ignore sources
                 'mapsrc',
                 'modelsrc',
                 # General files and extensions to ignore
                 '.gitignore',
                 '*.pyc',
                 # Windows
                 '*.dll', '*.exe',
                 # Linux
                 '*.so', 'so.0', '*.sh',
                 # Mac
                 '*.dylib'
             ))


def pack_files(src_path, depots_path, depot_dest_path):
    """
    Packs files into VPK files.

    https://developer.valvesoftware.com/wiki/VPK

    @param source path containing full game repository. Needed for vpk.exe.
    @param depots_path folder containing depots and work files
    @param depot_dest_path path to which depot files are copied

    """

    game_dir = os.path.join(depot_dest_path, 'lambdawars')

    # Create pak script
    pak_files_bat_content = '''
    PATH=PATH;"%(src_path)s/bin/"
    cd %(game_dir)s
    del pak*.vpk
    vpk.exe -M a pak01 "@./paklist.txt"
    ''' % {
        'src_path': src_path,
        'depots_path': depots_path,
        'depot_dest_path': depot_dest_path,
        'game_dir': game_dir,
    }

    pak_files_cmd = os.path.join(game_dir, 'pakfiles.bat')
    with open(pak_files_cmd, 'w') as fp:
        fp.write(pak_files_bat_content)

    # Create pak list
    pak_list_path = os.path.join(game_dir, 'paklist.txt')

    # List of complete folders to be packed
    pak_paths = [
        os.path.join(game_dir, 'materials'),
        os.path.join(game_dir, 'models'),
        os.path.join(game_dir, 'particles'),
        os.path.join(game_dir, 'sound'),
        os.path.join(game_dir, 'python'),
    ]

    # List of additional files
    pak_files = [
        os.path.join(game_dir, 'scripts/HudLayout.res'),
    ]

    # Add all .res files from resource folder
    for root, dirs, files in os.walk(os.path.join(game_dir, 'resource')):
        for file in files:
            path = os.path.join(root, file)
            if os.path.splitext(path)[1] != '.res':
                continue
            pak_files.append(path)

    with open(pak_list_path, 'w') as fp:
        # Walk through folders
        for folder in pak_paths:
            for root, dirs, files in os.walk(folder):
                for name in files:
                    fp.write('%s\n' % os.path.relpath(os.path.join(root, name), game_dir))

        for path in pak_files:
            fp.write('%s\n' % os.path.relpath(path, game_dir))

    # Create vpk
    print('Creating vpk pak files...')
    subprocess.call([pak_files_cmd])

    # Cleanup scripts
    os.remove(pak_list_path)
    os.remove(pak_files_cmd)

    # Cleanup packed folders
    print('Removing packed folders and files...')
    for folder in pak_paths:
        rmtree(folder)

    # Clean individual packed files
    for path in pak_files:
        os.remove(path)


def update_steam_inf(depot_dest_path, build_number):
    """
    Update steam.inf with the build number.

    :param depot_dest_path:
    :param build_number:
    :return:
    """
    keys = set()
    steam_inf_path = os.path.join(depot_dest_path, 'lambdawars/steam.inf')

    now = datetime.now()

    with open(steam_inf_path, 'r', encoding='UTF-8') as fp:
        content = fp.readlines()

    new_content = []
    for line in content:
        key, value = line.strip().split('=')
        keys.add(key)
        if key == 'ClientVersion' or key == 'ServerVersion':
            value = str(build_number)
        elif key == 'PatchVersion':
            value = '1.0.0.%s' % str(build_number)
        elif key == 'VersionDate':
            value = now.strftime('%b %d %Y')
        elif key == 'VersionTime':
            value = now.strftime('%I:%M:%S')
        elif key == 'SourceRevision':
            value = os.environ.get('GIT_COMMIT', '0')

        new_content.append('%s=%s' % (key, value))

    expected_keys = {'ClientVersion', 'ServerVersion', 'PatchVersion', 'VersionDate', 'SourceRevision'}
    missing_keys = expected_keys - keys
    if missing_keys:
        raise Exception('Could not find a number of keys: %s' % missing_keys)

    with open(steam_inf_path, 'w', encoding='UTF-8') as fp:
        fp.write('\n'.join(new_content))


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Lambda Wars Build common depot build script')
    parser.add_argument('--src', help='Root game directory of Lambda Wars', type=str)
    parser.add_argument('--dest', help='Folder containing depots and scripts. The depot itself is output in a '
                                       'sub folder with the id of the depot', type=str)
    parser.add_argument('--depotid', help='Depot id', type=int)
    parser.add_argument('--build_number', help='Build number for steam.inf', type=int, default=1)
    parser.add_argument('--commit', help='Git Commit', type=str, default='')

    args = parser.parse_args()

    build_common_depot(args)
