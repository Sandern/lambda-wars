from core.weapons import WeaponFlamer as BaseClass, VECTOR_CONE_5DEGREES
from entities import entity

@entity('wars_weapon_flamer', networked=True)
class WarsWeaponFlamer(BaseClass): 
    clientclassname = 'wars_weapon_flamer'
    
    def __init__(self):
        super().__init__()

        self.bulletspread = VECTOR_CONE_5DEGREES

    class AttackPrimary(BaseClass.AttackRange):
        damage = 3
        minrange = 0.0
        maxrange = 320.0
        attackspeed = 0.1 # Fire rate
        usesbursts = True
        minburst = 3
        maxburst = 5
        minresttime = 0.4
        maxresttime = 0.6
        attributes = ['fire']
