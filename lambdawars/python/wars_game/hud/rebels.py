from core.hud import HudInfo
from core.ui.hud import CefHudPlayerPanel


class CefRebelsHud(CefHudPlayerPanel):
    name = 'rebels'
    classidentifier = 'RebelsHud'


class CefRebelsHudInfo(HudInfo):
    name = 'rebels_hud'
    cefcls = CefRebelsHud
