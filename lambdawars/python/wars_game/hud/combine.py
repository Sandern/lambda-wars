from core.hud import HudInfo
from core.ui.hud import CefHudPlayerPanel, Minimap, Abilities, Units, ResourcesAndPopulation, Notifier, Groups

combine_minimap_width = 158
combine_minimap_tall = 152


class CombineMinimap(Minimap):
    coords = {
        'x': round(0.065 * combine_minimap_width),
        'y': round((480 - combine_minimap_tall) + (0.1658 * combine_minimap_tall)),
        'w': round(0.7682 * combine_minimap_width),
        'h': round(0.7682 * combine_minimap_tall),
    }


class CefCombineHud(CefHudPlayerPanel):
    name = 'combine'
    classidentifier = 'CombineHud'

    def CreatePanels(self):
        return {
            'minimap': CombineMinimap('minimap'),
            'abilities': Abilities('abilities'),
            'units': Units('units'),
            'resources_population': ResourcesAndPopulation('resources_popcap'),
            'notifier': Notifier('notifier'),
            'groups': Groups('groups'),
        }


class CefCombineHudInfo(HudInfo):
    name = 'combine_hud'
    cefcls = CefCombineHud
