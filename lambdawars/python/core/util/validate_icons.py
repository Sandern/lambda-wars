import filesystem
from core.abilities.info import dbabilities
from gameinterface import concommand

@concommand('validate_icons')
def CCValidateIcons(args):
    for ability in dbabilities.values():
        if not ability.image_name:
            continue

        icon_path = 'ui/assets/icons/%s.png' % ability.image_name
        if not filesystem.FileExists(icon_path):
            print('Icon missing for ability: %s\n\tPath "%s" does not exist.' % (ability.name, icon_path))
