from entities import entity
if isserver:
    from entities import CHL2WarsPlayer as BaseClass
else:
    from entities import C_HL2WarsPlayer as BaseClass


@entity('player', networked=True)
class WarsPlayer(BaseClass):
    owner_number = 0

    def __init__(self):
        super().__init__()

        # Stack that keeps track of which sub menu is active
        self.active_hud_abilities = []

    """ Player entity class. Most functionality is in CHL2WarsPlayer, but the Python code extends it a little bit. """
    def OnChangeOwnerNumber(self, oldownernumber):
        """ Called when the ownernumber changes.
            Caches owner number.
        """
        super().OnChangeOwnerNumber(oldownernumber)

        self.owner_number = self.GetOwnerNumber()
