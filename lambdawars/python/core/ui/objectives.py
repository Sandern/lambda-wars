from cef import CefPanel
from core.signals import prelevelinit


class CefObjectivesPanel(CefPanel):
    classidentifier = 'Objectives'
    
    # The last builded sorted list of objective information for the hud
    objective_info = []
    # The last received list of valid objective entities
    objective_events = []
    
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        
        prelevelinit.connect(self.OnPreLevelInit)
        
    def OnLoaded(self):
        super().OnLoaded()
        
        self.RebuildObjectiveList(self.objective_events)
        
    def OnRemove(self):
        super().OnRemove()
        
        prelevelinit.disconnect(self.OnPreLevelInit)
        
    def OnPreLevelInit(self, **kwargs):
        """
        Resets the objective list on level init.
        """
        self.objective_info = []
        if self.isloaded:
            self.UpdateObjectiveList()
        
    def RebuildObjectiveList(self, objective_events):
        """
        Rebuilds the objective list from scratch from the passed objective entities list.
        """
        self.objective_events = objective_events
        
        # Build info list
        self.objective_info = []
        for ent in objective_events:
            if not ent or not ent.visible:
                continue
                
            self.objective_info.append(ent.BuildObjectInfo())
            
        # Sort on priority...
        self.objective_info = sorted(self.objective_info, key=lambda v: v['priority'], reverse=True)
        
        # Do the update
        self.UpdateObjectiveList()
        
    def UpdateObjectiveList(self):
        """
        Calls the javascript part to rebuild the html list of objectives.
        """
        # Got anything to display?
        if not self.objective_info:
            self.visible = False
            return
            
        self.visible = True
        
        self.Invoke(self.component, "rebuildObjectiveList", [self.objective_info])


objectivespanel = CefObjectivesPanel('objectivespanel')
