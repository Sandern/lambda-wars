"""
Created on 04.07.2013
Message box that can be displayed with a hammer entity.<br>
You can define the text and speech bubble style.

Update 11.08.2013
- Added support for looking/unlooking the "Continue" button.

@author: ProgSys
"""
from cef import CefPanel, jsbind
from gameinterface import PlayerInfo, engine
from core.signals import postlevelshutdown


class CefMessagePanel(CefPanel):
    classidentifier = 'MessageBox'
    
    msg_box_name = ''
    
    def SetupFunctions(self):
        self.CreateFunction('onClose', False)
        self.CreateFunction('hide', False)
    
    def OnLoaded(self):
        super().OnLoaded()
        self.visible = False
        postlevelshutdown.connect(self.OnPostLevelShutdown)
        
    def OnRemove(self):
        super().OnRemove()
        
        postlevelshutdown.disconnect(self.OnPostLevelShutdown)
        
    def OnPostLevelShutdown(self, **kwargs):
        """ Resets the objective list on level init. """
        if not self.isloaded:
            return
        self.visible = False
        self.msg_box_name = ''
        
    def LockMessageBox(self, msg_box_name):
        if self.msg_box_name == msg_box_name:
            self.Invoke(self.component, "lockMessageBox")
        
    def UnlockMessageBox(self, msg_box_name):
        if self.msg_box_name == msg_box_name:
            self.Invoke(self.component, "unlockMessageBox")

    def SmoothCloseMessageBox(self, msg_box_name):
        if self.msg_box_name == msg_box_name:
            self.Invoke(self.component, "smoothCloseMessageBox")

    def ShowMessageBox(self, msg_box_name, text):
        self.visible = True
        self.Invoke(self.component, "setText", [text])
        self.msg_box_name = msg_box_name
       
    def HideMessageBox(self, msg_box_name):
        self.visible = False
        self.msg_box_name = ''

    @jsbind()
    def onClose(self, methodargs):
        """ Called from javascript on pressing the close button. """
        self.visible = False
        if not self.msg_box_name:
            PrintWarning('CefMessagePanel.onClose: Not displaying any current message box!\n')
            return
        # Tell msg box entity we closed
        engine.ClientCommand('wars_close_msgbox %s' % self.msg_box_name)
        self.msg_box_name = ''
        
          
messageboxpanel = CefMessagePanel('messageboxpanel')
