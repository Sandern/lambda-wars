"""
Controller code for top menu bar in the game view port.
"""
from cef import CefPanel, jsbind
from gamerules import gamerules


class CefTopBar(CefPanel):
    """ Generic header bar for buttons, to be filled in by the gamerules.
        Mostly used by Sandbox mode.
    """
    name = 'topbar'
    classidentifier = 'HeaderBar'
    
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        
        self.handlers = {}

    def OnLoaded(self):
        super().OnLoaded()

        self.handlers.clear()

        if hasattr(gamerules, 'SetupTopBar'):
            gamerules.SetupTopBar()
        
        self.visible = True
        
    def InsertButton(self, name, text='', order=0, handler=None, floatright=False):
        self.Invoke(self.component, "insertButton", [name, text, order, floatright])
        self.handlers[name] = handler

    @jsbind()
    def onButtonPressed(self, methodargs):
        button_name = methodargs[0]
        handler = self.handlers.get(button_name, None)
        if handler:
            handler(self)
