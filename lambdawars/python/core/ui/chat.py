from cef import CefPanel
from core.signals import receiveclientchat, startclientchat, gameui_inputlanguage_changed
from playermgr import dbplayers, OWNER_LAST
from entities import PlayerResource
import gameui


class CefChatPanel(CefPanel):
    classidentifier = 'Chat'
    
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        
        startclientchat.connect(self.StartClientChat)
        receiveclientchat.connect(self.OnPrintChat)
        gameui_inputlanguage_changed.connect(self.OnInputLanguageChanged)
    
    def OnRemove(self):
        super().OnRemove()
        
        startclientchat.disconnect(self.StartClientChat)
        receiveclientchat.disconnect(self.OnPrintChat)
        gameui_inputlanguage_changed.disconnect(self.OnInputLanguageChanged)

    def OnLoaded(self):
        super().OnLoaded()

        self.visible = True
        
    def StartClientChat(self, mode, *args, **kwargs):
        self.Invoke(self.component, "startChat", [mode, gameui.GetCurrentKeyboardLangId()])
        
    def OnPrintChat(self, playerindex, filter, msg, *args, **kwargs):
        if playerindex == 0:
            self.Invoke(self.component, "printChatNotification", [msg])
        else:
            say = msg.partition(':')
            owner = PlayerResource().GetOwnerNumber(playerindex) if PlayerResource() else OWNER_LAST
            c = dbplayers[owner].color
            playercolor = 'rgb(%d, %d, %d)' % (c.r(), c.g(), c.b())
            playername = say[0]
            msg = say[2]
        
            self.Invoke(self.component, "printChat", [playername, playercolor, msg])
    
    def OnInputLanguageChanged(self, *args, **kwargs):
        self.Invoke(self.component, "updateChatPlaceholder", [gameui.GetCurrentKeyboardLangId()])
        
    
chatpanel = CefChatPanel('chatpanel')
