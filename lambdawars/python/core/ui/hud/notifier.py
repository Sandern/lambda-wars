from cef import ReactComponent, jsbind
from core.signals import player_notification
from core.notifications import JumpToNotification
from _vlocalize import localize


class Notifier(ReactComponent):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        player_notification.connect(self.OnNotification)

    def OnNotification(self, notification, **kwargs):
        # Only care about message notifications here
        if not notification.message:
            return

        message = notification.message
        if message and message[0] == '#':
            message = localize.Find(message)

        self.Invoke(self.component, 'insertNotification', [{
            'id': notification.notification_id,
            'message': message,
            'icon': notification.icon,
            'color': notification.messagecolor,
        }])

    def Remove(self):
        super().Remove()

        player_notification.disconnect(self.OnNotification)

    @jsbind(hascallback=False)
    def jumpToNotification(self, methodargs):
        JumpToNotification(methodargs[0])
