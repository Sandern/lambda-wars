from cef import ReactComponent, jsbind
from entities import C_HL2WarsPlayer
from gameinterface import engine

from core.signals import FireSignalRobust, selectionchanged, refreshhud, units_hud_update_unit


class Units(ReactComponent):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        selectionchanged.connect(self.OnSelectionChanged)
        refreshhud.connect(self.OnRefreshHud)
        units_hud_update_unit.connect(self.OnUnitUpdate)

    def OnRemove(self):
        super().OnRemove()

        selectionchanged.disconnect(self.OnSelectionChanged)
        refreshhud.disconnect(self.OnRefreshHud)
        units_hud_update_unit.disconnect(self.OnUnitUpdate)

    def OnLoad(self):
        super().OnLoaded()

        self.FullUnitsUpdate()

    def OnRefreshHud(self, **kwargs):
        self.FullUnitsUpdate()

    def OnSelectionChanged(self, player, **kwargs):
        self.FullUnitsUpdate()

    def OnUnitUpdate(self, update, **kwargs):
        """ Called when there is a partial update. """
        self.Invoke(self.component, 'unitUpdate', [update])

    def FullUnitsUpdate(self):
        self.Invoke(self.component, 'fullUpdate', [self.BuildFullUnitsUpdate()])

    def BuildFullUnitsUpdate(self):
        state = {
            'type': 'multi_selection',
            'units': []
        }

        player = C_HL2WarsPlayer.GetLocalHL2WarsPlayer()
        if not player:
            return state

        selection = player.GetSelection()

        if len(selection) == 1:
            selection[0].UpdateUnitPanelClass()
            state['type'] = selection[0].unit_panel_type
        elif len(selection) > 1:
            state['type'] = 'multi_selection'

        for unit in selection:
            ui_info = {}

            unit.FillInfoForUnitsUI(ui_info)

            state['units'].append(ui_info)

        return state

    # General unit selection methods
    @jsbind()
    def selectUnitType(self, methodargs):
        """ Changes the active unit type displayed in the abilities hud panel. """
        player = C_HL2WarsPlayer.GetLocalHL2WarsPlayer()
        player.SetSelectedUnitType(methodargs[0])
        FireSignalRobust(refreshhud)

    @jsbind()
    def selectUnitsOfTypeInSelection(self, methodargs):
        """ Selects all units of the provided type in the selection, discarding the other units. """
        player = C_HL2WarsPlayer.GetLocalHL2WarsPlayer()
        unit_type = methodargs[0]

        selection = list(player.GetSelection())
        player.ClearSelection(False)  # Do not trigger on selection changed, since we do that below too already.
        engine.ServerCommand("player_clearselection")
        player.MakeSelection([unit for unit in selection if unit.GetUnitType() == unit_type])

    @jsbind()
    def removeUnitAtIndex(self, methodargs):
        """ Removes unit at index from slot. """
        player = C_HL2WarsPlayer.GetLocalHL2WarsPlayer()
        unit = player.GetSelection()[methodargs[0]]
        player.RemoveUnit(unit)
        engine.ServerCommand('player_removeunit %d' % (unit.entindex()))

    @jsbind()
    def removeUnitsOfTypeFromSelection(self, methodargs):
        """ Removes all units with the specified type from selection. """
        player = C_HL2WarsPlayer.GetLocalHL2WarsPlayer()
        unit_type = methodargs[0]

        selection = list(player.GetSelection())
        player.ClearSelection(False)  # Do not trigger on selection changed, since we do that below too already.
        engine.ServerCommand("player_clearselection")
        player.MakeSelection([unit for unit in selection if unit.GetUnitType() != unit_type])

    @jsbind()
    def onlySelectUnitAtIndex(self, methodargs):
        """ Only selects unit at given index, clearing selection from all other units. """
        player = C_HL2WarsPlayer.GetLocalHL2WarsPlayer()
        unit = player.GetSelection()[methodargs[0]]
        player.ClearSelection(False) # Do not trigger on selection changed, since we do that below too already.
        engine.ServerCommand('player_clearselection')
        player.AddUnit(unit)
        engine.ServerCommand('player_addunit %d' % unit.entindex())

    # Garrison building API
    @jsbind()
    def ungarrisionUnit(self, methodargs):
        engine.ServerCommand('player_ungarrison_unit %d' % (methodargs[0]))

    # Factory building api
    @jsbind()
    def removeUnitFromQueue(self, methodargs):
        engine.ClientCommand('player_queue %d ' % methodargs[0])
