

if isclient:
    from .hud_player import CefHudPlayerPanel
    from .minimap import Minimap
    from .abilities import Abilities
    from .population import Population
    from .units import Units
    from .resources_population import ResourcesAndPopulation
    from .notifier import Notifier
    from .groups import Groups
