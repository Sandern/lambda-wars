from cef import ReactComponent

from core.factions import GetFactionInfo
from core.resources import resources, resourcecaps, GetResourceInfo
from core.signals import resourceset
from entities import C_HL2WarsPlayer


class Resources(ReactComponent):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        resourceset.connect(self.OnSetResource)

    def OnRemove(self):
        super().OnRemove()

        resourceset.disconnect(self.OnSetResource)

    def OnLoaded(self):
        super().OnLoaded()

        # Force an update on load
        self.RefreshResources()

    def OnSetResource(self, ownernumber, type, amount, **kwargs):
        self.RefreshResources()

    def RefreshResources(self):
        player = C_HL2WarsPlayer.GetLocalHL2WarsPlayer()
        if not player:
            return

        owner = player.GetOwnerNumber()

        faction = player.GetFaction()
        info = GetFactionInfo(faction)
        if not info or not info.resources:
            return

        ui_resources_update = []

        for i, r in enumerate(info.resources):
            if i >= 3:
                break

            ui_resources_update.append({})

            resource = info.resources[i]

            res_info = GetResourceInfo(resource)
            if not res_info:
                continue

            resource_data = {
                'name': res_info.name,
                'amount': resources[owner][resource],
                'icon': res_info.icon,
                'displayName': res_info.displayname,
            }
            if res_info.iscapped:
                resource_data['cap'] = resourcecaps[owner][resource]

            ui_resources_update[i] = resource_data

        # Update UI
        self.Invoke(self.component, 'update', [{
            'resources': ui_resources_update
        }])
