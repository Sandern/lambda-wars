from cef import ReactComponent, jsbind

from core.signals import groupchanged
from entities import C_HL2WarsPlayer
from gameinterface import engine


class Groups(ReactComponent):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        groupchanged.connect(self.OnGroupChanged)

    def OnRemove(self):
        super().OnRemove()

        groupchanged.disconnect(self.OnGroupChanged)

    def OnLoaded(self):
        super().OnLoaded()

        self.FullUpdate()

    def FullUpdate(self):
        player = C_HL2WarsPlayer.GetLocalHL2WarsPlayer()

        groups = []
        for group_num in range(0, player.MaxGroups()):
            groups.append(self.BuildGroupInfo(player, group_num))

        self.Invoke(self.component, 'updateGroups', [groups])

    def OnGroupChanged(self, player, group, **kwargs):
        self.Invoke(self.component, 'updateGroup', [group, self.BuildGroupInfo(player, group)])

    def BuildGroupInfo(self, player, group):
        num_units = player.CountGroup(group)
        return {
            'unitInfo': self.GetGroupUnitInfo(player, group) if num_units > 0 else None
        }

    def GetGroupUnitInfo(self, player, group):
        ui_info = {}
        unit = player.GetGroupUnit(group, 0)
        unit.FillInfoForUnitsUI(ui_info)
        return ui_info

    @jsbind(hascallback=False)
    def selectGroup(self, methodargs):
        player = C_HL2WarsPlayer.GetLocalHL2WarsPlayer()
        group_idx = methodargs[0]
        player.SelectGroup(group_idx)
        engine.ServerCommand('select_group %d ' % group_idx)

    @jsbind(hascallback=False)
    def jumpToGroup(self, methodargs):
        player = C_HL2WarsPlayer.GetLocalHL2WarsPlayer()
        group_idx = methodargs[0]
        player.JumpToGroup(group_idx)
