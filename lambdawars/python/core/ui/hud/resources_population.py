from cef import ReactComponent, jsbind

from cef import ReactComponent

from core.factions import GetFactionInfo
from core.resources import resources, resourcecaps, GetResourceInfo
from core.units import unitpopulationcount, GetMaxPopulation
from entities import C_HL2WarsPlayer


class ResourcesAndPopulation(ReactComponent):
    last_pop = None
    last_pop_cap = None

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.resources = [{}, {}, {}]

    def OnLoaded(self):
        super().OnLoaded()

        self.RegisterTickSignal(0.1)
        # Reset to force an update on reload
        self.resources = [{}, {}, {}]
        self.last_pop = None
        self.last_pop_cap = None

    def OnTick(self):
        super().OnTick()

        player = C_HL2WarsPlayer.GetLocalHL2WarsPlayer()
        if not player:
            return

        owner = player.GetOwnerNumber()

        faction = player.GetFaction()
        info = GetFactionInfo(faction)
        if not info or not info.resources:
            return

        changed = False

        # Check resources
        # TODO: make event based to detect changes
        for i, r in enumerate(info.resources):
            if i >= 3:
                break

            resource = info.resources[i]

            res_info = GetResourceInfo(resource)
            if not res_info:
                continue

            resource_data = {
                'name': res_info.name,
                'amount': resources[owner][resource],
                'icon': res_info.icon,
                'displayName': res_info.displayname,
            }
            if res_info.iscapped:
                resource_data['cap'] = resourcecaps[owner][resource]

            if set(resource_data.items()) != set(self.resources[i].items()):
                self.resources[i] = resource_data
                changed = True

        # Check pop cap
        ownernumber = player.GetOwnerNumber()
        pop = unitpopulationcount[ownernumber]
        pop_cap = GetMaxPopulation(ownernumber)

        # Don't set updates to html ui when nothing changed.
        # TODO consider making this event based!
        if pop != self.last_pop or pop_cap != self.last_pop_cap:
            self.last_pop = pop
            self.last_pop_cap = pop_cap
            changed = True

        if changed:
            self.Invoke(self.component, 'update', [{
                'resources': self.resources,
                'pop': pop,
                'popCap': pop_cap,
            }])
