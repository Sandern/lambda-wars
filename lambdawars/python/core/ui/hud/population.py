from cef import ReactComponent

from entities import C_HL2WarsPlayer
from core.units import unitpopulationcount, GetMaxPopulation


class Population(ReactComponent):
    last_pop = None
    last_pop_cap = None

    def OnLoaded(self):
        super().OnLoaded()

        self.RegisterTickSignal(0.1)

        # Reset to force an update on reload
        self.last_pop = None
        self.last_pop_cap = None

    def OnTick(self):
        super().OnTick()

        player = C_HL2WarsPlayer.GetLocalHL2WarsPlayer()
        if not player:
            return

        # update amount of units
        ownernumber = player.GetOwnerNumber()
        pop = unitpopulationcount[ownernumber]
        pop_cap = GetMaxPopulation(ownernumber)

        # Don't set updates to html ui when nothing changed.
        # TODO consider making this event based!
        if pop != self.last_pop or pop_cap != self.last_pop_cap:
            self.last_pop = pop
            self.last_pop_cap = pop_cap

            self.Invoke(self.component, 'update', [{
                'pop': pop,
                'popCap': pop_cap,
            }])
