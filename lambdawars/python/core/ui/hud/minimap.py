""" Wraps game logic for minimap.
The actual minimap is implemented VGUI and a small part in HTML.
"""
from cef import ReactComponent
from gameui import GetMinimap
from sound import soundengine
import srcmgr
from core.units import unitlist
from core.signals import firedping, unitspawned, unitremoved, minimapupdateunit, playerchangedownernumber, player_notification


class Minimap(ReactComponent):
    coords = {
        'x': round(0.0693069307 * 158),
        'y': round((480 - 144) + (0.0891304348 * 144)),
        'w': round(0.782178218 * 158),
        'h': round(0.852173913 * 144),
    }

    def __init__(self, name=''):
        super().__init__(name)

        self.minimap_panel = GetMinimap()
        self.minimap_panel.ShowPanel(True)
        self.minimap_panel.SetMap(srcmgr.levelname)

        # Adjust minimap to circle
        self.minimap_panel.SetPosAndSize(
            self.coords['x'],
            self.coords['y'],
            self.coords['h'],
            self.coords['w'],
        )

        # Connect all game events
        firedping.connect(self.OnPing)
        player_notification.connect(self.OnNotification)
        unitspawned.connect(self.OnUnitSpawned)
        unitremoved.connect(self.OnUnitRemoved)
        minimapupdateunit.connect(self.OnMinimapUpdateUnit)
        playerchangedownernumber.connect(self.OnPlayerChangedOwnernumber)

        # Minimap might be created after the first units are spawned
        # Parse the existing list of units
        self.FullUnitUpdate()

    def OnRemove(self):
        # Ensure minimap is hidden and unreferenced
        self.minimap_panel.ShowPanel(False)
        self.minimap_panel = None

        firedping.disconnect(self.OnPing)
        player_notification.disconnect(self.OnNotification)
        unitspawned.disconnect(self.OnUnitSpawned)
        unitremoved.disconnect(self.OnUnitRemoved)
        minimapupdateunit.disconnect(self.OnMinimapUpdateUnit)
        playerchangedownernumber.disconnect(self.OnPlayerChangedOwnernumber)

    def FullUnitUpdate(self):
        for owner, l in unitlist.items():
            for unit in l:
                if not unit.handlesactive:
                    continue
                self.OnUnitSpawned(unit)

    def OnUnitSpawned(self, unit, **kwargs):
        info = unit.unitinfo
        if not info.minimaphalfwide or not unit.ShowOnMinimap():
            return
        self.minimap_panel.InsertEntityObject(unit, info.minimapicon, info.minimaphalfwide, info.minimaphalftall, True, True,
                                info.minimaplayer)

    def OnUnitRemoved(self, unit, **kwargs):
        # Noop in case the unit is not on the minimap
        self.minimap_panel.RemoveEntityObject(unit)

    def OnMinimapUpdateUnit(self, unit, **kwargs):
        self.OnUnitRemoved(unit)
        self.OnUnitSpawned(unit)

    def OnPlayerChangedOwnernumber(self, player, oldownernumber, **kwargs):
        self.minimap_panel.RemoveAllEntityObjects(units_only=True)
        self.FullUnitUpdate()

    def OnPing(self, pos, color, **kwargs):
        self.minimap_panel.Ping(color, pos)

        soundengine.EmitAmbientSound('ambient/alarms/warningbell1.wav', 0.8)

    def OnNotification(self, notification, **kwargs):
        if not notification.ent or not notification.minimapflashent:
            return

        self.minimap_panel.FlashEntity(notification.ent, notification.minimapflashent)
