from cef import ReactComponent, jsbind
from entities import C_HL2WarsPlayer
from gameinterface import engine
from time import time

from core.abilities import SendAbilityMenuChanged, GetAbilityInfo, ClientDoAbility, GetTechNode
from core.units import GetUnitInfo
from core.signals import selectionchanged, selected_unit_type_changed, abilitymenuchanged, refreshhud, resourceset
from core.factions import GetFactionInfo
from core.resources import GetResourceInfo
import hotkeymgr


update_slots_interval = 0.2


class Abilities(ReactComponent):
    number_of_slots = 12

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        selectionchanged.connect(self.OnSelectionChanged)
        selected_unit_type_changed.connect(self.OnSelectedUnitTypeChanged)
        abilitymenuchanged.connect(self.OnAbilityMenuChanged)
        refreshhud.connect(self.OnRefreshHud)
        resourceset.connect(self.OnRefreshHud)

        self.RegisterTickSignal(update_slots_interval)

    def OnRemove(self):
        super().OnRemove()

        selectionchanged.disconnect(self.OnSelectionChanged)
        selected_unit_type_changed.disconnect(self.OnSelectedUnitTypeChanged)
        abilitymenuchanged.disconnect(self.OnAbilityMenuChanged)
        refreshhud.disconnect(self.OnRefreshHud)
        resourceset.disconnect(self.OnRefreshHud)

    def OnSelectionChanged(self, player, **kwargs):
        self.UpdateSlots()

    def OnSelectedUnitTypeChanged(self, player, **kwargs):
        player.hudabilitiesmap = []
        SendAbilityMenuChanged()

    def OnAbilityMenuChanged(self, **kwargs):
        self.UpdateSlots()

    def OnRefreshHud(self, **kwargs):
        self.UpdateSlots()

    def UpdateSlots(self):
        """ Builds information for ability slots and updates ui component. """
        slots = self.BuildSlotsInfo()
        self.Invoke(self.component, 'updateSlots', [slots])

    def OnTick(self):
        """ Update slots at a fixed rate.

        Collecting requirements on units can be unpredictable, so currently it's hard to put into events.
        Also some events could be fired at a high rate (such as energy changes of units), which could
        make updating the slots actually slower.
        """
        self.UpdateSlots()

    def BuildSlotsInfo(self):
        slots_info = []
        for i in range(0, self.number_of_slots):
            slots_info.append({
                'key': i,
            })

        player = C_HL2WarsPlayer.GetLocalHL2WarsPlayer()
        if not player:
            return slots_info

        units_full_selection = player.GetSelection()
        unit_info = self.GetActiveUnitInfo()
        hl_min, hl_max = player.GetSelectedUnitTypeRange()
        units = units_full_selection[hl_min:hl_max]
        unit = units[0] if units else None

        # Hide everything if there is no unit type or if we selected another player's unit
        if not unit_info or not unit or not unit.CanPlayerControlUnit(player):
            return slots_info

        owner = player.owner_number
        faction_info = GetFactionInfo(player.GetFaction())

        # Retrieve the active hud abilities map
        abilities_map = player.active_hud_abilities[-1] if player.active_hud_abilities else unit_info.abilities

        for i in range(0, len(slots_info)):
            slot = slots_info[i]
            if i not in abilities_map:
                # Slot is empty
                continue

            info = unit_info.GetAbilityInfo(abilities_map[i], owner)

            tech_node = GetTechNode(info, owner)
            if not tech_node.available and not tech_node.showonunavailable:
                # Nothing in slot
                continue

            filtered_units = info.FilterSelection(info.name, units_full_selection)

            requirements = info.GetRequirementsUnits(player, units=filtered_units)

            # Fill slot information
            info.FillInfoForSlotUI(slot)

            # Hotkey for displaying in UI
            slot['hotkey'] = hotkeymgr.hotkeysystem.GetHotkeyForAbility(info, i)

            # Can we do this ability? Set enabled/disabled
            can_do_ability, recharge_complete = self.CalculateCanDoAbility(info, player, filtered_units)

            slot['canDoAbility'] = can_do_ability

            if recharge_complete:
                time_left = recharge_complete - gpGlobals.curtime

                # Translate to real time as gpGlobals.curtime is not available in UI and not usable for
                # animation purposes anyway.
                slot['rechargeStartTime'] = time() - info.rechargetime + time_left
                slot['rechargeEndTime'] = time() + time_left

            slot['autocastOn'] = self.HasUnitAutocastOn(info, filtered_units)

            # Strip available and resources from requirements, as it's shown in the UI through other means.
            slot['requirements'] = requirements - {'available', 'resources'}
            slot['costs'] = self.BuildCostsInfo(faction_info, requirements, info)
            slot['techRequirements'] = self.BuildTechRequirements(info, owner)

        return slots_info

    def GetActiveUnitInfo(self):
        player = C_HL2WarsPlayer.GetLocalHL2WarsPlayer()
        if not player:
            return None

        # Should always have unit info ( fallback is unit_unknown )
        highlight_unit_type = player.GetSelectedUnitType()
        if not highlight_unit_type:
            return None

        return GetUnitInfo(highlight_unit_type, fallback=None)

    def BuildTechRequirements(self, info, owner):
        tech_requirements = []

        for tr in info.techrequirements:
            tech_info = GetAbilityInfo(tr)
            if not tech_info:
                continue
            tech_node = GetTechNode(tech_info, owner)
            if not tech_node:
                continue
            tech_requirements.append({
                'name': tech_info.name,
                'displayName': tech_info.displayname,
                'enabled': tech_node.techenabled,
            })

        return tech_requirements

    def BuildCostsInfo(self, faction_info, requirements, info):
        costs_info = []

        costs = None

        # Prefer to show the costs that match the specified resources of the player faction
        if faction_info:
            for clist in info.costs:
                for c in clist:
                    if c[0] not in faction_info.resources:
                        break
                    costs = clist
                if costs:
                    break

        # Default to first cost set
        if not costs:
            costs = info.costs[0] if len(info.costs) > 0 else []

        for c in costs:
            res_info = GetResourceInfo(c[0])
            costs_info.append({
                'name': res_info.name,
                'amount': c[1],
                'displayName': res_info.displayname,
                'hasEnough': 'resources' not in requirements,
            })

        return costs_info

    def HasUnitAutocastOn(self, info, units):
        if not info.supportsautocast:
            return False
        for unit in units:
            if unit.abilitycheckautocast[info.uid]:
                return True
        return False

    def CalculateCanDoAbility(self, info, player, units):
        """" Calculates if at least one unit can do the ability.
        Returns soonest recharge time if not.

        @param info ability
        @param player doing ability
        @param units that can do this ability
        """
        min_recharge_complete = float('inf')

        for unit in units:
            if info.CanDoAbility(player, unit=unit):
                return True, 0.0
            if info.uid in unit.abilitynexttime:
                if unit.abilitynexttime[info.uid] < min_recharge_complete:
                    min_recharge_complete = unit.abilitynexttime[info.uid]
            else:
                min_recharge_complete = 0
        return False, min_recharge_complete

    # Bound js methods for abilities hud panel
    @jsbind()
    def doAbility(self, methodargs):
        slot_idx = int(methodargs[0])

        player = C_HL2WarsPlayer.GetLocalHL2WarsPlayer()
        hl_min, hl_max = player.GetSelectedUnitTypeRange()
        unit = player.GetUnit(hl_min)
        unit_info = unit.unitinfo

        # Retrieve the active hud abilities map
        abilities_map = player.active_hud_abilities[-1] if player.active_hud_abilities else unit_info.abilities
        info = unit_info.GetAbilityInfo(abilities_map[slot_idx], unit.owner_number)

        ClientDoAbility(player, info, unit_info.name)

    @jsbind()
    def doAbilityAlt(self, methodargs):
        slot_idx = int(methodargs[0])

        player = C_HL2WarsPlayer.GetLocalHL2WarsPlayer()
        hl_min, hl_max = player.GetSelectedUnitTypeRange()
        unit = player.GetUnit(hl_min)
        unit_info = unit.unitinfo

        # Retrieve the active hud abilities map
        abilities_map = player.active_hud_abilities[-1] if player.active_hud_abilities else unit_info.abilities
        info = unit_info.GetAbilityInfo(abilities_map[slot_idx], unit.owner_number)

        engine.ServerCommand('player_abilityalt %s' % info.name)
