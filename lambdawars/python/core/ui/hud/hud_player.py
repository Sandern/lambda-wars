from cef import CefHudPanel
from .notifier import Notifier
from .minimap import Minimap
from .abilities import Abilities
from .units import Units
from .population import Population
from .resources import Resources
from .groups import Groups


class CefHudPlayerPanel(CefHudPanel):
    def GetConfig(self):
        """ Dictionary passed as config to javascript, used for initializing. """
        config = super().GetConfig()
        config['visible'] = True
        return config

    def __init__(self, name=''):
        # Initialize sub components before main hud component so the game bindings are there
        # when the hud creates the sub components.
        self.panels = self.CreatePanels()

        super().__init__(name)

    def CreatePanels(self):
        return {
            'minimap': Minimap('minimap'),
            'abilities': Abilities('abilities'),
            'units': Units('units'),
            'population': Population('population'),
            'resources': Resources('resources'),
            'notifier': Notifier('notifier'),
            'groups': Groups('groups'),
        }

    def OnRemove(self):
        super().OnRemove()

        for panel in self.panels.values():
            panel.Remove()
