from cef import CefPanel
from gamerules import gamerules
from gameui import GetMainMenu
from steam import steamapicontext


class CefWaitingForPlayers(CefPanel):
    name = 'waitingforplayers'
    classidentifier = 'WaitingForPlayersPanel'
    wfptimeout = 0

    def BuildPlayerStatuses(self, gameplayers):
        statuses = []
        
        for gp in gameplayers:
            gp['steamid'] = str(gp['steamid'])
            statuses.append(gp)
            
        return statuses
        
    def UpdatePanel(self, wfptimeout, gameplayers):
        self.wfptimeout = wfptimeout
        
        # Fill in playername from steamid if no playername is present
        steamfriends = steamapicontext.SteamFriends()
        if steamfriends:
            for gp in gameplayers:
                steamid = gp.get('steamid', None)
                if steamid and 'playername' not in gp:
                    gp['playername'] = steamfriends.GetFriendPersonaName(steamid)
    
        hostcontent = gamerules.GetTableInfoString('hostfile')
        if hostcontent:
            if hostcontent.startswith('http://'):
                self.Invoke(self.component, "updateBanner", [hostcontent])
            else:
                self.Invoke(self.component, "updateBannerFromContent", [hostcontent])
            
        motdcontent = gamerules.GetTableInfoString('motd')
        if motdcontent:
            if motdcontent.startswith('http://'):
                self.Invoke(self.component, "updateMOTD", [motdcontent])
            else:
                self.Invoke(self.component, "updateMOTDFromContent", [motdcontent])
                
        title = 'Waiting for Players...'
                
        mainmenu = GetMainMenu()
        if mainmenu and mainmenu.gamelobby and mainmenu.gamelobby.steamidlobby:
            gamelobby = mainmenu.gamelobby
            datamodel = gamelobby.datamodel
            title = datamodel.name
        
        self.Invoke(self.component, "updatePanel", [round(self.wfptimeout - gpGlobals.curtime),
                                    title, self.BuildPlayerStatuses(gameplayers)])
