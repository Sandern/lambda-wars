from cef import CefPanel


class CefPostGamePlayers(CefPanel):
    ''' Screen shown to all players after game ended. 
    
        Contains disconnect button to leave game.
    '''
    name = 'postgame'
    classidentifier = 'PostGamePanel'

    def ShowPanel(self, winners, losers, iswinner):
        self.visible = True
        self.Invoke(self.component, "updatePanel", [winners, losers, iswinner])
        
    def HidePanel(self):
        self.visible = False
