"""
Controller code for toolbox panel in editor mode.
"""
from srcbase import IN_DUCK, KeyValues
from cef import CefPanel, jsbind
from gameinterface import engine, ConVarRef
from entities import CBasePlayer, ReadDataDesc
from input import ButtonCode
import filesystem
import os
from fields import GetField

from gamerules import gamerules
from editorsystem import EditorSystem
from _recast import RecastMgr


class CefToolbox(CefPanel):
    name = 'toolbox'
    classidentifier = 'EditorToolbox'
    
    currentmapname = '&lt;no map loaded&gt;'

    def OnLoaded(self):
        super().OnLoaded()

        self.visible = True
        
    def GetConfig(self):
        """
        Dictionary passed as config to javascript, used for initializing.

        :return:
        """
        config = super().GetConfig()
        config['currentmapname'] = self.currentmapname
        config['rootmodellist'] = self.LoadRootModelsAndFolders()
        return config
        
    def FloraKeyInput(self, down, keynum, currentbinding):
        return 1
        
    keyinputmap = {
        'flora': 'FloraKeyInput',
    }
    def KeyInput(self, down, keynum, currentbinding):
        player = CBasePlayer.GetLocalPlayer()
        if not player:
            return
        ctrldown = player.buttons & IN_DUCK
        
        if keynum == ButtonCode.KEY_DELETE:
            if down:
                # Delete selection
                engine.ServerCommand('wars_editor_delete_selection')
            return 0
        elif ctrldown and keynum == ButtonCode.KEY_C:
            if down:
                engine.ServerCommand('wars_editor_copy_selection')
            return 0
        elif ctrldown and keynum == ButtonCode.KEY_V:
            if down:
                engine.ServerCommand('wars_editor_paste_selection')
            return 0
            
        activemode = gamerules.activemode
        if activemode in self.keyinputmap:
            return getattr(self, self.keyinputmap[activemode])(down, keynum, currentbinding)
        return 1
        
    def LoadRootModelsAndFolders(self, rootpath='models/'):
        """
        Builds a list of records for tree grids.

        The returned model paths uses forward slashes, which is expected by
        the vmf file format.

        :param rootpath:
        :return:
        """
        records = []
        root_models = filesystem.ListDir(rootpath, pathid=None, wildcard='*')
        for rm in root_models:
            if rm == '.' or rm == '..':
                continue
            path = os.path.normpath(os.path.join(rootpath, rm)).replace('\\', '/')
            is_directory = filesystem.IsDirectory(path)
            if not is_directory and os.path.splitext(rm)[1] != '.mdl':
                continue
            if is_directory:
                # Add trailing separator for directories
                path += '/'
            records.append({
                'fullPath': path,
                'model': rm,
                'isFolder': is_directory,
            })
        return records

    def OnEditorSelectionChanged(self, selection):
        if not self.isloaded:
            return

        editor_selection = []
        for ent in selection:
            editor_selection.append({
                'entIndex': ent.entindex(),
            })

        self.Invoke(self.component, "editorSelectionChanged", [editor_selection])

    @jsbind()
    def setActiveMode(self, methodargs):
        """
        Changes the toolmode (for example: "select", "transform", "flora").

        :param methodargs:
        :param callbackid:
        :return:
        """
        mode = methodargs[0]
        gamerules.activemode = mode

    @jsbind(hascallback=True)
    def listModels(self, methodargs):
        modelpath = methodargs[0]
        return [self.LoadRootModelsAndFolders(modelpath)]

    @jsbind()
    def addPlaceToolAsset(self, methodargs):
        asset = methodargs[0]
        player = CBasePlayer.GetLocalPlayer()
        player.GetSingleActiveAbility().AddPlaceToolAsset(asset)
        engine.ServerCommand('wars_editor_add_pt_asset %s' % asset)

    @jsbind()
    def removePlaceToolAsset(self, methodargs):
        asset = methodargs[0]
        player = CBasePlayer.GetLocalPlayer()
        player.GetSingleActiveAbility().RemovePlaceToolAsset(asset)
        engine.ServerCommand('wars_editor_remove_pt_asset %s' % asset)

    @jsbind()
    def setPlaceToolAssets(self, methodargs):
        player = CBasePlayer.GetLocalPlayer()
        assets = methodargs[0]
        place_ability = player.GetSingleActiveAbility()

        # Clear assets first
        place_ability.ClearPlaceToolAssets()
        engine.ServerCommand('wars_editor_clear_pt_assets')

        # Select new assets
        for asset in assets:
            place_ability.AddPlaceToolAsset(asset)
            engine.ServerCommand('wars_editor_add_pt_asset %s' % asset)

    @jsbind()
    def setPlaceToolDensity(self, methodargs):
        density = methodargs[0]
        player = CBasePlayer.GetLocalPlayer()
        player.GetSingleActiveAbility().SetPlaceToolDensity(float(density))
        engine.ServerCommand('wars_editor_set_pt_density %s' % density)

    @jsbind()
    def setPlaceToolPlaceOnNavMesh(self, methodargs):
        usenavmesh = methodargs[0]
        player = CBasePlayer.GetLocalPlayer()
        player.GetSingleActiveAbility().usenavmesh = bool(usenavmesh)
        engine.ServerCommand('wars_editor_set_pt_usenavmesh %d' % (int(usenavmesh)))

    @jsbind()
    def setPlaceToolAttribute(self, methodargs):
        player = CBasePlayer.GetLocalPlayer()
        fieldname = methodargs[0]
        value = methodargs[1]
        ability = player.GetSingleActiveAbility()
        field = GetField(ability, fieldname)
        field.Set(ability, value)
        engine.ServerCommand('wars_editor_set_pt_attr %s %s' % (fieldname, value if type(value) != bool else int(value)))

    # Properties panel methods
    @jsbind(hascallback=True)
    def getProperties(self, methodargs):
        properties = {}
        for i in range(0, EditorSystem().GetNumSelected()):
            ent = EditorSystem().GetSelected(i)
            if not ent:
                continue

            data_desc = ReadDataDesc(ent)
            for key, value in data_desc.items():
                if key in properties:
                    if properties[key]['value'] != value:
                        properties[key]['value'] = '<mixed values>'
                else:
                    properties[key] = {
                        'attribute': key,
                        'value': value,
                        'type': 'string',
                    }

        return properties

    @jsbind()
    def applyProperties(self, methodargs):
        attributevalues = methodargs[0]
        attributes = KeyValues("data")
        for entry in attributevalues:
            key = entry[0]
            value = entry[1]
            attributes.SetString(key, value)

        EditorSystem().QueueCommand(EditorSystem().CreateEditCommand(attributes))

    # Nav mesh panel methods
    @jsbind(hascallback=True)
    def getMeshSettings(self, methodargs):
        settings = {}

        recast_debug_mesh = ConVarRef('recast_debug_mesh')
        recast_draw_navmesh = ConVarRef('recast_draw_navmesh')
        recast_draw_server = ConVarRef('recast_draw_server')

        settings['debug_mesh'] = recast_debug_mesh.GetString() if (not methodargs or not methodargs[0]) else methodargs[0]
        settings['draw_navmesh'] = recast_draw_navmesh.GetBool()
        settings['draw_server'] = recast_draw_server.GetBool()

        mesh = RecastMgr().GetMesh(settings['debug_mesh'])
        #print('Getting mesh %s: %s, params: %s' % (settings['debug_mesh'], mesh, methodargs))
        if mesh:
            settings['cellsize'] = mesh.cellsize
            settings['cellheight'] = mesh.cellheight
            settings['tilesize'] = mesh.tilesize
        else:
            settings['cellsize'] = -1
            settings['cellheight'] = -1
            settings['tilesize'] = -1

        return settings

    @jsbind()
    def meshSetCellSize(self, methodargs):
        meshname = methodargs[0]
        mesh = RecastMgr().GetMesh(meshname)
        if mesh:
            mesh.cellsize = float(methodargs[1])
            engine.ServerCommand('recast_mesh_setcellsize %s %f\n' % (meshname, mesh.cellsize))
        else:
            PrintWarning('meshSetCellSize: could not find mesh %s\n' % meshname)

    @jsbind()
    def meshSetCellHeight(self, methodargs):
        meshname = methodargs[0]
        mesh = RecastMgr().GetMesh(meshname)
        if mesh:
            mesh.cellheight = float(methodargs[1])
            engine.ServerCommand('recast_mesh_setcellheight %s %f\n' % (meshname, mesh.cellheight))
        else:
            PrintWarning('meshSetCellHeight: could not find mesh %s\n' % meshname)

    @jsbind()
    def meshSetTileSize(self, methodargs):
        meshname = methodargs[0]
        mesh = RecastMgr().GetMesh(meshname)
        if mesh:
            mesh.tilesize = float(methodargs[1])
            engine.ServerCommand('recast_mesh_settilesize %s %f\n' % (meshname, mesh.tilesize))
        else:
            PrintWarning('meshSetTileSize: could not find mesh %s\n' % meshname)
