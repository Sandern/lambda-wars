from _cef import *
from srcbuiltins import RegisterTickMethod, UnregisterTickMethod, IsTickMethodRegistered
from gameinterface import concommand, ConVarRef, engine, CommandLine
from kvdict import LoadFileIntoDictionaries
from gameui import GetGameUILanguage
import srcmgr

import os
import traceback
import inspect


def jsbind(objname=None, hascallback=False, manuallycallback=False):
    """ Binds a Python method to a javascript object.
    
        Args:
            objname (str): name of global javascript object to bind to

        Kwargs:
            hascallback (bool): Made callback to js method with returned parameters
            manuallycallback (bool): Indicates binding has callback, but is not automatically called.
                                     Can be used to delay the call.
    """
    def dojsbind(fn):
        setattr(fn, 'jsbound', True)
        fn.objname = objname
        fn.hascallback = hascallback
        fn.manuallycallback = manuallycallback
        return fn
    return dojsbind


class WebViewComponent(object):
    """ Component for web views, allowing js bindings to be moved out of the main
        web view class. """
    #: Default global javascript object for binding js methods
    js_object_name = None
    
    def __init__(self, webview):
        super().__init__()
        
        self.webview = webview
        
    def InitializeObjects(self):
        pass

    def OnInitializedBindings(self):
        pass
        
    def OnDestroy(self):
        pass
        
    def CreateGlobalObject(self, *args, **kwargs):
        self.webview.CreateGlobalObject(*args, **kwargs)

    def CreateFunction(self, *args, **kwargs):
        self.webview.CreateFunction(*args, **kwargs)

    def SendCallback(self, *args, **kwargs):
        self.webview.SendCallback(*args, **kwargs)

    def ExecuteJavaScript(self, *args, **kwargs):
        self.webview.ExecuteJavaScript(*args, **kwargs)

    def ExecuteJavaScriptWithResult(self, *args, **kwargs):
        self.webview.ExecuteJavaScriptWithResult(*args, **kwargs)

    def Invoke(self, *args, **kwargs):
        self.webview.Invoke(*args, **kwargs)

    def InvokeWithResult(self, *args, **kwargs):
        self.webview.InvokeWithResult(*args, **kwargs)

    def KeyInput(self, down, keynum, currentbinding):
        """ Can be overridden by component to receive game key input. """
        return 1


def GetUILanguage():
    engine_language = engine.GetUILanguage()
    if CommandLine().CheckParm('-language'):
        return CommandLine().ParmValue('-language', engine_language)
    return GetGameUILanguage() or engine_language


class WebView(CefUIBrowser):
    """ Generic web view for displaying a web page. """
    #: Name of global javascript object to which js methods of this class around using @jsbind
    js_object_name = None

    initialized_bindings = False
    
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.__js_globals = {}
        self.__js_methods = {}
        self.__components = []
        
    def OnLoadStart(self, frame):
        super().OnLoadStart(frame)
        
        if not frame.IsMain():
            return

        self.__js_globals = {}
        self.__js_methods = {}
        self.initialized_bindings = False
        
    def OnLoadEnd(self, frame, httpStatusCode):
        super().OnLoadEnd(frame, httpStatusCode)

        if not frame.IsMain():
            return
            
        # Don't clear these variables until load ended, in case it's a "fake" load (e.g. due routers)
        self.__js_globals = {}
        self.__js_methods = {}
        
        self.InitializeBindings(frame)
        
    def OnDestroy(self):
        ''' Called when the web view is completely destroyed. '''
        super().OnDestroy()
        
        for obj in self.__components:
            obj.OnDestroy()
        
    def InitializeObjects(self):
        pass
        
    def InitializeBindings(self, frame):
        # Only want to do this for the main frame
        if not frame.IsMain():
            return

        # Create global js objects
        self.InitializeObjects()
        self.InitializeComponentBindings(self)

        self.OnInitializedBindings()

        for component in self.__components:
            self.InitializeComponentBindings(component)

        self.initialized_bindings = True

        self.OnInitializedComponents()

    def AddComponent(self, component):
        self.__components.append(component)

        if self.initialized_bindings:
            self.InitializeComponentBindings(component)

    def RemoveComponent(self, component):
        self.__components.remove(component)

        # Remove
        try:
            self.InvokeWithResult(None, 'removeElement', [component.name])
        except ValueError:
            traceback.print_exc()
            return

    def InitializeComponentBindings(self, component):
        """ Initialize js functions on WebView or WebViewComponent instances. """
        component.InitializeObjects()

        for name, fn in inspect.getmembers(component, predicate=inspect.ismethod):
            if not getattr(fn, 'jsbound', False):
                continue

            # Find to which global object this method should be bound
            objname = fn.objname if fn.objname is not None else component.js_object_name
            assert objname is not None, 'js binding %s has no object to bind to' % name

            # Auto create it if it does not exists yet
            if objname not in self.__js_globals:
                self.CreateGlobalObject(objname)

            # Create and bind the method
            component.CreateFunction(fn, self.__js_globals[objname], callback=fn.hascallback,
                                     manuallycallback=fn.manuallycallback)

        component.OnInitializedBindings()
        
    def OnInitializedBindings(self):
        pass

    def OnInitializedComponents(self):
        """ Called on load end after components are created.
            Could still be the case that components are added after this.
        """
        pass
        
    def CreateGlobalObject(self, name, *args, **kwargs):
        obj = super().CreateGlobalObject(name, *args, **kwargs)
        self.__js_globals[name] = obj
        return obj
        
    def CreateFunction(self, fn, obj, callback=False, manuallycallback=False):
        jsfn = super().CreateFunction(fn.__name__, obj, callback)
        jsfn.fn = fn
        jsfn.manuallycallback = manuallycallback
        self.__js_methods[jsfn.identifier] = jsfn
        return jsfn
        
    def OnMethodCall(self, identifier, methodargs, callbackid):
        jsobj = self.__js_methods.get(identifier, None)
        if jsobj:
            fn = jsobj.fn
            if fn.manuallycallback:
                fn(methodargs, callbackid)
            elif callbackid is not None:
                callbackargs = fn(methodargs)
                self.SendCallback(callbackid, [callbackargs] if type(callbackargs) != list else callbackargs)
            else:
                fn(methodargs)
            return True
        PrintWarning('WebView.OnMethodCall: Could not find method with identifier %s\n' % identifier)
        return False

    def GetGameUITranslations(self):
        # Return the merged translations from gameui and lambdawars_ui
        translations = self.GetTranslationsFromFile('gameui')
        translations.update(self.GetTranslationsFromFile('lambdawars_ui'))
        return translations

    def GetTranslationsFromFile(self, resource_name):
        language = GetUILanguage()
        translations_file = os.path.join('resource', f'{resource_name}_{language}.txt')
        translations = LoadFileIntoDictionaries(translations_file, default={})
        if not translations:
            english_translations_file = os.path.join('resource', f'{resource_name}_english.txt')
            translations = LoadFileIntoDictionaries(english_translations_file, default={})
        return translations.get('Tokens', {})

    def KeyInput(self, down, keynum, currentbinding):
        ret = 1
        for component in self.__components:
            ret = component.KeyInput(down, keynum, currentbinding)
            if ret == 0:
                break
        return ret


class Viewport(WebView):
    htmlfile = 'local://localhost/ui/viewport/dist/index.html'

    js_object_name = 'interface'

    def __init__(self, *args, **kwargs):
        super().__init__('CefViewPort', self.htmlfile, browser_type=CEFUI_BROWSER_VIEWPORT)

        # Filter out the tab key from receiving in key events
        self.SetIgnoreTabKey(True)
        # Mouse events at positions with no alpha will be passed to the game rather than ui
        self.SetPassMouseTruIfAlphaZero(True)
        # Must enable key input through SetKeyFocus to receive key events
        self.SetRequireInputKeyFocus(True)

    def Load(self):
        self.LoadURL(self.htmlfile)
        
    def OnSizeChanged(self, newwidth, newtall):
        self.ReloadViewport()

    def ReloadViewport(self):
        self.ReloadIgnoreCache()

    def OnLoadEnd(self, frame, httpStatusCode):
        if not frame.IsMain():
            return

        self.SetMouseInputEnabled(True)
        self.SetKeyBoardInputEnabled(True)
        self.SetZPos(-10)
        self.Focus()

        # Initialize viewport before calling super to setup the viewport before component creation..
        self.Invoke(None, 'init_viewport', [self.GetGameUITranslations()])

        super().OnLoadEnd(frame, httpStatusCode)

    # Js methods
    @jsbind()
    def serverCommand(self, methodargs):
        engine.ServerCommand(methodargs[0])

    @jsbind()
    def clientCommand(self, methodargs):
        engine.ClientCommand(methodargs[0])

    @jsbind(hascallback=True)
    def gettranslations(self, methodargs):
        return self.GetGameUITranslations()

    @jsbind(hascallback=True)
    def retrieveConVarValue(self, methodargs):
        ref = ConVarRef(methodargs[0])
        return ref.GetString()

    @jsbind()
    def setCefFocus(self, methodargs):
        self.SetKeyFocus(methodargs[0])


class ReactComponent(WebViewComponent):
    #: name of the component for game - js bindings
    name = ''
    #: Reference to compnent in js
    component = None
    #: Indicates the component is mounted
    isloaded = False

    def __init__(self, name=''):
        super().__init__(viewport)

        if name:
            self.name = name

        self.js_object_name = '%s_obj' % self.name
        print('ReactComponent adding %s' % self.js_object_name)

        viewport.AddComponent(self)

    def OnLoaded(self):
        """ Called when component is mounted. """
        pass

    def Remove(self):
        self.OnRemove()

        self.UnregisterTickSignal()
        viewport.RemoveComponent(self)

    def OnRemove(self):
        pass

    # Convenient tick method
    def OnTick(self):
        pass

    def RegisterTickSignal(self, interval, looped=True):
        """" Registers a periodic tick. This calls the method OnTick for the given interval.

        Auto unregisters when the component is destroyed.

        @param interval tick interval
        @param looped by default is called over and over again until unregistered. Set looped to False to only call once.
        """
        # Ensure old tick is unregistered before registering new tick
        # This can be useful if you want to change the timing.
        self.UnregisterTickSignal()
        RegisterTickMethod(self.OnTick, interval, looped)

    def UnregisterTickSignal(self):
        if IsTickMethodRegistered(self.OnTick):
            UnregisterTickMethod(self.OnTick)

    # JS Methods
    @jsbind()
    def componentDidMount(self, methodargs):
        self.component = methodargs[0]
        self.isloaded = True
        self.OnLoaded()

    @jsbind()
    def componentWillUnmount(self, methodargs):
        self.component = methodargs[0]


class CefPanel(ReactComponent):
    _visible = False

    #: React Component registered for usage in viewport
    classidentifier = ''
    #: Configuration passed to javascript for initialization
    defaultconfig = {
        'visible': False,
    }
        
    def GetConfig(self):
        """ Dictionary passed as config to javascript, used for initializing. """
        config = dict(self.defaultconfig)
        config['name'] = self.name
        # In case reloaded, restore the visible state by default
        config['visible'] = self.visible
        return config

    def OnInitializedBindings(self):
        super().OnInitializedBindings()

        # Insert and setup
        try:
            self.InvokeWithResult(None, 'insertElement', [self.classidentifier, self.name, self.GetConfig()])
        except ValueError:
            traceback.print_exc()
            return

    @property
    def visible(self):
        return self._visible
    
    @visible.setter
    def visible(self, visible):
        if self.isloaded:
            self.Invoke(self.component, 'setVisible', [visible])
        self._visible = visible
        
    # JS Methods
    @jsbind()
    def onSetVisible(self, methodargs):
        self._visible = methodargs[0]


class CefHudPanel(CefPanel):
    def __init__(self, name=''):
        if not name and not self.name:
            name = self.__class__.__name__
        super().__init__(name)


# Create the default viewport
viewport = Viewport()


@concommand('viewport_reload')
def CCReloadViewport(args):
    viewport.ReloadViewport()


@concommand('viewport_show_devtools')
def CCViewPortShowDevTools(args):
    viewport.ShowDevTools()


@concommand('viewport_debuginfo')
def CCViewportDebugInfo(args):
    for i, e in enumerate(viewport.panels):
        print('%d Panel %s' % (i, e.name))


@concommand('viewport_run')
def CCViewportRun(args):
    viewport.ExecuteJavaScript(args.ArgS(), '')


@concommand('cef_open_window')
def CCCefOpenWindow(args):
    command = args.ArgS()
    print('command: %s' % (command))
    viewport.ExecuteJavaScript('window.open("%s");' % (command), '')
