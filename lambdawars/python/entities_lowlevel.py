""" Implements PyHandle class for from Python instantiated entities.

In general the game code will always pass entities as an entity handle, so attribute access must be as efficient as
possible.
"""
from _entities import CBaseHandle


# Map of all Python instantiated entities
# Hashed by handle integer (index + serial)
full_entities_map = {}


class PyHandle(CBaseHandle):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        # Handle index does not ever change for PyHandle
        # Saves time by not having to call into C++ code.
        # Avoid __setattr__ of PyHandle here.
        object.__setattr__(self, 'handle_index', object.__getattribute__(self, 'ToInt')())

    def Get(self):
        return full_entities_map.get(self.handle_index, None)

    def __getattribute__(self, item):
        ent = full_entities_map.get(object.__getattribute__(self, 'handle_index'), None)
        if ent and hasattr(ent, item):
            return getattr(ent, item)

        # Check for attributes of CBaseHandle or handle_index
        return super().__getattribute__(item)

    def __setattr__(self, name, value):
        """ Redirect set atrribute to entity. Fails if handle is None."""
        setattr(full_entities_map.get(object.__getattribute__(self, 'handle_index'), None), name, value)

    def __eq__(self, other):
        if isinstance(other, CBaseHandle):
            return super().__eq__(other)
        return full_entities_map.get(object.__getattribute__(self, 'handle_index'), None) is other

    def __bool__(self):
        return bool(full_entities_map.get(object.__getattribute__(self, 'handle_index'), None))

    def __str__(self):
        ent = full_entities_map.get(object.__getattribute__(self, 'handle_index'), None)
        if ent:
            return '%s(PyHandle)' % str(ent)

        return super().__str__()

    # Hashable implementation
    def __hash__(self):
        return hash(full_entities_map.get(object.__getattribute__(self, 'handle_index'), None))
