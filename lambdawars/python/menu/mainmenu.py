from .chatroom import WebChatLobby
from .gamelobby import WebGameLobby
from .settings import WebVideoSettings, WebHotKeys
from .savedgames import WebSavedGames
try:
    from .workshop import WebWorkshop
except ImportError:
    DevMsg(1, 'Update steam api')
    WebWorkshop = None

from srcbuiltins import KeyValues
from cef import WebView, jsbind, NT_DEFAULT, NT_ONLYFILEPROT
from _vlocalize import localize
from gameui import GameUICommand
from gameinterface import engine, Plat_FloatTime, ConVar, FCVAR_HIDDEN, CommandLine
from steam import (steamapicontext, NumberOfCurrentPlayersCallResult, PersonaStateChangeCallback,
                    EPersonaChange)
from entities import PlayerResource
from playermgr import MAX_PLAYERS
from core.signals import (steam_serversdisconnected, steam_serverconnectfailure, steam_serversconnected,
                          match_event, gameui_inputlanguage_changed)
import matchmaking
import filesystem
from kvdict import LoadFileIntoDictionaries
import gameui

import srcmgr
import settings

import os

connect_lobby = ConVar("connect_lobby", "", FCVAR_HIDDEN, "Sets the lobby ID to connect to on start.")


class MainMenuNumberOfCurrentPlayersCallResult(NumberOfCurrentPlayersCallResult):
    def OnNumberOfCurrentPlayers(self, data, iofailure):
        self.webview.SendCallback(self.callbackid, [data.players if (data.success and not iofailure) else None])


class MainMenuNumberOfCurrentPlayersBroadcastCallResult(NumberOfCurrentPlayersCallResult):
    def OnNumberOfCurrentPlayers(self, data, iofailure):
        # Num players from Steam updates at a lower rate, so use the chat lobby as indication if it has a higher
        # number of players
        self.webview.PushValueToStore('general:number-of-current-players',
                                        max(1, data.players if data.success and not iofailure else 1,
                                            self.webview.chatlobby.numlobbymembers))

class MainMenuPersonaStateChangeCallback(PersonaStateChangeCallback):
    def OnPersonaStateChange(self, data):
        steamuser = steamapicontext.SteamUser()
        if steamuser.GetSteamID() != data.steamid:
            return
        
        if data.changeflags & EPersonaChange.k_EPersonaChangeName or data.changeflags & EPersonaChange.k_EPersonaChangeAvatar:
            self.UpdateUserInfo()

class WebGlobalChatLobby(WebChatLobby):
    js_object_name = 'globalchat'


class CefMainMenu(WebView, MainMenuPersonaStateChangeCallback):
    levelloading = False
    
    rooturl = None

    js_object_name = 'gameui'

    chatlobby = None
    gamelobby = None
    video_settings = None
    
    def __init__(self):
        # ui/menu contains the new new main menu. Remove once porting is finished.
        navigation_behavior = NT_ONLYFILEPROT
        if CommandLine().CheckParm('-newmenu'):
            self.rooturl = CommandLine().ParmValue('-newmenu', None)
            if self.rooturl:
                # Assume developer url
                navigation_behavior = NT_DEFAULT
            else:
                self.rooturl = 'local://localhost/ui/menu/dist/'
        else:
            self.rooturl = 'local://localhost/ui/menu_next/dist/'

        WebView.__init__(self, "Main Menu", self.GetPageUrl('main-menu'), navigationbehavior=navigation_behavior)
        
        MainMenuPersonaStateChangeCallback.__init__(self)

        # Create components
        self.chatlobby = WebGlobalChatLobby(self)
        self.gamelobby = WebGameLobby(self)
        self.video_settings = WebVideoSettings(self)

        self.AddComponent(self.chatlobby)
        self.AddComponent(self.gamelobby)
        self.AddComponent(self.video_settings)
        self.AddComponent((WebHotKeys(self)))
        self.AddComponent(WebSavedGames(self))
        if WebWorkshop:
            self.AddComponent(WebWorkshop(self))
        
        steam_serversconnected.connect(self.OnSteamServersConnected)
        steam_serversdisconnected.connect(self.OnSteamServersDisconnected)
        steam_serverconnectfailure.connect(self.OnSteamServerConnectFailure)

        match_event.connect(self.OnMatchEvent)
        
        gameui_inputlanguage_changed.connect(self.OnInputLanguageChanged)
        
    def OnDestroy(self):
        super().OnDestroy()
        
        steam_serversconnected.disconnect(self.OnSteamServersConnected)
        steam_serversdisconnected.disconnect(self.OnSteamServersDisconnected)
        steam_serverconnectfailure.disconnect(self.OnSteamServerConnectFailure)

        match_event.disconnect(self.OnMatchEvent)
        
        gameui_inputlanguage_changed.disconnect(self.OnInputLanguageChanged)
        
    def OnAfterCreated(self):
        super().OnAfterCreated()
        
        self.SetMouseInputEnabled(True)

    def OnLoadError(self, frame, errorcode, errortext, failedurl):
        PrintWarning('CefMainMenu: Failed to load %s!\n\t%s:%s\n' % (failedurl, errorcode, errortext))

        if frame != self.GetMainFrame():
            return

        if failedurl != self.rooturl:
            self.GoToMainMenu()

    def OnLoadEnd(self, frame, httpStatusCode):
        if frame != self.GetMainFrame():
            return
    
        super().OnLoadEnd(frame, httpStatusCode)
        
        # self.SetVisible(True)
        self.SetMouseInputEnabled(True)
        self.SetKeyBoardInputEnabled(True)
        # self.SetUseMouseCapture(False)
        self.Focus()
        
        steamfriends = steamapicontext.SteamFriends()
        if steamfriends:
            steamfriends.SetRichPresence("status", "In Main menu")

        self.curversion = self.GetVersion()

        self.UpdateUserInfo()
        self.UpdateUserState()
        self.OnInputLanguageChanged()  # Show correct input language in chat boxes
        # if we were launched with "+connect_lobby <lobbyid>" on the command line, join that lobby immediately
        # else try if we still had an active lobby, but exited the game without leaving it (e.g. crash or something else)
        connectlobbyid = connect_lobby.GetString()
        if connectlobbyid:
            self.gamelobby.joinlobby([connectlobbyid])
            self.CallServiceMethod('gamelobbymanager', 'setStateWithApply', ['joining'])
        else:
            self.gamelobby.StartTestActiveLobby()

    def OnInitializedComponents(self):
        super().OnInitializedComponents()
        
        self.Invoke(None, 'init_mainmenu', [self.GetGameUITranslations()])
        
    def OnSteamServersConnected(self, *args, **kwargs):
        print('OnSteamServersConnected')

    def OnSteamServersDisconnected(self, *args, **kwargs):
        print('OnSteamServersDisconnected')

    def OnSteamServerConnectFailure(self, *args, **kwargs):
        print('OnSteamServerConnectFailure')

    def CalcPlayerCount(self):
        """ Purpose: Returns the # of teammates of the local player. """
        count = 0
        if PlayerResource():
            for playerindex in range(1, MAX_PLAYERS+1):
                # find all players who are on the local player's team
                if PlayerResource().IsConnected(playerindex):
                    count += 1
        return count

    __nextupdatenumcurplayers = 0
    __nextupdateingameinfo = 0

    numplayers = 0

    def OnThink(self):
        self.chatlobby.OnThink()
        self.gamelobby.OnThink()

        if not self.IsVisible():
            return

        if engine.IsClientLocalToActiveServer() and self.__nextupdateingameinfo < Plat_FloatTime():
            self.__nextupdateingameinfo = Plat_FloatTime() + 1.0

            numplayers = self.CalcPlayerCount()
            if self.numplayers != numplayers:
                self.numplayers = numplayers
                self.PushValueToStore('hosting:host-num-players', numplayers)
            
        if self.__nextupdatenumcurplayers < Plat_FloatTime():
            self.__nextupdatenumcurplayers = Plat_FloatTime() + 5.0
            
            steamuserstats = steamapicontext.SteamUserStats()
            self.currentplayersresult = MainMenuNumberOfCurrentPlayersBroadcastCallResult(
                steamuserstats.GetNumberOfCurrentPlayers() if steamuserstats else 1
            )
            self.currentplayersresult.webview = self

    def OnScreenSizeChanged(self, old_width, old_height):
        super().OnScreenSizeChanged(old_width, old_height)

        self.video_settings.OnScreenSizeChanged(old_width, old_height)
        
    # Common
    def GoToMainMenu(self):
        self.GoToPage('main-menu')

    def GoToInGameMenu(self):
        self.GoToPage('main-menu/in-game')

    def GoToGameLobby(self):
        self.GoToPage('main-menu/game-lobby')

    def GoToPage(self, page):
        self.LoadURL(self.GetPageUrl(page))

    def GetPageUrl(self, page):
        return '%s/#%s' % (self.rooturl, page)

    # Angularjs functions
    def CallServiceMethod(self, servicename, methodname, args):
        """ Calls a function on an AngularJS service with the provided arguments. """
        injector = self.InvokeWithResult(self.InvokeWithResult(self.angular, 'element', [self.ObjectGetAttr(self.document, 'body')]), 'injector', [])
        service = self.InvokeWithResult(injector, 'get', [servicename])
        return self.InvokeWithResult(service, methodname, args)
        
    def AngularJsBroadcast(self, broadcastname, args):
        pass
        # if self.angularjs_rootscope:
        #    self.Invoke(self.angularjs_rootscope, '$broadcast', [broadcastname] + args)
        #    self.Invoke(self.angularjs_rootscope, '$apply', [])

    def PushValueToStore(self, key, value):
        self.Invoke(None, 'pushValueToStore', [key, value])

    def UpdateUserInfo(self):
        """ Pushes user info to store in main menu. This information usually does not change. """
        steamuser = steamapicontext.SteamUser()
        steamfriends = steamapicontext.SteamFriends()
        steamid = steamuser.GetSteamID() if steamuser else '12345678'

        self.PushValueToStore('general:user-info', {
            'name': steamfriends.GetFriendPersonaName(steamid) if steamfriends else '<Unknown>',
            'steamid': str(steamid),
            'steamAvailable': True if steamuser else False,
            'devBranch': self.GetDevBranch(),
            'gameVersion': self.GetVersion(),
        })

    def UpdateUserState(self):
        """ Pushes user state to store in main menu. """
        ingame = engine.IsConnected() or self.levelloading

        self.PushValueToStore('general:user-state', {
            'inGame': ingame,
            'isHosting': not ingame and engine.IsClientLocalToActiveServer(),
            'isOffline': not ingame and gpGlobals.maxClients == 1,
        })

    def GetDevBranch(self):
        if srcmgr.DEVVERSION:
            return 'dev'
        steamapps = steamapicontext.SteamApps()
        success, beta = steamapps.GetCurrentBetaName()
        if success:
            return beta
        return None

    def OnInputLanguageChanged(self, *args, **kwargs):
        self.PushValueToStore('general:input-language', gameui.GetCurrentKeyboardLangId())

    def GetVersion(self):
        if srcmgr.DEVVERSION:
            return srcmgr.DEVVERSION
            
        if filesystem.FileExists('gamerevision'):
            return 'BUILD-%s' % (filesystem.ReadFile('gamerevision', None).decode('utf-8'))
            
        return '%d.%d.%d' % (srcmgr.VERSION[0], srcmgr.VERSION[1], srcmgr.VERSION[2])
        
    def GetMissions(self):
        """ Builds the list of single player missions. """
        missionresourcedefault = {
            'name': 'Unknown map',
            'description': 'No description',
        }
    
        missions = []
        for filename in filesystem.ListDir(path="maps/", wildcard="*.bsp"):
            root, ext = os.path.splitext(filename)
            if root.startswith('sp_') and ext == '.bsp':
                mission_info = dict(missionresourcedefault)
                path = os.path.join('maps', '%s.res' % root)
                mission_info.update(LoadFileIntoDictionaries(path, default={}))
                mission_info['mapname'] = root

                if mission_info['description'] and mission_info['description'][0] == '#':
                    localized_value = localize.Find(mission_info['description'])
                    if localized_value is not None:
                        mission_info['description'] = localized_value

                # Correct image path
                if 'image' in mission_info:
                    mission_info['image'] = 'images/%s' % mission_info['image']
                
                missions.append(mission_info)
        return missions
        
    # GameUI Events
    def OnGameUIActivated(self):
        """ Called when the game ui is going to be shown, for example when starting the
            game or when pressing escape to go to the ingame main menu. """
        self.UpdateUserState()
      
    def OnGameUIHidden(self):
        """ Called when the gameui is being hidden. """
        self.UpdateUserState()

    def ChangeGameUIState(self, state):
        print('Main menu state changed to %s' % state)

        if state == 'LW_GAME_UI_STATE_LOADINGSCREEN':
            self.GoToPage('loading')
        elif state == 'LW_GAME_UI_STATE_INTROMOVIE':
            self.GoToPage('intro')
        elif state == 'LW_GAME_UI_STATE_INGAME':
            self.GoToInGameMenu()
        else:
            self.GoToMainMenu()

        # Set invisible when ingame to avoid UI interaction with Main Menu
        self.SetVisible(state != 'LW_GAME_UI_STATE_INGAME')

    def OnLevelLoadingStarted(self, levelname, showprogressdialog):
        """ Called when a level starts loading.
        
            Args:
                levelname (str): name of level
                showprogressdialog (bool): show the progress dialog?
        """
        self.levelloading = True
        
    def OnLevelLoadingFinished(self, error, failurereason, extendedreason):
        """ Called when a level finished loading (either success or canceled).
            
            Args:
                error (bool): True if loading failed.
                failurereason (str): message containing reason.
                extendedreason (str): Extended reason
        """
        self.levelloading = False

    def OnMatchEvent(self, event, *args, **kwargs):
        self.gamelobby.OnMatchEvent(event)

    # Javascript methods
    @jsbind()
    def clientcommand(self, methodargs):
        #print('Received client command %s' % (methodargs[0]))
        engine.ClientCommand(methodargs[0])
        
    @jsbind()
    def servercommand(self, methodargs):
        #print('Received server command %s' % (methodargs[0]))
        engine.ServerCommand(methodargs[0])
        
    @jsbind(hascallback=True)
    def getUserSteamID(self, methodargs):
        steamuser = steamapicontext.SteamUser()
        if not steamuser:
            return ['<no steam id>']
        return [str(steamapicontext.SteamUser().GetSteamID().ConvertToUint64())]
    
    @jsbind()
    def openurl(self, methodargs):
        if len(methodargs) > 0:
            url = methodargs[0]
            steamfriends = steamapicontext.SteamFriends()
            if steamfriends:
                steamapicontext.SteamFriends().ActivateGameOverlayToWebPage(url)
                
    @jsbind(hascallback=True)
    def gettranslations(self, methodargs):
        return self.GetGameUITranslations()
            
    @jsbind(hascallback=True)
    def retrieveversion(self, methodargs):
        return [self.curversion]
        
    @jsbind(hascallback=True)
    def retrievemissions(self, methodargs):
        return [self.GetMissions()]
        
    #@jsbind()
    #def clientcommand(self, methodargs):
    #    engine.ClientCommand('%s' % (methodargs[0]))
        
    @jsbind()
    def launchmission(self, methodargs):
        sessiondata = KeyValues("Session")

        sessiondata.SetString('system/network', 'offline')
        #sessiondata.SetString('system/access', 'public')

        sessiondata.SetString('game/mode', 'mission')
        sessiondata.SetString('game/bspname', methodargs[0])
        sessiondata.SetString('game/difficulty', methodargs[1])

        matchmaking.CreateSession(sessiondata)
        matchmaking.matchsession.Command(KeyValues("Start"))
        
    @jsbind()
    def launchbonus(self, methodargs):
        sessiondata = KeyValues("Session")

        sessiondata.SetString('system/network', 'offline')

        sessiondata.SetString('game/mode', 'swarmkeeper')
        sessiondata.SetString('game/bspname', 'random_100_100')

        matchmaking.CreateSession(sessiondata)
        matchmaking.matchsession.Command(KeyValues("Start"))
        
    @jsbind()
    def gameuicommand(self, methodargs):
        GameUICommand(methodargs[0])
        
    # Statistics
    currentplayersresult = None

    @jsbind(hascallback=True, manuallycallback=True)
    def getNumberOfPlayers(self, methodargs, callbackid):
        steamuserstats = steamapicontext.SteamUserStats()
        self.currentplayersresult = MainMenuNumberOfCurrentPlayersCallResult(
            steamuserstats.GetNumberOfCurrentPlayers() if steamuserstats else 0
        )
        self.currentplayersresult.callbackid = callbackid
        self.currentplayersresult.webview = self
        
    # Matchmaking
    @jsbind()
    def creategamelobby(self, methodargs):
        sessiondata = KeyValues("Session")

        sessiondata.SetString('system/network', 'LIVE')
        sessiondata.SetString('system/access', 'public')

        sessiondata.SetString('game/mode', 'gamelobbyrules')
        sessiondata.SetString('game/bspname', 'gamelobby')

        matchmaking.CreateSession(sessiondata)
        
    @jsbind()
    def findfriendgames(self, methodargs):
        settings = KeyValues.FromString(
            "settings",
            " game { " + \
                " mode = " + \
            " } " 
        )

        settings.SetString( "game/mode", "" );

        GameUIOpenWindow(WINDOW_TYPE.WT_ALLGAMESEARCHRESULTS, True, settings );
        
    @jsbind()
    def findpublicgame(self, methodargs):
        settings = KeyValues.FromString(
            "settings",
            " system { " + \
                " network LIVE " + \
            " } " + \
            " game { " + \
                " mode = " + \
            " } " + \
            " options { " + \
                " action custommatch " + \
            " } "
        )

        settings.SetString("game/mode", '')

        GameUIOpenWindow(
            WINDOW_TYPE.WT_FOUNDPUBLICGAMES, #ui_play_online_browser.GetBool() ? WT_FOUNDPUBLICGAMES : WT_GAMESETTINGS,
           True, settings );
        
    @jsbind()
    def createsoloplay(self, methodargs):
        settings = KeyValues.FromString(
        "settings",
        " system { " + \
        " network offline " + \
        " } " + \
        " game { " + \
        " mode sdk " + \
        " bspname gamelobby " + \
        " } "
        )

        settings.SetString("Game/difficulty", '')

        matchmaking.CreateSession(settings)

        # Automatically start the credits session, no configuration required
        matchmaking.matchsession.Command(KeyValues("Start"))
            
    @jsbind()
    def creategametutorial(self, methodargs):
        settings = KeyValues.FromString(
        "settings",
        " system { " + \
        " network offline " + \
        " } " + \
        " game { " + \
        " mode mission " + \
        " bspname tutorial_annihilation_rebels " + \
        " } "
        );

        settings.SetString("Game/difficulty", '')

        # Create Session and DIRECTLY start the game
        matchmaking.CreateSession(settings)
        matchmaking.matchsession.Command(KeyValues("Start"))
        
    # Options
    @jsbind()
    def openvideo(self, methodargs):
        GameUIOpenWindow(WINDOW_TYPE.WT_VIDEO)
        
    @jsbind()
    def openbrightness(self, methodargs):
        OpenGammaDialog(self.GetVPanel())
        
    @jsbind()
    def openaudio(self, methodargs):
        GameUIOpenWindow(WINDOW_TYPE.WT_AUDIO)
        
    @jsbind()
    def openkeyboardmouse(self, methodargs):
        GameUIOpenWindow(WINDOW_TYPE.WT_KEYBOARDMOUSE)
        
    @jsbind()
    def openmultiplayersettings(self, methodargs):
        GameUIOpenWindow(WINDOW_TYPE.WT_MULTIPLAYER)
