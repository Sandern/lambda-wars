from cef import WebViewComponent, jsbind
from gameui import CGameUIConVarRef, GetBindingsToKeys, VirtualKeyToKey
from gameinterface import engine
import copy
import filesystem
from _vlocalize import localize
import re
from core.signals import (key_bindings_changed)

class WebHotKeys(WebViewComponent):
    js_object_name = 'hotkeys'

    bindings_to_keys = None
    settings = None

    def __init__(self, webview):
        super().__init__(webview)

        self.settings = self.ReadKbAct('scripts/kb_act.lst')

        key_bindings_changed.connect(self.OnKeyBindingsChanged)

    def OnDestroy(self):
        super().OnDestroy()

        key_bindings_changed.disconnect(self.OnKeyBindingsChanged)

    @jsbind()
    def refreshHotkeys(self, methodargs):
        self.webview.PushValueToStore('settings:hotkeys', self.BuildCurrentSettings())

    @jsbind()
    def bind(self, methodargs):
        binding = methodargs[0]
        old_key = methodargs[1]
        key_code = methodargs[2]

        new_key = VirtualKeyToKey(key_code)

        self.PerformBind(binding, old_key, new_key)

    @jsbind()
    def mouseBind(self, methodargs):
        binding = methodargs[0]
        old_key = methodargs[1]
        new_key = methodargs[2]

        self.PerformBind(binding, old_key, new_key)

    def PerformBind(self, binding, old_key, new_key):
        if old_key == new_key:
            return

        command = ''
        if old_key:
            command += f'unbind "{old_key}"\n'
        command += f'unbind "{new_key}"\nbind "{new_key}" "{binding}"\nhost_writeconfig\n'
        print(command)
        engine.ExecuteClientCmd(command)

    @jsbind(hascallback=True)
    def getCurrentSettings(self, methodargs):
        return [self.BuildCurrentSettings()]

    @jsbind()
    def resetToDefault(self, methodargs):
        print('Reset to default bindings')
        engine.ClientCmd_Unrestricted(f'unbindall\nexec config_default.cfg\nhost_writeconfig\n')

    def OnKeyBindingsChanged(self, *args, **kwargs):
        self.refreshHotkeys([])

    def BuildCurrentSettings(self):
        self.bindings_to_keys = GetBindingsToKeys()

        settings = copy.deepcopy(self.settings)
        for setting in settings:
            self.ParseSetting(setting)

        return settings

    def ParseSetting(self, setting):
        if setting['displayName'][0] == '#':
            setting['displayName'] = localize.Find(setting['displayName'])
        if 'choices' in setting:
            for choice in setting['choices']:
                if choice[1][0] == '#':
                    choice[1] = localize.Find(choice[1])
        if 'binding' in setting:
            setting['key'] = self.bindings_to_keys.get(setting['binding'], '')

    def ReadKbAct(self, filename):
        settings = []

        for line_number, line in enumerate(filesystem.ReadFile(filename, None, textmode=True).splitlines()):
            if not line.strip():
                continue
            values = re.findall(r'"([^"]*)"', line)
            if len(values) != 2:
                print('Invalid config values at line number %d in "%s":\n%s' % (line_number, filename, line))
                continue

            binding, description = values

            if description.startswith('='):
                continue

            if binding == 'blank':
                settings.append({
                    'displayName': description,
                    'type': 'sectionTitle',
                })
            else:
                settings.append({
                    'displayName': description,
                    'binding': binding,
                    'type': 'binding',
                })

        return settings

