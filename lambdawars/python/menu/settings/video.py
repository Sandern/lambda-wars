from cef import WebViewComponent, jsbind
from gameui import GetVideoSettings, GetSettingFromDefaultVideoCfg
from gameinterface import engine, ConVarRef

AUTO_OPTION_VALUE = 9999999

# Fallback values in Apply when default config is missing
setting_fallbacks = {
    'cpu_level': 0,
    'gpu_level': 0,
    'mat_forceaniso': 1,
    'gpu_mem_level': 0,
    'csm_quality_level': 0,

    'mat_software_aa_strength': 0,
    'mat_queue_mode': -1,
    'mat_motion_blur_enabled': 0,
    'mat_monitorgamma_tv_enabled': 0,
    'mat_powersavingsmode': 0,
}

class WebVideoSettings(WebViewComponent):
    js_object_name = 'video'
    settings = None

    def BuildCurrentSettings(self):
        current_settings = GetVideoSettings()

        return {
            'displayMode': current_settings['displayMode'],
            'resolution': current_settings['resolution'],
            'desktopResolution': current_settings['desktopResolution'],
            'fullScreenResolutions': current_settings['fullScreenResolutions'],
            'windowedResolutions': current_settings['windowedResolutions'],

            'cpu_level': self.GetSettingsValueForUI('cpu_level'),
            'cpu_level_auto': self.GetSettingsAutoValue('cpu_level'),
            'gpu_level': self.GetSettingsValueForUI('gpu_level'),
            'gpu_level_auto': self.GetSettingsAutoValue('gpu_level'),
            'gpu_mem_level': self.GetSettingsValueForUI('gpu_mem_level'),
            'gpu_mem_level_auto': self.GetSettingsAutoValue('gpu_mem_level'),
            'csm_quality_level': self.GetSettingsValueForUI('csm_quality_level'),
            'csm_quality_level_auto': self.GetSettingsAutoValue('csm_quality_level'),
        }

    def GetSettingsValueForUI(self, convar_name):
        ui_convar = ConVarRef('%s_optionsui' % convar_name)
        return ui_convar.GetInt()

    def GetSettingsValue(self, convar_name):
        convar = ConVarRef('%s' % convar_name)
        return convar.GetInt()

    def GetSettingsAutoValue(self, convar_name):
        settings_value = GetSettingFromDefaultVideoCfg('setting.%s' % convar_name)
        if settings_value is not None:
            return int(settings_value)
        return setting_fallbacks.get(convar_name, 0)

    @jsbind()
    def refreshVideoSettings(self, methodargs):
        self.settings = self.BuildCurrentSettings()
        self.webview.PushValueToStore('settings:video', self.settings)

    def OnScreenSizeChanged(self, old_width, old_height):
        self.refreshVideoSettings([])

    @jsbind()
    def apply(self, methodargs):
        """ Applies settings to game. """
        # New settings is same structure as in BuildCurrentSettings, but with the new selected settings reflected
        new_settings = self.ParseSettingsFromList(methodargs[0])

        # Apply resolution settings
        old_resolution = self.settings['resolution']
        new_resolution = new_settings['resolution']
        if (old_resolution['width'] != new_resolution['width'] or
                old_resolution['height'] != new_resolution['height'] or
                self.settings['displayMode'] != new_settings['displayMode']):
            windowed = 1 if new_settings['displayMode'] != 1 else 0
            border_less = 1 if new_settings['displayMode'] == 2 else 0

            engine.ClientCmd_Unrestricted('mat_setvideomode %d %d %d %d\n' %
                                          (new_resolution['width'], new_resolution['height'], windowed, border_less))

        # Apply advanced settings
        self.ApplyAutoSetting('csm_quality_level', new_settings['csm_quality_level'])
        self.ApplyAutoSetting('gpu_mem_level', new_settings['gpu_mem_level'])
        self.ApplyAutoSetting('cpu_level', new_settings['cpu_level'])
        self.ApplyAutoSetting('gpu_level', new_settings['gpu_level'])

        # Make sure to call mat_savechanges at the end!
        engine.ClientCmd_Unrestricted('mat_savechanges\n')

        # Ensure latest settings are reflected
        # triggered too early for resolution change, so again called in OnScreenSizeChanged
        self.refreshVideoSettings([])

    def ApplyAutoSetting(self, name, value):
        convar = ConVarRef(name)
        ui_convar = ConVarRef('%s_optionsui' % name)
        restart_convar = ConVarRef('%s_restart' % name)

        actual_value = value

        if value == AUTO_OPTION_VALUE:
            actual_value = self.GetSettingsAutoValue(name)

        if restart_convar.IsValid():
            restart_convar.SetValue(actual_value)

        ui_convar.SetValue(value)
        convar.SetValue(actual_value)


    def ParseSettingsFromList(self, new_setttings_list):
        """ Temporary until we can convert js objects to Python dictionaries... """
        new_settings = {}

        for new_setting in new_setttings_list:
            key, value = new_setting

            if key == 'resolution':
                new_settings[key] = { 'width': value[0], 'height': value[1] }
            else:
                new_settings[key] = value

        return new_settings