import uuid
from srcbuiltins import KeyValues, KeyValuesDumpAsDevMsg
from cef import WebViewComponent, jsbind
import matchmaking
from steam import (CSteamID, steamapicontext, LobbyChatMsgCallback, LobbyChatUpdateCallback, LobbyMatchListCallResult,
                   k_uAPICallInvalid, ELobbyDistanceFilter, ELobbyComparison)
from gameinterface import engine

from .gamelobby_datamodel import GameLobbyDataModel
from .settingsinfo import SettingsInfo

from core.gamerules.info import dbgamerules


class WebLobbyMatchListCallResult(LobbyMatchListCallResult):
    def OnLobbyMatchList(self, lobbymatchlist, iofailure):
        self.webview.OnLobbyMatchListCallResult(lobbymatchlist, iofailure, self.callbackid)


class WebGameLobby(WebViewComponent, LobbyChatMsgCallback, LobbyChatUpdateCallback):
    js_object_name = 'gamelobby'

    # Helper instances.
    data_model = None
    settings_info = None

    # List callback variable for lobbies. Maybe move code out of WebGameLobby? Doesn't really share code anymore.
    lobby_list_callback = None

    # Maintains active lobby state
    lobby_state = 'none'
    lobby_id = None
    lobby_owner_id = None
    _last_lobby_owner_id = CSteamID()
    chat_history = []

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.lobby_chat_handlers = self.BuildLobbyChatMsgHandlers()

    def OnInitializedBindings(self):
        super().OnInitializedBindings()

        self.PushLobbyData()

    def OnDestroy(self):
        super().OnDestroy()

        matchmaking.CloseSession()

    def BuildLobbyChatMsgHandlers(self):
        """ Creates chat handlers for callbacks from LobbyChatMsgCallback. """
        return {
            'chat': self.HandleLobbyChatMsg,
            'globalmsg': self.HandleLobbyGlobalMsg,
            'player': self.HandleLobbyPlayerDataMsg,
            'slot': self.HandleLobbySlotMsg,
            'spectate': self.HandleLobbySpectateMsg,
            'kicked_player': self.HandleKickedPlayer,
        }

    def OnThink(self):
        pass

    def StartTestActiveLobby(self):
        """ Starts a test for the active gamelobby.

        This allows a crashed player to rejoin after starting the game.
        """
        # TODO

    def OnMatchEvent(self, event):
        """ Receives all match events from the matchmaking system. """
        event_name = event.GetName()
        DevMsg(4, 'Match Event: %s\n' % event_name)

        if event_name == 'OnMatchSessionUpdate':
            self.OnMMSessionUpdate(event)
        elif event_name == 'OnPlayerMachinesConnected':
            self.OnPlayerConnected(event)
        elif event_name == 'OnPlayerMachinesDisconnected':
            self.OnPlayerDisconnected(event)
        elif event_name == 'OnPlayerRemoved':
            self.OnPlayerRemoved(event)

    def OnMMSessionUpdate(self, event):
        """ Called on matchmaking session updates.

        Means data changed.
        """
        DevMsg(1, 'Change Event: \n')
        KeyValuesDumpAsDevMsg(event)

        if not matchmaking.IsSessionActive():
            self.UpdateLobbyState(event)
            self.OnLeftLobby()
            return

        session_system_data = matchmaking.matchsession.GetSessionSystemData()

        # xuidReserve is same as Steam lobby id
        self.lobby_id = CSteamID(int(session_system_data.GetString('xuidReserve')))
        # xuidHost is the one who owns the lobby
        self.lobby_owner_id = CSteamID(int(session_system_data.GetString('xuidHost')))

        if self._last_lobby_owner_id != self.lobby_owner_id:
            self.OnLobbyOwnerChanged(self._last_lobby_owner_id)
            self._last_lobby_owner_id = self.lobby_owner_id

        self.UpdateLobbyState(event)

        state = event.GetString('state', '')
        transition = event.GetString('transition', '')

        if state == 'ready' and (transition == 'hostinit' or transition == 'clientconnect'):
            self.InitLobby()

        self.UpdateLobbyData(event)

    def OnPlayerConnected(self, event):
        """ Called upon player machine being connected.

        Indicates the player entered the lobby.
        Find an open slot if possible.

        @param event machine connected event with id of player.
        """
        steamid = event.GetString('id')
        user_info = self.BuildUserInfo(self.FindPlayerByXuid(steamid))
        DevMsg(1, 'User %s entered chat room\n' % (user_info['username']))
        # if self.js_handler:
        #     self.Invoke(self.js_handler, 'OnLobbyChatUserEntered', [user_info])

        # Find slot for player upon entering
        if self.is_lobby_owner:
            found_slot = self.FindSlotWithSteamID(steamid)
            if not found_slot:
                self.settings_info.FindSlotForPlayer(steamid)

    def OnPlayerDisconnected(self, event):
        """ Called upon player machine being disconnected.

        Indicates player left the lobby.

        @param event machine disconnected event with id of player.
        """
        self.RemovePlayerFromLobby(event.GetString('id'))

    def OnPlayerRemoved(self, event):
        """ Called upon OnPlayerRemoved event.

        Does pretty much the same as player disconnected, but different event structure...

        @param event data
        """
        self.RemovePlayerFromLobby(event.GetString('player/xuid'))

    def OnLobbyOwnerChanged(self, old_lobby_owner_id):
        self.PushLobbyOwner()

        if not self.is_lobby_owner:
            return

        # There's no old lobby owner yet when lobby is created
        if not old_lobby_owner_id.IsValid():
            return

        self.settings_info.FindAndRemovePlayerFromSlot(old_lobby_owner_id)

        steam_friends = steamapicontext.SteamFriends()
        self.SendLobbyChatMsg('globalmsg', 'Lobby leader changed to %s' % steam_friends.GetFriendPersonaName(self.lobby_owner_id))


    def RemovePlayerFromLobby(self, steamid):
        DevMsg(1, 'User %s left chat room\n' % steamid)

        if self.is_lobby_owner:
            # Remove from slots
            self.settings_info.FindAndRemovePlayerFromSlot(steamid)

        # if self.js_handler:
        #     self.Invoke(self.js_handler, 'OnLobbyChatUserLeft', [steamid])

    def OnLobbyChatMsg(self, data):
        """ Callback handler for any lobby chat msg in which the player is present.

        Filter out messages not intended for this lobby.
        Then process the type of chat message, as it's also used for player data changes (like changing faction).
        """
        if data.steamidlobby != self.lobby_id:
            return  # Not for us

        matchmaking = steamapicontext.SteamMatchmaking()
        if not matchmaking:
            return
        steamiduser = CSteamID(data.steamiduser)
        data, type = matchmaking.GetLobbyChatEntry(CSteamID(data.steamidlobby), data.chatid, steamiduser)

        if not data:
            PrintWarning('game lobby: invalid lobby chat msg\n')
            return

        self.DoHandleLobbyChatMsg(data, steamiduser, type)

    def OnLobbyChatUpdate(self, data):
        """ Callback handler for any lobby chat update in which the player is present. """
        if data.steamidlobby != self.lobby_id:
            return  # Not for us
        # print('OnLobbyChatUpdate: %s' % data.steamidlobby)

    def DoHandleLobbyChatMsg(self, data, steamiduser, type):
        """ Handles lobby chat msg. Data is already retrieved.
            Shared function for offline and online lobby.

            Args:
                data (str): chat data
                steamiduser (CSteamID): steam id of the user sending the message
                type (EChatEntryType): type of message
        """
        try:
            msg_type, msg = data.split(' ', 1)
        except ValueError:
            msg_type = data
            msg = ''

        handler = self.lobby_chat_handlers.get(msg_type, None)
        if not handler:
            PrintWarning('Invalid chat msg type %s\n' % msg_type)
            return

        handler(steamiduser, type, msg)

    def HandleLobbyChatMsg(self, steamiduser, type, msg):
        """ Handler for player chat messages in the lobby.

        These are forwarded to the UI.
        """
        DevMsg(1, 'OnLobbyChatMsg -> Text: %s, type: %s, steamid: %s\n' % (msg, type, str(steamiduser)))
        self.chat_history.append({ 'steamid': str(steamiduser), 'text': msg, 'id':  str(uuid.uuid4())[:8] })
        self.PushLobbyChatHistory()

    def HandleLobbyGlobalMsg(self, steamiduser, type, msg):
        """ Handles global messages in lobby.

        These are forwarded to the UI.
        """
        if steamiduser != self.lobby_owner_id:
            return

        self.chat_history.append({ 'text': msg, 'id':  str(uuid.uuid4())[:8] })
        self.PushLobbyChatHistory()

    def PushLobbyChatHistory(self):
        # print('push update chat history: %s -> %s' % (f'{self.js_object_name}:history', self.chat_history))
        self.webview.PushValueToStore(f'{self.js_object_name}:history', self.chat_history)

    def HandleLobbyPlayerDataMsg(self, steamiduser, type, data):
        # print('HandleLobbyPlayerDataMsg %s' % (data))
        if not self.is_lobby_owner or not self.is_lobbying:
            return
        lobby_owner_id = self.lobby_owner_id

        # By default sets data for a user in the lobby
        # If a slot id is found, then the data is being set for a cpu
        key, value = data.strip().split('=', 1)
        try:
            value, slotid = value.split(' ', 1)
            slotid = int(slotid)
            # Since we found a slotid, the message should come from the lobby owner
            if lobby_owner_id != steamiduser:
                return
        except ValueError:
            slotid = None

        # print('Setting player data %s to value %s (slotid: %s)' % (key, value, slotid))
        data_model = self.data_model
        if key == 'faction':
            if value not in self.settings_info.availablefactions:
                PrintWarning('Trying to set invalid faction %s for player %s\n' % (value, str(steamiduser)))
                return
            if slotid is not None:
                data_model.SetSlotLobbyData(slotid, 'faction', value)
            else:
                data_model.SetPlayerLobbyData(steamiduser, 'faction', value)
            # self.settings_info.InvalidateReady() #if player selects ready then he is ready!
        elif key == 'color':
            if not self.settings_info.IsColorAvailable(value):
                PrintWarning('Trying to set invalid or unavailable color %s for player %s\n' % (value, str(steamiduser)))
                return
            if slotid is not None:
                data_model.SetSlotLobbyData(slotid, 'color', value)
            else:
                data_model.SetPlayerLobbyData(steamiduser, 'color', value)
            self.settings_info.RebuildAvailableColors()
        elif key == 'difficulty':  # CPU Only
            found_diff = False
            for x in self.settings_info.availabledifficulties:
                if x['id'] == value:
                    found_diff = True
                    break
            if not found_diff:
                PrintWarning('Trying to set invalid difficulty %s for player %s\n' % (value, str(steamiduser)))
                return
            data_model.SetSlotLobbyData(slotid, 'difficulty', value)
        elif key == 'ready':
            data_model.SetPlayerLobbyData(steamiduser, 'ready', str(int(value)))
        else:
            PrintWarning('Trying to set unknown player data %s for player %s\n' % (key, str(steamiduser)))

    def HandleLobbySlotMsg(self, steamiduser, type, data):
        """ Lobby chat handler to try and take a slot. """
        if not self.is_lobby_owner or not self.is_lobbying:
            return

        try:
            slotid = int(data)
        except ValueError:
            PrintWarning('HandleLobbySlotMsg -> Invalid slot id: %s\n' % slotid)
            return

        self.settings_info.TryTakeSlot(steamiduser, slotid)

    def HandleLobbySpectateMsg(self, steamiduser, type, data):
        """ Lobby chat handler for player going spectator. """
        if not self.is_lobby_owner or not self.is_lobbying:
            return

        self.settings_info.FindAndRemovePlayerFromSlot(steamiduser)

    def HandleKickedPlayer(self, steamiduser, type, data):
        """ Lobby chat message handler for notification that lobby owner kicked a player. """
        steamid, username = data.split('_', 1)
        user_info = {
            'steamid': steamid,
            'username': username,
        }
        DevMsg(1, 'User %s was kicked\n' % (user_info['username']))
        if self.js_handler:
            self.Invoke(self.js_handler, 'OnLobbyUserKicked', [user_info])


    def UpdateLobbyState(self, event):
        """ Updates the lobby state.

        Could be creating, lobby, ingame, etc.
        Broadcasts the state to the UI.
        """
        self.SetLobbyState(self.ComputeNewLobbyState(event))


    def ComputeNewLobbyState(self, event):
        """ Figure out new lobby state from change event.

        Return None if nothing changed.
        """

        # This makes so much sense.
        state = event.GetString('state', '')
        progress = event.GetString('progress', '')
        transition = event.GetString('transition', '')
        error = event.GetString('error', '')

        lock = event.GetString('update/system/lock', '')
        # server = event.GetString('update/server/server')
        game_state = event.GetString('update/game/state')

        if state == 'progress' and progress == 'creating':
            return 'creating'
        elif state == 'ready' and (transition == 'hostinit' or transition == 'clientconnect'):
            return 'lobbying'
        elif state == 'closed':
            return 'none'
        elif state == 'error':
            if error == 'kicked':
                # Show a dialog we were kicked
                self.webview.AngularJsBroadcast('user:kicked', [])
            return 'none'
        elif lock == 'starting':
            return 'startinglocalserver'
        elif game_state == 'game':
            return 'gamestarted'

        return None

    def SetLobbyState(self, new_lobby_state):
        if not new_lobby_state or new_lobby_state == self.lobby_state:
            return

        self.lobby_state = new_lobby_state
        self.PushLobbyState()

        DevMsg(1, 'Lobby state changed to %s\n' % self.lobby_state)

    def InitLobby(self):
        """ Initializes the lobby once created.

        Creates instances for data model access and managing settings.
        Triggers initial change callbacks to correctly set mode and map.
        """
        self.data_model = GameLobbyDataModel()
        self.settings_info = SettingsInfo(self)

        if self.is_lobby_owner:
            self.settings_info.OnModeChanged()
            self.settings_info.OnMapChanged()

            # Find slot for player that just created the lobby
            self.settings_info.FindSlotForPlayer(self.lobby_owner_id)

    def OnLeftLobby(self):
        """ Cleanup lobby data. """
        self.lobby_state = None
        self.lobby_id = None
        self.lobby_owner_id = None
        self.data_model = None
        self.settings_info = None

    def UpdateLobbyData(self, event):
        """ Updates data for lobby.

        Think of the available settings and player slots.
        """
        data_model = self.data_model
        if not data_model:
            return

        if event.GetString('update/game/mode', ''):
            self.settings_info.OnModeChanged()
        if event.GetString('update/game/bspname', ''):
            self.settings_info.OnMapChanged()
        if event.GetString('update/game/teamsetup', ''):
            self.settings_info.OnTeamSetupChanged()
        if not event.IsEmpty('update/game/slots'):
            self.settings_info.OnSlotsChanged()
            self.PushLobbySlots()
            self.PushLobbyOwner() # Ready might have changed

        # Always refresh dropdowns for now.
        self.settings_info.RebuildAvailableFactions()
        self.settings_info.RebuildCustomFields()

        # Update settings_info to reflect potential changes
        self.PushLobbySettings()
        self.PushLobbyMembersInfoList()

    def PushLobbyData(self):
        """ Pushes the full lobby data to the UI. """
        self.PushLobbyState()
        self.PushLobbySettings()
        self.PushLobbyOwner()
        self.PushLobbyMembersInfoList()
        self.PushLobbySlots()
        if self.settings_info:
            self.settings_info.PushLobbySettingsInfo()

    def PushLobbyState(self):
        self.webview.PushValueToStore('gamelobby:state', self.lobby_state)

    def PushLobbySettings(self):
        self.webview.PushValueToStore('gamelobby:settings', self.BuildSettingsInfo())

    def PushLobbyOwner(self):
        self.webview.PushValueToStore('gamelobby:lobby-owner', self.BuildLobbyOwnerInfo())

    def PushLobbyMembersInfoList(self):
        self.webview.PushValueToStore('gamelobby:lobby-members', self.BuildLobbyMembersInfoList())

    def PushLobbySlots(self):
        self.webview.PushValueToStore('gamelobby:slots', self.data_model.slots if self.data_model else [])

    def BuildLobbyMembersInfoList(self):
        """ Build dictionary with all players in the lobby.

            This dictionary maps the players by steam id, like:

            steamid: {
                'username': 'Sandern',
                'steamid': '1',
            }
        """
        members = {}
        if not self.data_model:
            return members

        session_data = self.data_model.session_data

        num_machines = session_data.GetInt('members/numMachines')
        for i in range(0, num_machines):
            num_players = session_data.GetInt('members/machine%d/numPlayers' % i)

            for j in range(0, num_players):
                user_info = self.BuildUserInfo(session_data.FindKey('members/machine%d/player%d' % (i, j)))
                members[user_info['steamid']] = user_info

        return members

    def BuildUserInfo(self, player):
        """" Builds user info from player data system session."""
        steamid = player.GetString('xuid')

        user_data = {
            'steamid': steamid,
            'username': player.GetString('name')
        }

        player_slot = self.FindSlotWithSteamID(steamid)
        if player_slot:
            user_data.update({
                'faction': player_slot['player']['faction'],
                'color': player_slot['player']['color'],
                'ready': player_slot['player']['ready'],
                'islocaluser': player_slot['player']['islocaluser'],
                'localslotid': player_slot['slotid'],
            })

        return user_data

    def FindPlayerByXuid(self, xuid):
        """ Find player data by xuid. """
        session_data = self.data_model.session_data

        num_machines = session_data.GetInt('members/numMachines')
        for i in range(0, num_machines):
            num_players = session_data.GetInt('members/machine%d/numPlayers' % i)

            for j in range(0, num_players):
                player = session_data.FindKey('members/machine%d/player%d' % (i, j))

                if player.GetString('xuid') == xuid:
                    return player

        return None

    def BuildSettingsInfo(self):
        """" Builds settings information for lobby UI. """
        data_model = self.data_model
        if not data_model:
            return None

        settings_info = {}

        found_slot = self.FindSlotWithSteamID(steamapicontext.SteamUser().GetSteamID())

        # Building info for lobby in which is player
        settings_info.update({
            'steamid': str(self.lobby_owner_id.ConvertToUint64() if self.lobby_owner_id else 'offline'),
            'name': data_model.name,
            'num_members': self.num_members,
            'lobbytype': data_model.access,

            'mode': data_model.mode,
            'map': data_model.map,
            'teamsetup': data_model.teamsetup,
            'islobbyowner': self.is_lobby_owner,
            'spectators': self.BuildSpectatorList(),
            'numslots': data_model.numslots,
            'numtakenslots': data_model.numtakenslots,
            'localslotid': found_slot['slotid'] if found_slot else None,
            # 'mm_password': mm_password.GetString(),
            'match_uuid': data_model.match_uuid,
        })

        if self.settings_info:
            settings_info.update({
                'availablemodes': self.settings_info.availablemodes,
                'availablemaps': self.settings_info.availablemaps,
                'customfields': self.settings_info.customfields,
            })

        return settings_info

    def BuildLobbyOwnerInfo(self):
        return {
            'steamid': str(self.lobby_owner_id.ConvertToUint64() if self.lobby_owner_id else 'offline'),
            'islobbyowner': self.is_lobby_owner,
            'ready': self.data_model.IsLobbyLeaderReady() if self.data_model else True
        }

    def BuildSpectatorList(self):
        """" Builds spectator list for lobby UI. """
        spectators = []

        session_data = self.data_model.session_data

        steam_friends = steamapicontext.SteamFriends()

        num_machines = session_data.GetInt('members/numMachines')
        for i in range(0, num_machines):
            num_players = session_data.GetInt('members/machine%d/numPlayers' % i)

            for j in range(0, num_players):
                player = session_data.FindKey('members/machine%d/player%d' % (i, j))
                steamid = CSteamID(int(player.GetString('xuid')))

                if not self.FindSlotWithSteamID(steamid):
                    spectators.append({
                        'name': steam_friends.GetFriendPersonaName(steamid),
                        'steamid': str(steamid),
                        'islocaluser': steamid == steamapicontext.SteamUser().GetSteamID()
                    })

        return spectators

    def FindSlotWithSteamID(self, steamid):
        """ Find slot of player given the steamid """
        data_model = self.data_model
        if not data_model:
            return None

        slots = data_model.slots
        for slot in slots:
            player_data = slot['player']
            if player_data and player_data['steamid'] == str(steamid):
                return slot

        return None

    @property
    def is_lobby_owner(self):
        """ Does local user owns this lobby? """
        return self.lobby_owner_id == steamapicontext.SteamUser().GetSteamID()

    @property
    def is_lobbying(self):
        return self.lobby_state == 'lobbying'

    @property
    def is_offline_lobby(self):
        return self.data_model and self.data_model.session_data.GetString('system/network', '').lower() == 'offline'

    # Access only: number of members
    @property
    def num_members(self):
        steammatchmaking = steamapicontext.SteamMatchmaking()
        lobby_id = self.lobby_id
        if not steammatchmaking or not lobby_id:
            return 1
        return steammatchmaking.GetNumLobbyMembers(lobby_id)

    def SetDataModelSetting(self, key, value):
        if not hasattr(self.data_model, key):
            PrintWarning('SetSetting: invalid key %s\n' % key)
            return

        if not self.is_lobby_owner:
            return

        setattr(self.data_model, key, value)
        self.settings_info.InvalidateReady()  # remove ready if game settings change

    def SendLobbyChatMsg(self, msgtype, msg=''):
        """ Sends a lobby chat message (communication and changing player/settings data).

            Online this sends it through Steam, offline it directly handles the message.

            Args:
                msgtype (str): Type of message to send
                msg (str): Data to send
        """
        steammatchmaking = steamapicontext.SteamMatchmaking()
        if not steammatchmaking:
            return
        steammatchmaking.SendLobbyChatMsg(self.lobby_id, '%s %s' % (msgtype, msg))

    def OnLobbyMatchListCallResult(self, lobbymatchlist, iofailure, callbackid):
        """ Callback for listlobbies js method. """
        if iofailure:
            PrintWarning('OnLobbyMatchListCallResult: io failure\n')
            self.SendCallback(callbackid, [[], True])
            return

        matchmaking = steamapicontext.SteamMatchmaking()

        lobbies = []

        for i in range(0, lobbymatchlist.lobbiesmatching):
            steamid = matchmaking.GetLobbyByIndex(i)

            map_name = matchmaking.GetLobbyData(steamid, 'game:bspname')

            lobbies.append({
                'steamid': str(steamid),

                'name': matchmaking.GetLobbyData(steamid, 'game:name'),
                'map': map_name,
                'mode': matchmaking.GetLobbyData(steamid, 'game:mode'),
                'overviewsrc': SettingsInfo.GetOverviewSrc(map_name),
                'numtakenslots': matchmaking.GetLobbyData(steamid, 'game:slots:numTakenSlots'),
                'numslots': matchmaking.GetLobbyData(steamid, 'game:slots:numSlots'),
            })

        self.SendCallback(callbackid, [lobbies])

    # Only Javascript methods follow below!
    @jsbind()
    def createlobby(self, methodargs):
        """ Create a new lobby.

        Set up the initial data.
        """
        access = methodargs[1] if len(methodargs) > 1 and methodargs[1] else 'public'

        session_data = KeyValues("Session")

        # listen for now. When not set defaults to "official", always requiring a dedicated server
        session_data.SetString('options/server', 'listen')

        session_data.SetString('system/network', 'OFFLINE' if access == 'offline' else 'LIVE')
        session_data.SetString('system/access', access)

        session_data.SetString('game/type', self.js_object_name)
        session_data.SetString('game/version', engine.GetProductVersionString())

        session_data.SetString('game/name', 'New Lobby')
        session_data.SetString('game/mode', 'annihilation')

        # Enough slots for players + spectators
        session_data.SetInt('members/numSlots', 16)

        matchmaking.CreateSession(session_data)

    @jsbind()
    def joinlobby(self, methodargs):
        session_data = KeyValues("Session")
        session_data.SetString("system/network", "LIVE")
        session_data.SetString("options/action", "joinsession")
        session_data.SetUint64("options/sessionid", int(methodargs[0]))
        matchmaking.MatchSession(session_data)

    @jsbind()
    def leavelobby(self, methodargs):
        matchmaking.CloseSession()
        self.webview.GoToMainMenu()

    def AddLobbyTypeFilters(self, limitversion=True):
        """ Adds lobby filters which should be added to every lobby list query call.

            This filters on lobby type and version.

            Kwargs:
                limitversion (bool): Filters out lobby for a different game version.
        """
        steammatchmaking = steamapicontext.SteamMatchmaking()
        # For now, search on lobbies everywhere
        steammatchmaking.AddRequestLobbyListDistanceFilter(ELobbyDistanceFilter.k_ELobbyDistanceFilterWorldwide)
        # There are currently two types: "global chat" and "game lobby". Obviously you don't want a game lobby in the
        # global chat
        steammatchmaking.AddRequestLobbyListStringFilter('game:type', self.js_object_name,
                                                         ELobbyComparison.k_ELobbyComparisonEqual)

        if limitversion:
            # You can only connect to servers with the same version, so it makes no sense to join lobbies made for a
            # different version
            steammatchmaking.AddRequestLobbyListStringFilter('game:version', engine.GetProductVersionString(),
                                                              ELobbyComparison.k_ELobbyComparisonEqual)

    def AddLobbyListFilters(self):
        self.AddLobbyTypeFilters()

    @jsbind(hascallback=True, manuallycallback=True)
    def listlobbies(self, methodargs, callbackid):
        matchmaking = steamapicontext.SteamMatchmaking()
        if not matchmaking:
            return

        self.AddLobbyListFilters()

        steamapi_call = matchmaking.RequestLobbyList()
        if steamapi_call != k_uAPICallInvalid:
            callback = WebLobbyMatchListCallResult(steamapi_call)
            callback.webview = self
            callback.callbackid = callbackid
            self.lobby_list_callback = callback
        else:
            PrintWarning('listchatlobbies: Failed to make lobby list request\n')

    @jsbind()
    def launch(self, methodargs):
        """ Launch the game.

        Validates the settings.
        """
        if not self.is_lobby_owner:
            return

        # state of lobby should be correct
        if self.lobby_state != 'lobbying':
            PrintWarning('launch: lobby is not lobbying. Cannot launch game.\n')
            return

        # Make sure slots are valid at this point
        self.settings_info.ValidateSlots()

        # Selected game mode should exist
        info = dbgamerules.get(self.data_model.mode, None)
        if not info:
            self.SendLobbyChatMsg('globalmsg', 'Found an invalid game mode')
            return

        # Custom validation by the game mode
        success, error_msg = info.ValidateGameSettings(self.data_model.session_data)
        if not success:
            self.SendLobbyChatMsg('globalmsg', error_msg)
            return

        # Everybody should be ready
        if not self.is_offline_lobby and not self.settings_info.IsEverybodyReady():
            self.SendLobbyChatMsg('globalmsg', 'Some players are not ready yet.')
            return

        self.data_model.UpdateSessionPlayersFromSlots(self.settings_info)

        matchmaking.matchsession.Command(KeyValues("Start"))

    @jsbind()
    def setlobbyname(self, methodargs):
        if not self.is_lobby_owner:
            return
        data_model = self.data_model
        if not data_model:
            return
        data_model.name = methodargs[0]

    @jsbind()
    def setlobbytype(self, methodargs):
        # TODO check for offline lobby?
        # if self.isofflinelobby:
        #    return  # Not changable in offline mode
        if not self.is_lobby_owner:
            return
        data_model = self.data_model
        if not data_model:
            return
        data_model.access = methodargs[0]

    @jsbind()
    def refreshnumgames(self, methodargs):
        pass # TODO

    @jsbind()
    def setSetting(self, methodargs):
        """ Sets a lobby settings.

            Always executed by the lobby owner. No-op in case of other players.
        """
        key = methodargs[0]
        value = methodargs[1]
        self.SetDataModelSetting(key, value)

    @jsbind()
    def setCustomField(self, methodargs):
        """ Sets a game mode custom field. """
        self.data_model.SetCustomLobbyData(methodargs[0], methodargs[1])

    @jsbind()
    def invite(self, methodargs):
        steamapicontext.SteamFriends().ActivateGameOverlay("LobbyInvite")

    @jsbind()
    def sendchatmessage(self, methodargs):
        self.SendLobbyChatMsg('chat %s' % (methodargs[0]))

    @jsbind()
    def setPlayerData(self, methodargs):
        """ Sets a player specific setting.

            This is broadcast to all players in the lobby through chat.
            The lobby owner will pick this up, validate the setting and apply
            it to the lobby data.
        """
        key = methodargs[0]
        value = methodargs[1]
        slotid = methodargs[2]
        self.SendLobbyChatMsg('player', '%s=%s %s' % (key, value, slotid))

    @jsbind()
    def requestSlot(self, methodargs):
        """ Request for taking a slot.

            This is broadcast to all players in the lobby through chat.
            The lobby owner will pick this up, validate the setting and apply
            it to the lobby data.
        """
        slot = methodargs[0]
        self.SendLobbyChatMsg('slot', slot)

    @jsbind()
    def addCPU(self, methodargs):
        if not self.is_lobby_owner:
            return
        slot = methodargs[0]
        self.settings_info.AddCPUToSlot(slot)

    @jsbind()
    def removeCPU(self, methodargs):
        if not self.is_lobby_owner:
            return
        slot = methodargs[0]
        self.settings_info.RemoveCPUFromSlot(slot)

    @jsbind()
    def goSpectate(self, methodargs):
        self.SendLobbyChatMsg('spectate')

    @jsbind()
    def kickPlayer(self, methodargs):
        if not self.is_lobby_owner:
            return
        slot_id = methodargs[0]
        slot = self.data_model.slots[slot_id]

        player_data = slot.get('player', None)
        if player_data is not None:
            command = KeyValues('Kick')
            command.SetUint64('xuid', int(player_data['steamid']))
            matchmaking.matchsession.Command(command)

            self.SendLobbyChatMsg('kicked_player', '%s_%s' % (player_data['steamid'], player_data['name']))
