from srcbuiltins import KeyValues, KeyValuesDumpAsDevMsg, Color
import matchmaking
from steam import CSteamID, steamapicontext
import random

from core.factions import GetFactionInfo


class GameLobbyDataModel(object):
    """ Wraps around the session data.

    Provides convenient setters and getters for the common lobby data.
    """

    @property
    def session_data(self):
        """ Returns the session data.

        TODO: cache and update when session data changes.
        """
        return matchmaking.matchsession.GetSessionSettings()

    @property
    def session_system_data(self):
        """ Returns the session system data.

        TODO: cache and update when session system data changes.
        """
        return matchmaking.matchsession.GetSessionSystemData()

    def _update_session_game_data(self, key, value):
        """ Convenience method for setting a key value in the game sub key of session data. """
        update = KeyValues('update')
        update.SetString('update/game/%s' % key, value)
        matchmaking.matchsession.UpdateSessionSettings(update)

    @property
    def access(self):
        """" Returns type of access for this lobby/session.

        Expected values are "public", "friends" and "private".
        """
        return self.session_data.GetString('system/access')

    @access.setter
    def access(self, value):
        """ Change access for this lobby/session. """
        matchmaking.matchsession.UpdateSessionSettings(KeyValues.FromString('update', '''
        update {
            system {
                access %s
            }
        }''' % value))

    @property
    def name(self):
        """" Name of lobby"""
        return self.session_data.GetString('game/name')

    @name.setter
    def name(self, value):
        """ Set name of lobby """
        self._update_session_game_data('name', value)

    @property
    def match_uuid(self):
        """ uuid of match used for history. """
        return self.session_data.GetString('game/match_uuid')

    @match_uuid.setter
    def match_uuid(self, value):
        """ Set uuid of match used for history. """
        self._update_session_game_data('match_uuid', value)

    @property
    def teamsetup(self):
        """ Get team setup. FFA, 1vs1, 2vs2, etc """
        return self.session_data.GetString('game/teamsetup')

    @teamsetup.setter
    def teamsetup(self, value):
        """ Set team setup. FFA, 1vs1, 2vs2, etc """
        self._update_session_game_data('teamsetup', value)

    @property
    def map(self):
        """ Get the map to be played. """
        return self.session_data.GetString('game/bspname')

    @map.setter
    def map(self, value):
        """ Set the map to be played. """
        self._update_session_game_data('bspname', value)

    @property
    def mode(self):
        """ Get mode. Annihilation, Overrun, etc """
        return self.session_data.GetString('game/mode')

    @mode.setter
    def mode(self, value):
        """ Set mode. Annihilation, Overrun, etc """
        self._update_session_game_data('mode', value)

    @property
    def numslots(self):
        return self.session_data.GetInt('game/slots/numSlots')

    @property
    def numtakenslots(self):
        return self.session_data.GetInt('game/slots/numTakenSlots')

    @property
    def slots(self):
        """ Returns slots with player information, like faction and team selection. """
        slots = []
        local_steamid = str(steamapicontext.SteamUser().GetSteamID())

        local_user_is_lobby_owner = local_steamid == self.session_system_data.GetString('xuidHost')

        for i in range(0, self.numslots):
            slot_data = self.session_data.FindKey('game/slots/slot%d' % i)
            if not slot_data:
                PrintWarning('Missing slot data in session!\n')
                KeyValuesDumpAsDevMsg(self.session_data)
                continue

            steamid = slot_data.GetString('steamid', '')
            is_cpu = slot_data.GetBool('iscpu', False)
            editable = False

            if steamid or is_cpu:
                player_data = self.session_data.FindKey('game/slots/player_%s' % steamid) if not is_cpu else slot_data
                if player_data:
                    islocaluser = local_steamid == steamid and not is_cpu
                    is_lobby_leader = steamid == self.session_system_data.GetString('xuidHost')

                    player_slot_data = {
                        'steamid': steamid,
                        'name': steamapicontext.SteamFriends().GetFriendPersonaName(
                            CSteamID(int(steamid))) if not is_cpu else 'CPU #%d' % i,
                        'faction': player_data.GetString('faction'),
                        'color': player_data.GetString('color'),
                        'ready': bool(int(player_data.GetString('ready') or '0')),
                        'islocaluser': islocaluser,
                        'isLobbyLeader': is_lobby_leader,
                    }

                    editable = islocaluser or (local_user_is_lobby_owner and is_cpu)

                    # Do some enriching for the js code
                    if player_slot_data['faction'] == '__random__':
                        player_slot_data['factionname'] = 'Random'
                    else:
                        faction_info = GetFactionInfo(player_slot_data['faction'])
                        player_slot_data['factionname'] = faction_info.displayname if faction_info else player_slot_data['faction']
                else:
                    player_slot_data = None
            else:
                player_slot_data = None

            slots.append({
                'slotid': i,
                'type': slot_data.GetString('type', 'open'),
                'team': slot_data.GetInt('team', 0),
                'availablepositions': set(map(int, slot_data.GetString('availablepositions', '2').split(','))),

                'iscpu': is_cpu,
                'cputype': slot_data.GetString('cputype', 'cpu_wars_default'),
                'difficulty': slot_data.GetString('difficulty', 'easy'),

                'player': player_slot_data,

                'editable': editable,
            })

        return slots

    @slots.setter
    def slots(self, slots):
        """ Update slots with player information, like faction and team selection. """
        num_taken_slots = 0

        slots_update_data = ''

        for slot_idx, slot_data in enumerate(slots):
            # Keep track of slots taken by a player (either human or cpu)
            if slot_data['type'] == 'player':
                num_taken_slots += 1

            # Parse data for when a player (cpu or human) has taken the slot
            player_data = slot_data.get('player', None)
            if player_data:
                steamid = player_data['steamid']
                player_update_data = '''
                    faction %(faction)s
                    color %(color)s
                    ready #int#%(ready)s
                ''' % {
                    'faction': player_data['faction'],
                    'color': player_data['color'],
                    'ready': str(int(player_data['ready'])),
                }
            else:
                steamid = ''
                player_update_data = None

            # For cpu, faction/color/ready is stored in slots
            # For humans it's stored by steamid, so choice is remembered when changing slots.
            slots_update_data += '''slot%(slot_idx)d {
                type %(type)s
                team #int#%(team)s
                availablepositions %(availablepositions)s
                iscpu #int#%(iscpu)s
                cputype %(cputype)s
                difficulty %(difficulty)s
                steamid %(steamid)s%(player)s
            }
            ''' % {
                'slot_idx': slot_idx,
                'type': slot_data['type'],
                'team': slot_data['team'],
                'availablepositions': ','.join(map(str, slot_data['availablepositions'])),

                'iscpu': '1' if slot_data['iscpu'] else '0',
                'cputype': slot_data['cputype'],
                'difficulty': slot_data['difficulty'],

                'player': player_update_data if slot_data['iscpu'] else ' #empty# ',

                'steamid': steamid or ' #empty# ',
            }

            if player_update_data and not slot_data['iscpu']:
                slots_update_data += '''player_%s {
                    %s
                }
                ''' % (steamid, player_update_data)

        matchmaking.matchsession.UpdateSessionSettings(KeyValues.FromString('update', '''
        update {
            game {
                slots {
                    numSlots %(numSlots)d
                    numTakenSlots %(numTakenSlots)d
                    %(slots)s
                }
            }
        }
        ''' % {
            'numSlots': len(slots),
            'numTakenSlots': num_taken_slots,
            'slots': slots_update_data,
        }))

    def SetPlayerLobbyData(self, steamid, key, value):
        """ Update slot player data. """
        matchmaking.matchsession.UpdateSessionSettings(KeyValues.FromString('update', '''
        update {
            game {
                slots {
                    player_%s {
                        %s %s
                    }
                }
            }
        }''' % (steamid, key, value)))

    def SetSlotLobbyData(self, idx, key, value):
        """ Update slot data. """
        matchmaking.matchsession.UpdateSessionSettings(KeyValues.FromString('update', '''
        update {
            game {
                slots {
                    slot%s {
                        %s %s
                    }
                }
            }
        }''' % (idx, key, value)))

    def SetCustomLobbyData(self, key, value):
        matchmaking.matchsession.UpdateSessionSettings(KeyValues.FromString('update', '''
        update {
            game {
                custom {
                    %s %s
                }
            }
        }''' % (key, value)))

    def GetCustomLobbyData(self, key):
        return self.session_data.GetString('game/custom/%s' % key, None)

    def UpdateSessionPlayersFromSlots(self, settings_info):
        """ Creates player keys from slots.

        May also do some post processing.
        This method should be called just before launching the game.
        """
        steamfriends = steamapicontext.SteamFriends()
        game_update_data = ''
        num_players = 0
        slots = self.slots
        for idx, slot in enumerate(slots):
            type = slot['type']
            if type not in ['player']:
                continue

            player_update_data = ''

            player_update_data += 'team #int#%s\n' % slot['team']
            player_update_data += 'availablepositions %s\n' % ','.join(map(str, slot['availablepositions']))

            player_update_data += 'iscpu #int#%s\n' % int(slot['iscpu'])
            player_update_data += 'cputype %s\n' % slot['cputype']
            player_update_data += 'difficulty %s\n' % slot['difficulty']

            if type == 'player':
                playerdata = slot['player']
                if not slot['iscpu']:
                    user_steamid = CSteamID(int(playerdata['steamid']))
                    player_update_data += 'steamid %s\n' % playerdata['steamid']
                    player_update_data += 'playername %s\n' % steamfriends.GetFriendPersonaName(user_steamid)
                else:
                    player_update_data += 'steamid %s\n' % playerdata.get('steamid', '#empty#')
                    player_update_data += 'playername %s\n' % 'CPU #%d' % idx

                faction = playerdata['faction']
                if faction == '__random__':
                    settings_factions = set(settings_info.availablefactions.keys())
                    settings_factions.discard('__random__')
                    faction = random.sample(settings_factions, 1)[0]

                player_update_data += 'faction %s\n' % faction
                color = settings_info.allcolors.get(playerdata['color'], {'color': Color(0, 255, 0, 255)})['color']
                player_update_data += 'color %d_%d_%d_%d\n' % (color.r(), color.g(), color.b(), color.a())

            game_update_data += '''player%d {
                %s
            }
            ''' % (num_players, player_update_data)
            num_players += 1

        matchmaking.matchsession.UpdateSessionSettings(KeyValues.FromString('update', '''
        update {
            game {
                numplayers #int#%s
                %s
            }
        }''' % (num_players, game_update_data)))

    def IsLobbyLeaderReady(self):
        slots = self.slots

        for slot in slots:
            playerdata = slot['player']
            if not playerdata or slot['iscpu'] or not playerdata['isLobbyLeader']:
                continue
            return playerdata['ready']
        return True # Spectator for example

