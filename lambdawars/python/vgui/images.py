""" Small module that used to store bitmap images. Probably not needed anymore, since we no longer need vgui. Just
 stores the uri to the shared assets folder now. """
import os

imagedb = {}


def GetImage(path):
    if not path:
        return None
    if path in imagedb:
        return imagedb[path]
    imagedb[path] = 'local://localhost/ui/assets/icons/%s.png' % os.path.splitext(path)[0]
    return imagedb[path]
