from cef import CefPanel, jsbind
from gameinterface import concommand
from input import ButtonCode

from core.abilities import GetAbilityInfo
from core.dispatch import receiver
from core.signals import gamepackageloaded, gamepackageunloaded

from fields import BaseField

from core.attributemgr_shared import IsAttributeFiltered

from operator import itemgetter

# Helper
def ShowTool(tool, show):
    if not show:
        tool.SetVisible(False)
        tool.SetEnabled(False)  
    else:
        tool.SetVisible(True)
        tool.SetEnabled(True)   
        tool.RequestFocus()
        tool.MoveToFront()
        
@receiver(gamepackageloaded)
@receiver(gamepackageunloaded)
def OnGamePackageChanged(sender, packagename, **kwargs):
    unitpanel.OnGamePackageChanged(packagename)
    abilitypanel.OnGamePackageChanged(packagename)
    attributemodifiertool.OnGamePackageChanged(packagename)


# 
# New CEF panels
#
class CefUnitPanel(CefPanel):
    classidentifier = 'ToolsUnitPanel'

    def GetConfig(self):
        ''' Dictionary passed as config to javascript, used for initializing. '''
        config = super(CefUnitPanel, self).GetConfig()
        config['units'] = self.BuildUnitList()
        return config
        
    def OnGamePackageChanged(self, package_name):
        if not self.isloaded:
            return
        self.LoadUnits()
    
    def SetEnabled(self, enabled):
        pass
    def RequestFocus(self):
        pass
    def MoveToFront(self):
        pass
        
    def BuildUnitList(self):
        from core.units.info import dbunits
        return [unit.name for unit in dbunits.values() if not unit.hidden]
        
    def LoadUnits(self):
        self.Invoke(self.component, "clearList", [])
        units = self.BuildUnitList()
        self.Invoke(self.component, "addUnits", [units])
        
    def KeyInput(self, down, keynum, currentbinding):
        if not self.visible:
            return 1

        if keynum == ButtonCode.KEY_SPACE:
            if down:
                self.Invoke(self.component, "onSpacePressed", [])
                self.RegisterTickSignal(0.08)
                self.startrepeattime = gpGlobals.curtime + 0.4
            else:
                self.UnregisterTickSignal()
            return 0
        return 1
        
    def OnTick(self):
        if self.startrepeattime > gpGlobals.curtime:
            return
        # Tick method only used for spawning when holding down the key
        self.Invoke(self.component, "onSpacePressed", [])
        
    startrepeattime = 0
    
unitpanel = CefUnitPanel('unitpanel')

@concommand('unitpanel', 'Show up a panel to create units (cheats)', 0)
def cc_show_unitpanel(args):
    unitpanel.visible = not unitpanel.visible


class CefAbilityPanel(CefPanel):
    classidentifier = 'ToolsAbilityPanel'
    
    def GetConfig(self):
        ''' Dictionary passed as config to javascript, used for initializing. '''
        config = super().GetConfig()
        config['abilities'] = self.BuildAbilitiesList()
        return config

    def OnGamePackageChanged(self, package_name):
        if not self.isloaded:
            return
        self.LoadAbilities()
    
    def BuildAbilitiesList(self):
        from core.abilities.info import dbabilities
        return [abi.name for abi in dbabilities.values() if not abi.hidden]
        
    def LoadAbilities(self):
        self.Invoke(self.component, "clearList", [])
        abilities = self.BuildAbilitiesList()
        self.Invoke(self.component, "addAbilities", [abilities])
        
    def KeyInput(self, down, keynum, currentbinding):
        if not self.visible:
            return 1

        if keynum == ButtonCode.KEY_SPACE:
            if down:
                self.Invoke(self.component, "onSpacePressed", [])
                self.RegisterTickSignal(0.08)
                self.startrepeattime = gpGlobals.curtime + 0.4
            else:
                self.UnregisterTickSignal()
            return 0
        return 1
        
    def OnTick(self):
        if self.startrepeattime > gpGlobals.curtime:
            return
        # Tick method only used for spawning when holding down the key
        self.Invoke(self.component, "onSpacePressed", [])
        
    startrepeattime = 0


abilitypanel = CefAbilityPanel('abilitypanel')


@concommand('abilitypanel', 'Show up a panel to create abilities (cheats)', 0)
def cc_show_abilitypanel(args):
    abilitypanel.visible = not abilitypanel.visible


class CefAttributePanel(CefPanel):
    classidentifier = 'ToolsAttributePanel'

    def GetConfig(self):
        ''' Dictionary passed as config to javascript, used for initializing. '''
        config = super().GetConfig()
        config['abilities'] = self.BuildAbilitiesList()
        return config
        
    def OnGamePackageChanged(self, package_name):
        if not self.isloaded:
            return
        self.LoadAbilities()

    def BuildAbilitiesList(self):
        from core.abilities.info import dbabilities
        data = []
        for abi in dbabilities.values():
            data.append({
                'name': abi.name,
            })
        data.sort(key=itemgetter('name'))
        return data
        
    def LoadAbilities(self):
        self.Invoke(self.component, "clearList", [])
        abilities = self.BuildAbilitiesList()
        self.Invoke(self.component, "addAbilities", [abilities])
        
    def ClearAttributes(self, tabname):
        self.Invoke(self.component, "clearAttrList", [tabname])
        
    def SetAttribute(self, tabname, fieldname, value):
        self.Invoke(self.component, "addAttribute", [tabname, {
            'attribute': fieldname,
            'value': str(value)
        }])
        
    def LoadAttributesFromObject(self, object, filterflags=0):
        attributes = []
        for k, v in object.__dict__.iteritems():
            if not isinstance(v, BaseField):
                continue
            if IsAttributeFiltered(k, v, filterflags):
                continue
            attributes.append({
                'attribute': v.name,
                'value': str(v.Get(object, allowdefault=True)),
            })
        return attributes

    @jsbind(hascallback=True)
    def getAttributes(self, methodargs):
        abiname = methodargs[0]
        abiinfo = GetAbilityInfo(abiname)
        if not abiinfo:
            return []
        
        return self.LoadAttributesFromObject(abiinfo)
        
attributemodifiertool = CefAttributePanel('attributepanel')

@concommand('attributemodifiertool', 'Show a panel to modify attributes of an unit class or instance', 0)
def show_attributemodifiertool(args):
    attributemodifiertool.visible = not attributemodifiertool.visible
