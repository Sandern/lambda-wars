""" Create and store cursors."""
from gameui import CreateCursorFromFile

cursordb = {}


def GetCursor(path):
    cursor = cursordb.get(path, None)
    if cursor:
        return cursor
        
    cursordb[path] = CreateCursorFromFile(path)
    return cursordb[path]
