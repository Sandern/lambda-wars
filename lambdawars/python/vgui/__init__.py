# Deprecated module

# from _vlocalize import localize
from utils import ScreenWidth, ScreenHeight

def XRES(x): return int( x  * ( ScreenWidth() / 640.0 ) )
def YRES(y): return int( y  * ( ScreenHeight() / 480.0 ) )

def vgui_input():
    return None

# Stubs
class CHudElementHelper(object):
	def __init__(self, *args, **kwargs):
		pass

class HudIconsStub(object):
	@staticmethod
	def GetIcon(cls):
		return 'todo'

def HudIcons():
	return HudIconsStub

class CHudElement(object):
	def __init__(self, *args, **kwargs): pass

scheme = 'todo'
surface = 'todo' 
FontDrawType_t = 'todo'
FontVertex_t = 'todo'
DataType_t = 'todo'

ipanel = 'ipanel'

class ClientMode(object):
	@staticmethod
	def GetViewport(): return 'viewport'

def GetClientMode():
	return ClientMode

def AddTickSignal(*args, **kwargs):
	pass
def RemoveTickSignal(*args, **kwargs):
	pass

def vgui_system():
	return 'todo'