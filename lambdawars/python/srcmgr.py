"""
Provides general information and intializes the Python side of the game.
"""
import sys
import gc
import os
import pkgutil
import traceback
import imp
import queue

from srcbuiltins import RegisterTickMethod
from gameinterface import CommandLine
import filesystem as fs
from kvdict import LoadFileIntoDictionaries

# Global indicating the current version of Lambda Wars
# MACRO / MINOR / MICRO
VERSION = (2, 4, 0)
DEVVERSION = None

# Gameinfo
gameinfo = {}
steamappid = None
moddirectory = None

# Level related globals
levelname = ""
levelinit = False
levelpreinit = False

revisioninfopath = os.path.join(os.path.split(__file__)[0], 'srcmgr.info')
versioninfopath = os.path.join(os.path.split(__file__)[0], 'version.info')


def GetModDirectory():
    try:
        moddir = CommandLine().ParmValue("-game", CommandLine().ParmValue("-defaultgamedir", "lambdawars"))
        if '/' in moddir or '\\' in moddir:
            moddir = os.path.dirname(moddir)
    except UnicodeDecodeError:
        moddir = 'lambdawars'
    return moddir


def _Init():
    """ Initialize """
    global gameinfo, steamappid, moddirectory, DEVVERSION
    
    # Load gameinfo
    gameinfo = LoadFileIntoDictionaries('gameinfo.txt')
    steamappid = int(gameinfo['FileSystem']['SteamAppId'])
    
    # In the dev version, the game directory might be different
    moddirectory = GetModDirectory()
    if isclient:
        from _vlocalize import localize
        if moddirectory != 'lambdawars':
            localize.AddFile("Resource/lambdawars_%language%.txt", "MOD", True)


# Dealing with threads
threadcallbacks = queue.Queue()


def DoThreadCallback(method, args):
    threadcallbacks.put((method, args))


def CheckThreadsCallbacks():
    while not threadcallbacks.empty():
        callback = threadcallbacks.get_nowait()
        if callback:
            try:
                callback[0](*callback[1])
            except:
                traceback.print_exc()


RegisterTickMethod(CheckThreadsCallbacks, 0.2)


# Level init and shutdown methods
def _LevelInitPreEntity(lvlname):
    global levelname, levelpreinit
    levelname = lvlname
    
    levelpreinit = True


def _LevelInitPostEntity():
    """ Called when all map entities are created.
    """
    global levelinit

    # Set level init to true
    levelinit = True


def _LevelShutdownPreEntity():
    """ Called before the entities are removed from the map.
        Dispatches related callbacks.
    """
    pass


def _LevelShutdownPostEntity():
    """ Called when all entities are removed.
        Dispatches related callbacks. """    
    global levelpreinit, levelinit, levelname

    # Put levelinit to false
    levelpreinit = False
    levelinit = False
    levelname = ""
   
    # Cleanup memory
    gc.collect()


# Temporary signal methods for c++
def _CheckReponses(responses):
    for r in responses:
        if isinstance(r[1], Exception):
            PrintWarning('Error in receiver %s (module: %s): \n%s' %
                         (r[0], r[0].__module__, r[2]))


def _CallSignal(method, kwargs):
    _CheckReponses(method(**kwargs))


# Useful methods        
def VerifyIsClient():
    """ Throws an exception if this is not the client. To be used when importing modules. """
    if not isclient:
        raise ImportError('Cannot import this module on the server')


def VerifyIsServer():
    """ Throws an exception if this is not the client. To be used when importing modules. """
    if not isserver:
        raise ImportError('Cannot import this module on the client')


def ImportSubMods(mod):
    """ Import all sub modules for the specified module. """
    name = mod.__name__
    path = mod.__path__
    pathrel = []
    for v in path:
        pathrel.append(os.path.normpath(
            fs.FullPathToRelativePath(os.path.normpath(v), defaultvalue=v) if fs.IsAbsolutePath(v) else v
        ))
        
    for item in pkgutil.iter_modules(pathrel):
        submod = '%s.%s' % (name, item[1])
        try:
            __import__(submod)   
            sys.modules[submod]
        except:
            traceback.print_exc()


def ReloadSubMods(mod, exludelist=None):
    """ Reloads all sub modules for the specified module. """
    name = mod.__name__
    path = mod.__path__
    pathrel = []
    for v in path:
        pathrel.append(os.path.normpath(
            fs.FullPathToRelativePath(os.path.normpath(v)) if fs.IsAbsolutePath(v) else v
        ))
        
    for item in pkgutil.iter_modules(pathrel):
        submod = '%s.%s' % (name, item[1])
        try:
            __import__(submod)  # Might not be imported in the first place because the module is just added
            if not exludelist or submod not in exludelist:
                imp.reload(sys.modules[submod])
        except:
            traceback.print_exc()
