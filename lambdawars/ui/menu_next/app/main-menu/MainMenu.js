/**
 * Defines the main menu routes & navigation buttons
 */
import React from 'react';
import {
    Switch,
    Route,
    useRouteMatch,
  } from "react-router-dom";
import {FormattedMessage} from 'react-intl';

import { MainMenuList } from './header/MainMenuList';
import { LogoLink } from './header/LogoLink';
import { MenuLink } from './header/MenuLink';
import { PlayerInfo } from './header/PlayerInfo';

import { useObservable } from '../observable-hook';
import { getStoreObservableAt, GENERAL_USER_INFO, GENERAL_USER_STATE } from '../gameui-store';

import { Content } from './common/Content';
import { ContentWithGlobalChat } from './common/ContentWithGlobalChat';

import { News } from './news/News';
import { MultiPlayerMenu } from './multiplayer/MultiPlayerMenu';
import { SinglePlayerMenu } from './singleplayer/SinglePlayerMenu';
import { Missions } from './missions/Missions';
import { InGameMenu } from './ingame/InGameMenu';
import { Settings } from './settings/Settings';
import { Profile } from './profile/Profile';
import { OnlineGamesFinder } from './online-games-finder/OnlineGamesFinder';
import { GameLobby } from './game-lobby/GameLobby';

import logoImg from '../images/lw-logo.png';
import styled from 'styled-components';

const HeaderTopBar = styled.header`
    width: 100%;
    flex: 0 1 auto;
    background-color: var(--secondary-theme-color-op90);
`;

const MainMenuContent = styled.div`
    display: flex;
    flex-flow: column;
    height: 100%;
`;

export function MainMenu() {
    const { path, url } = useRouteMatch();

    const userInfo = useObservable(getStoreObservableAt(GENERAL_USER_INFO), {});
    const userState = useObservable(getStoreObservableAt(GENERAL_USER_STATE), {});

    return (<MainMenuContent>
        <HeaderTopBar className="noselect">
            <LogoLink to="/"><img src={logoImg} /></LogoLink>

            <div className="inner">
                <MainMenuList>
                    { !userState.inGame && userInfo.steamAvailable ? <li>
                        <MenuLink to={`${url}/multiplayer`}><FormattedMessage id="MenuOnline"></FormattedMessage></MenuLink>
                    </li> : undefined }
                    { userState.inGame && userInfo.steamAvailable ? <li>
                        <MenuLink to={`${url}/in-game`}><FormattedMessage id="MenuInGame"></FormattedMessage></MenuLink>
                    </li> : undefined }
                    { !userState.inGame && userInfo.steamAvailable ? <li>
                        <MenuLink to={`${url}/singleplayer`}><FormattedMessage id="MenuOffline"></FormattedMessage></MenuLink>
                    </li> : undefined }
                    <li>
                        <MenuLink to={`${url}/settings`}><FormattedMessage id="MenuSettings"></FormattedMessage></MenuLink>
                    </li>
                    <li>
                        <MenuLink to={`${url}/`} onClick={() => gameui.clientcommand('quit')}><FormattedMessage id="MenuQuit"></FormattedMessage></MenuLink>
                    </li>
                </MainMenuList>
            </div>

            <PlayerInfo></PlayerInfo>
        </HeaderTopBar>

        <Switch>
            <Route exact path={`${path}`}>
                <ContentWithGlobalChat><News></News></ContentWithGlobalChat>
            </Route>
            <Route path={`${path}/multiplayer`}>
                <Content><MultiPlayerMenu></MultiPlayerMenu></Content>
            </Route>
            <Route path={`${path}/singleplayer`}>
                <Content><SinglePlayerMenu></SinglePlayerMenu></Content>
            </Route>
            <Route path={`${path}/in-game`}>
                <Content><InGameMenu></InGameMenu></Content>
            </Route>
            <Route path={`${path}/settings`}>
                <Content><Settings></Settings></Content>
            </Route>
            <Route path={`${path}/profile`}>
                <Content><Profile></Profile></Content>
            </Route>
            <Route path={`${path}/missions`}>
                <Content><Missions></Missions></Content>
            </Route>
            <Route path={`${path}/online-games-finder`}>
                <Content><OnlineGamesFinder></OnlineGamesFinder></Content>
            </Route>
            <Route path={`${path}/game-lobby`}>
                <Content><GameLobby></GameLobby></Content>
            </Route>
        </Switch>
    </MainMenuContent>);
}
