import React, { useState } from 'react';
import styled from 'styled-components';

const emptyCharacter = '⠀';

const HotkeyRow = styled.div`
    margin: 0.5em 0 0.5em 0;
    background-color: ${({ active }) => active ? '#6F6F6F' : 'inherit'};
    :hover {
        background-color: ${({ active }) => active ? '#6F6F6F' : '#4F4F4F'};
    }
`;

const HotkeyNameCol = styled.div`
    display: inline-block;
    background-color: rgba(40, 40, 40, 0.5);
    width: 50%;
`;

const HotkeyName = styled.p`
    margin: 0.1em;
    padding: 0.1em;
    width: 100%;
    display: inline-block;
`;

const HotkeyKeyCol = styled.div`
    display: inline-block;
    background-color: rgba(65, 65, 65, 0.5);
    width: 50%;
`;

const HotkeyKey = styled.p`
    text-align: center;
    margin: 0.1em;
    padding: 0.1em;
    width: 100%;
    display: inline-block;
`;

/**
 * Handles a hotkey binding.
 * When clicking a row, the input will be redirected.
 * On next key press or mouse click, the button will be bound and the input is released again.
 * The game will push an update with hotkeys to the UI.
 */
export function HotkeyBinding(props) {
    const { hotkey } = props;
    const [isCapturing, setIsCapturing] = useState(false);

    return <HotkeyRow active={isCapturing ? 1 : 0} onClick={(e) => captureInput(e, hotkey, setIsCapturing)}>
        <HotkeyNameCol span={12}>
            <HotkeyName>{hotkey.displayName}</HotkeyName>
        </HotkeyNameCol>
        <HotkeyKeyCol span={12}>
            <HotkeyKey>{isCapturing ? emptyCharacter : hotkey.key || emptyCharacter}</HotkeyKey>
        </HotkeyKeyCol>
    </HotkeyRow>
}

function captureInput(mouseEvent, hotkey, setIsCapturing) {
    console.log(`captureInput:`, mouseEvent.nativeEvent);
    setIsCapturing(true);

    const listeners = {
        keyCapture: (ev) => {
            ev.preventDefault();
            ev.stopImmediatePropagation();

            doBindKeyboardEvent(hotkey, ev);
            
            releaseInput(listeners, setIsCapturing);
        },
        mouseCapture: (ev) => {
            ev.preventDefault();
            ev.stopImmediatePropagation();

            doBindMouseEvent(hotkey, ev);

            // Swallow click event by not releasing it yet for primary mouse button
            if (ev.button !== 0) {
                releaseInput(listeners, setIsCapturing);
            }
            
        },
        swallowListener: (ev) => {
            ev.preventDefault();
            ev.stopImmediatePropagation();
        },
        mouseReleaseListener: (ev) => {
            ev.preventDefault();
            ev.stopImmediatePropagation();
            releaseInput(listeners, setIsCapturing);
        },
    };

    window.addEventListener('keydown', listeners.keyCapture, { capture: true });
    window.addEventListener('mousedown', listeners.swallowListener, { capture: true });
    window.addEventListener('mouseup', listeners.mouseCapture, { capture: true });
    window.addEventListener('click', listeners.mouseReleaseListener, { capture: true });
}

function releaseInput(listeners, setIsCapturing) {
    setIsCapturing(false);

    window.removeEventListener('keydown', listeners.keyCapture, { capture: true });
    window.removeEventListener('mousedown', listeners.swallowListener, { capture: true });
    window.removeEventListener('mouseup', listeners.mouseCapture, { capture: true });
    window.removeEventListener('click', listeners.mouseReleaseListener, { capture: true });
}

function doBindKeyboardEvent(hotkey, ev) {
    // Cannot bind meta (windows key) and escape
    // They will simply cancel the action.
    if (['Meta', 'Escape'].includes(ev.key)) {
        return;
    }
    // Cannot bind toggleconsole to anything other than back quote
    if (ev.key === '`' && hotkey.binding !== 'toggleconsole') {
        console.log(`Can only bind "\`" to "toggleconsole"!`);
        return;
    }

    window.hotkeys.bind(hotkey.binding, hotkey.key, ev.keyCode);
}

const mouseButtonToKey = {
    0: 'MOUSE1', // Left
    1: 'MOUSE3', // Middle
    2: 'MOUSE2', // Right
    3: 'MOUSE4', // Mouse 4
    4: 'MOUSE5', // Mouse 5
}

function doBindMouseEvent(hotkey, ev) {
    console.log('doBindMouseEvent', ev);
    window.hotkeys.mouseBind(hotkey.binding, hotkey.key, mouseButtonToKey[ev.button]);
}
