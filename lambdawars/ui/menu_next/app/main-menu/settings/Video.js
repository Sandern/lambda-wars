import React, { useEffect, useState } from 'react';
import { Select } from '../../main-menu/common/Select';
import { useObservable } from '../../observable-hook';
import { getStoreObservableAt, pushValueToStore, SETTINGS_VIDEO } from '../../gameui-store';
import { FormattedMessage } from 'react-intl';
import styled from 'styled-components';
import { ConfirmModal } from '../common/modals/ConfirmModal';
import { Button } from '../common/Button';

const SettingRow = styled.div`
    margin: 0.5em 0 0.5em 0;
`;

const SettingLabel = styled.div`
    display: inline-block;
    width: 50%;
`;

const SettingControl = styled.div`
    display: inline-block;
    width: 50%;
`;

const SettingsSelect = styled(Select)`
    width: 100%;
`;

const SettingsContainer = styled.div`
    font-size; 0.8em;
`;
const SettingsCard = styled.div`
    padding: 0.5em;
`;

// const dispModeWindowed = 0;
const dispModeFullScreen = 1;
const dispModeFullScreenWindowed = 2;

const aspectRatiosToIndex = [
    4.0 / 3.0,
    16.0 / 9.0,
    16.0 / 10.0,
    1.0,
];

/**
 * Video Settings
 */
export function Video() {
    const [settingsDirty, setSettingsDirty] = useState(false);
    const [discardModalOpen, setDiscardModalOpen] = useState(false);

    const settings = useObservable(getStoreObservableAt(SETTINGS_VIDEO), {});

    useEffect(() => {
        window.video.refreshVideoSettings();
    }, []);

    const updateSettings = (newSettings) => {
        setSettingsDirty(true);
        pushValueToStore(SETTINGS_VIDEO, Object.assign({}, settings, newSettings));
    };

    return <SettingsContainer>
        {renderResolutionCard(settings, updateSettings)}
        {renderAdvancedCard(settings, updateSettings)}
        <div>
            <Button disabled={!settingsDirty} onClick={() => setDiscardModalOpen(true)}><FormattedMessage id="settings_discard_confirm_title"></FormattedMessage></Button>
            <Button disabled={!settingsDirty} onClick={() => window.video.apply(settingsAsList(settings))}><FormattedMessage id="settings_apply_video"></FormattedMessage></Button>
        </div>
        <ConfirmModal
            title={<FormattedMessage id="settings_discard_confirm_title"></FormattedMessage>}
            isOpen={discardModalOpen}
            onConfirm={() => { setSettingsDirty(false); window.video.refreshVideoSettings(); setDiscardModalOpen(false); }}
            onCancel={() => setDiscardModalOpen(false)}
        ></ConfirmModal>
    </SettingsContainer>;
}

function renderResolutionCard(settings, updateSettings) {
    if (!settings.resolution) {
        return;
    }

    const displayMode = settings.displayMode;
    const resolution = settings.resolution;
    const resolutions = selectAndFilterResolutionList(settings);
    const aspectRatio = getAspectRatioIndex(resolution);

    const aspectRatioOptions = [
        { value: 0, label: <FormattedMessage id="SFUI_Settings_Normal"></FormattedMessage> },
        { value: 1, label: <FormattedMessage id="SFUI_Settings_Widescreen_16_9"></FormattedMessage> },
        { value: 2, label: <FormattedMessage id="SFUI_Settings_Widescreen_16_10"></FormattedMessage> },
    ];
    const resolutionOptions = resolutions.map((v) => ({ value: resolutionToKey(v), label: resolutionToKey(v) }));
    const displayOptions = [
        { value: 0, label: <FormattedMessage id="SFUI_Settings_Windowed"></FormattedMessage> },
        { value: 1, label: <FormattedMessage id="SFUI_Settings_Fullscreen"></FormattedMessage> },
        { value: 2, label: <FormattedMessage id="SFUI_Settings_Fullscreen_Windowed"></FormattedMessage> },
    ]

    return (<SettingsCard>
        <h2><FormattedMessage id="settings_video_section"></FormattedMessage></h2>
        <hr />
        <SettingRow>
            <SettingLabel>
                <FormattedMessage id="SFUI_Settings_Aspect_Ratio"></FormattedMessage>
            </SettingLabel>
            <SettingControl>
                <SettingsSelect
                    value={aspectRatioOptions.find(o => o.value === aspectRatio)}
                    isDisabled={displayMode === dispModeFullScreenWindowed}
                    isSearchable={false}
                    onChange={(aspectRatioOption) => { 
                        const newResolution = findClosestResolution(
                            filterResolutions(settings, aspectRatioOption.value),
                            settings.resolution
                        );
                        updateSettings({ resolution: newResolution });
                    } }
                    options={aspectRatioOptions}
                ></SettingsSelect>
            </SettingControl>
        </SettingRow>
        <SettingRow>
            <SettingLabel>
                <FormattedMessage id="SFUI_Settings_Resolution"></FormattedMessage>
            </SettingLabel>
            <SettingControl>
                <SettingsSelect 
                    value={resolutionOptions.find((o) => o.value === resolutionToKey(findClosestResolution(resolutions, resolution)))} 
                    isDisabled={displayMode === dispModeFullScreenWindowed}
                    isSearchable={false}
                    onChange={(resolutionOption) => updateSettings({ resolution: keyToResolution(resolutionOption.value) }) } 
                    options={resolutionOptions}
                ></SettingsSelect>
            </SettingControl>
        </SettingRow>
        <SettingRow>
            <SettingLabel>
                <FormattedMessage id="SFUI_Settings_Display_Mode"></FormattedMessage>
            </SettingLabel>
            <SettingControl>
                <SettingsSelect 
                    value={displayOptions.find((o) => o.value === displayMode)} 
                    isSearchable={false}
                    onChange={(displayOption) => updateSettings({ displayMode: displayOption.value }) } 
                    options={displayOptions}
                ></SettingsSelect>
            </SettingControl>
        </SettingRow>
    </SettingsCard>);
}

function renderAdvancedCard(settings, updateSettings) {
    if (!settings.resolution) {
        return;
    }

    const csmTranslations = {
        0: <FormattedMessage id="SFUI_CSM_Low"></FormattedMessage>,
        1: <FormattedMessage id="SFUI_CSM_Med"></FormattedMessage>,
        2: <FormattedMessage id="SFUI_CSM_High"></FormattedMessage>,
        3: <FormattedMessage id="SFUI_CSM_VeryHigh"></FormattedMessage>,
    };

    const settingLevelsTranslations = {
        0: <FormattedMessage id="SFUI_Settings_Low"></FormattedMessage>,
        1: <FormattedMessage id="SFUI_Settings_Medium"></FormattedMessage>,
        2: <FormattedMessage id="SFUI_Settings_High"></FormattedMessage>,
        3: <FormattedMessage id="SFUI_Settings_Very_High"></FormattedMessage>,
    };

    const csmOptions = [
        { value: 0, label: csmTranslations[0] },
        { value: 1, label: csmTranslations[1] },
        { value: 2, label: csmTranslations[2] },
        { value: 3, label: csmTranslations[3] },
        { value: 9999999, label: <span><FormattedMessage id="SFUI_Settings_Choice_Autodetect"></FormattedMessage>:{csmTranslations[settings.csm_quality_level_auto]}</span> },
    ];

    const gpuMemLevelOptions = [
        { value: 0, label: settingLevelsTranslations[0] },
        { value: 1, label: settingLevelsTranslations[1] },
        { value: 2, label: settingLevelsTranslations[2] },
        { value: 9999999, label: <span><FormattedMessage id="SFUI_Settings_Choice_Autodetect"></FormattedMessage>:{settingLevelsTranslations[settings.gpu_mem_level_auto]}</span> },
    ];

    const cpuLevelOptions = [
        { value: 0, label: settingLevelsTranslations[0] },
        { value: 1, label: settingLevelsTranslations[1] },
        { value: 2, label: settingLevelsTranslations[2] },
        { value: 9999999, label: <span><FormattedMessage id="SFUI_Settings_Choice_Autodetect"></FormattedMessage>:{settingLevelsTranslations[settings.cpu_level_auto]}</span> },
    ];

    const gpuLevelOptions = [
        { value: 0, label: settingLevelsTranslations[0] },
        { value: 1, label: settingLevelsTranslations[1] },
        { value: 2, label: settingLevelsTranslations[2] },
        { value: 3, label: settingLevelsTranslations[3] },
        { value: 9999999, label: <span><FormattedMessage id="SFUI_Settings_Choice_Autodetect"></FormattedMessage>:{settingLevelsTranslations[settings.gpu_level_auto]}</span> },
    ];

    return (<SettingsCard>
        <h2><FormattedMessage id="settings_video_advanced_section"></FormattedMessage></h2>
        <hr />
        <SettingRow>
            <SettingLabel>
                <FormattedMessage id="SFUI_Settings_CSM"></FormattedMessage>
            </SettingLabel>
            <SettingControl>
                <SettingsSelect 
                    value={csmOptions.find((o) => o.value === settings.csm_quality_level)} 
                    isSearchable={false}
                    onChange={(csmOption) => updateSettings({ csm_quality_level: csmOption.value }) } 
                    options={csmOptions}
                ></SettingsSelect>
            </SettingControl>
        </SettingRow>
        <SettingRow>
            <SettingLabel>
                <FormattedMessage id="SFUI_Settings_Model_Texture_Detail"></FormattedMessage>
            </SettingLabel>
            <SettingControl>
                <SettingsSelect 
                    value={gpuMemLevelOptions.find((o) => o.value === settings.gpu_mem_level)} 
                    isSearchable={false}
                    onChange={(gpuMemLevelOption) => updateSettings({ gpu_mem_level: gpuMemLevelOption.value }) } 
                    options={gpuMemLevelOptions}
                ></SettingsSelect>
            </SettingControl>
        </SettingRow>
        <SettingRow>
            <SettingLabel>
                <FormattedMessage id="SFUI_Settings_Effect_Detail"></FormattedMessage>
            </SettingLabel>
            <SettingControl>
                <SettingsSelect 
                    value={cpuLevelOptions.find((o) => o.value === settings.cpu_level)} 
                    isSearchable={false}
                    onChange={(cpuLevelOption) => updateSettings({ cpu_level: cpuLevelOption.value }) } 
                    options={cpuLevelOptions}
                ></SettingsSelect>
            </SettingControl>
        </SettingRow>
        <SettingRow>
            <SettingLabel>
                <FormattedMessage id="SFUI_Settings_Shader_Detail"></FormattedMessage>
            </SettingLabel>
            <SettingControl>
                <SettingsSelect 
                    value={gpuLevelOptions.find((o) => o.value === settings.gpu_level)} 
                    isSearchable={false}
                    onChange={(gpuLevelOption) => updateSettings({ gpu_level: gpuLevelOption.value }) } 
                    options={gpuLevelOptions}
                ></SettingsSelect>
            </SettingControl>
        </SettingRow>
    </SettingsCard>);
}

// Temporary until we support sending objects as data to Python
// Currently it creates a ref, however that is still used in few places but we can do without...
function settingsAsList(settings) {
    return [
        ['displayMode', settings.displayMode],
        ['resolution', [settings.resolution.width, settings.resolution.height]],

        ['csm_quality_level', settings.csm_quality_level],
        ['gpu_mem_level', settings.gpu_mem_level],
        ['cpu_level', settings.cpu_level],
        ['gpu_level', settings.gpu_level],
    ];
}

function resolutionToKey(resolution) {
    return `${resolution.width}x${resolution.height}`;
}

function keyToResolution(key) {
    const [width, height] = key.split('x');
    return { width: parseInt(width, 10), height: parseInt(height, 10) };
}

function getAspectRatioIndex(resolution) {
    const aspectRatio = resolution.width / resolution.height

    let closestAspectRatioDist = Infinity;
    let closestIndex = 0;
    for (let index = 0; index < aspectRatiosToIndex.length; index++) {
        const testAspectRatio = aspectRatiosToIndex[index];
        const dist = Math.abs( testAspectRatio - aspectRatio );
        if (dist < closestAspectRatioDist) {
            closestAspectRatioDist = dist;
            closestIndex = index;
        }
    }
    return closestIndex;
}

function selectAndFilterResolutionList(settings) {
    if (settings.displayMode === dispModeFullScreenWindowed) {
        // Always follows the desktop resolution
        return [settings.desktopResolution];
    }

    return filterResolutions(
        settings,
        getAspectRatioIndex(settings.resolution)
    );
}

/**
 * Filter the passed resolutions on aspect ratio and max desktop resolution.
 */
function filterResolutions(settings, aspectRatioIndex) {
    return getFullResolutionList(settings).filter((resolution) => 
        getAspectRatioIndex(resolution) === aspectRatioIndex
        && resolution.width <= settings.desktopResolution.width 
        && resolution.height <= settings.desktopResolution.height
    );
}

/**
 * Resolution list with all possible resolutions for the display mode.
 * Not filtered on aspect ratio or desktop resolution yet.
 */
function getFullResolutionList(settings) {
    return settings.displayMode === dispModeFullScreen ? 
    settings.fullScreenResolutions
    : settings.windowedResolutions;
}

/**
 * @param {{ width: number, height: number }[]} resolutions 
 * @param {{ width: number, height: number }} resolution 
 */
function findClosestResolution(resolutions, resolution) {
    let closestDelta = Infinity;
    let closestResolution = null;
    for (const testResolution of resolutions) {
        const delta = Math.abs(testResolution.width - resolution.width) + Math.abs(testResolution.height - resolution.height);
        if (delta < closestDelta) {
            closestDelta = delta;
            closestResolution = testResolution;
        }
    }
    return closestResolution;
}


