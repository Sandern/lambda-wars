
import React from 'react'
import { cleanup, screen, act, waitFor } from '@testing-library/react';
import { Hotkeys } from './hotkeys';
import { setupHotkeys, defaultHotkeysConfig } from '../../../testing/settings/hotkeys';
import { renderWithTranslations } from '../../../testing/render';
import { translate } from '../../../testing/translations';

afterEach(cleanup);

test('should render default hotkeys config', async () => {
    setupHotkeys(defaultHotkeysConfig);

    renderWithTranslations(<Hotkeys />);

    // Rendering is debounced because otherwise it might process too many updates at once
    // Caused by an event being dispatched for each changed binding, for example when resetting to default
    expect(() => screen.getByText('Bindings Title 1')).toThrowError(/Unable to find an element with the text/);

    await waitFor(() => expect(screen.getByText('Bindings Title 1')).toBeDefined());

    expect(screen.getByText('Hotkey 1')).toBeDefined();
    expect(screen.getByText('Hotkey 2')).toBeDefined();

    expect(screen.getByText('Hotkey 3')).toBeDefined();
    expect(screen.getByText('Hotkey 4')).toBeDefined();
});

test('should reset bindings on pressing ok', async () => {
    setupHotkeys(defaultHotkeysConfig);

    const resetToDefaultSpy = jest.spyOn(window.hotkeys, 'resetToDefault');

    renderWithTranslations(<Hotkeys />);

    await waitFor(() => expect(screen.getByText('Bindings Title 1')).toBeDefined());

    const resetButton = screen.getByText(translate('GameUI_UseDefaults'));
    act(() => resetButton.click());

    // Dialog should open
    expect(screen.getByText(translate('GameUI_KeyboardSettings'))).toBeInTheDocument();

    expect(resetToDefaultSpy).not.toHaveBeenCalled();

    const okButton = screen.getByText(translate('GameUI_OK'));
    act(() => okButton.click());

    expect(screen.queryByText(translate('GameUI_KeyboardSettings'))).not.toBeInTheDocument();
    await waitFor(() => expect(screen.getByText('Bindings Title 1')).toBeDefined());

    expect(resetToDefaultSpy).toHaveBeenCalledTimes(1);
});

test('should not reset bindings on pressing cancel', async () => {
    setupHotkeys(defaultHotkeysConfig);

    const resetToDefaultSpy = jest.spyOn(window.hotkeys, 'resetToDefault');

    renderWithTranslations(<Hotkeys />);

    await waitFor(() => expect(screen.getByText('Bindings Title 1')).toBeDefined());

    const resetButton = screen.getByText(translate('GameUI_UseDefaults'));
    act(() => resetButton.click());

    // Dialog should open
    expect(screen.getByText(translate('GameUI_KeyboardSettings'))).toBeInTheDocument();

    expect(resetToDefaultSpy).not.toHaveBeenCalled();

    const cancelButton = screen.getByText(translate('GameUI_Cancel'));
    act(() => cancelButton.click());

    expect(screen.queryByText(translate('GameUI_KeyboardSettings'))).not.toBeInTheDocument();
    await waitFor(() => expect(screen.getByText('Bindings Title 1')).toBeDefined());

    expect(resetToDefaultSpy).not.toHaveBeenCalled();
});