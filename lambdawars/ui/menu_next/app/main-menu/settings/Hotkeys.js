import React, { useEffect, useState } from 'react';
import styled from 'styled-components';
import { Button } from '../common/Button';
import { useObservable } from '../../observable-hook';
import { getStoreObservableAt, SETTINGS_HOTKEYS } from '../../gameui-store';
import { HotkeyBinding } from './HotkeyBinding';
import { FormattedMessage } from 'react-intl';
import { debounceTime } from 'rxjs/operators';
import { ConfirmModal } from '../common/modals/ConfirmModal';

const HotkeysContainer = styled.div`
    font-size; 0.8em;
`;
const HotkeysCard = styled.div`
    padding: 0.5em;
`;

/**
 * Bindings and hotkeys.
 */
export function Hotkeys() {
    const [modalOpen, setModalOpen] = useState(false);
    const hotkeys = useObservable(getStoreObservableAt(SETTINGS_HOTKEYS).pipe(debounceTime(1)), []);

    useEffect(() => {
        window.hotkeys.refreshHotkeys();
    }, []);

    return (
        <HotkeysContainer>
            {transformHotKeyItems(hotkeys).map((card) => renderGroup(card))}
            <Button onClick={() => setModalOpen(true)}>
                <FormattedMessage id="GameUI_UseDefaults"></FormattedMessage>
            </Button>
            <ConfirmModal
                isOpen={modalOpen}
                title={<FormattedMessage id="GameUI_KeyboardSettings"></FormattedMessage>}
                description={<FormattedMessage id="GameUI_KeyboardSettingsText"></FormattedMessage>}
                onConfirm={() => { window.hotkeys.resetToDefault(); setModalOpen(false); }}
                onCancel={() => setModalOpen(false)}
            ></ConfirmModal>
        </HotkeysContainer>
    );
}

/**
 * Transform flat list to lists per section.
 */
function transformHotKeyItems(hotkeyItems) {
    return hotkeyItems.reduce((cards, item) => {
        if (item.type === 'sectionTitle') {
            cards.push({
                displayName: item.displayName,
                items: [],
            });
            
        } else {
            if(cards.length === 0) {
                cards.push({
                    displayName: item.displayName,
                    items: [],
                });
            }
            cards[cards.length - 1].items.push(item)
        }
        return cards;
    }, []);
}

function renderGroup(group) {
    return <HotkeysCard key={group.displayName}>
        <h2 key="_title">{group.displayName}</h2>
        <hr />
        {group.items.map(renderItem)}
    </HotkeysCard>;
}

function renderItem(hotkey) {
    return <HotkeyBinding key={hotkey.binding} hotkey={hotkey}></HotkeyBinding>
}