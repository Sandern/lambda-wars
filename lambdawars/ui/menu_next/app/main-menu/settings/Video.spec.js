
import React from 'react'
import { cleanup, screen } from '@testing-library/react';
import { Video } from './video';
import { setupVideoSettings, defaultVideoSettings } from '../../../testing/settings/video';
import { renderWithTranslations } from '../../../testing/render';
import { translate } from '../../../testing/translations';

afterEach(cleanup);

function getSelectElement(label) {
    return screen.getByText(label);
}

test('should render video config', async () => {
    setupVideoSettings(defaultVideoSettings);

    renderWithTranslations(<Video />);

    const applyButton = screen.getByText(translate('settings_apply_video'));
    expect(applyButton).toBeDefined();

    // Check if select boxes match the config
    expect(getSelectElement(translate('SFUI_Settings_Widescreen_16_9'))).toBeDefined();

    expect(getSelectElement(`${defaultVideoSettings.resolution.width}x${defaultVideoSettings.resolution.height}`)).toBeDefined();

    expect(getSelectElement(translate('SFUI_Settings_Fullscreen'))).toBeDefined();
});
