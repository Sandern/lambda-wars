import React from 'react';
import { useState } from 'react';

import { Hotkeys } from './Hotkeys';
import { Options } from './Options';
import { Video } from './Video';
import { Audio } from './Audio';
import { FormattedMessage } from 'react-intl';
import styled from 'styled-components';
import { Button } from '../common/Button';

const Header = styled.div`
    height: 4em;
`;

const Content = styled.div`
    height: calc(100% - 4em);
`;

const settingPanels = {
    hotkeys: {
        component: <Hotkeys />,
    },
    options: {
        component: <Options/>,
    },
    video: {
        component: <Video/>,
    },
    audio: {
        component: <Audio/>,
    },
};

/**
 * Creates a menu with the available settings panels.
 */
export function Settings() {
    const [ selected, setSelected ] = useState('hotkeys');

    return <main>
        <Header>
            <Button onClick={() => setSelected('hotkeys')}><FormattedMessage id="settings_keyboard_mouse"></FormattedMessage></Button>
            <Button onClick={() => setSelected('options')}><FormattedMessage id="settings_game"></FormattedMessage></Button>
            <Button onClick={() => setSelected('video')}><FormattedMessage id="settings_video"></FormattedMessage></Button>
            <Button onClick={() => setSelected('audio')}><FormattedMessage id="settings_audio"></FormattedMessage></Button>
        </Header>
        <Content>
            {settingPanels[selected].component}
        </Content>
    </main>;
}
