import React from 'react';
import { FormattedMessage } from "react-intl";
import { SubMenuList } from '../common/SubMenuList'; 

import { useObservable } from '../../observable-hook';
import { getStoreObservableAt, GENERAL_USER_INFO, GENERAL_USER_STATE, HOSTING_NUM_PLAYERS } from '../../gameui-store';

/**
 * Represents the In Game menu route.
 * 
 * Displays menu items available when In Game.
 */
export function InGameMenu() {
    const userInfo = useObservable(getStoreObservableAt(GENERAL_USER_INFO), {});
    const userState = useObservable(getStoreObservableAt(GENERAL_USER_STATE), {});
    const hostNumPlayers = useObservable(getStoreObservableAt(HOSTING_NUM_PLAYERS), 0);

    function returnToGame() {
        gameui.clientcommand('gameui_hide');
    }
    
    function forfeit() {
        gameui.servercommand('player_forfeit');
        gameui.clientcommand('gameui_hide'); // go back ingame
    }
    
    function disconnect() {
        if (userState.isHosting && !userState.isOffline && hostNumPlayers > 1) {
            // confirm leaving
            /*ngDialog.open({ 
                template: 'confirmDisconnect', 
                className: 'ngdialog-theme-wars',
                showClose: true 
            }).closePromise.then(function (data) {
                if (data.value) {
                    gameui.clientcommand('disconnect');
                } else {
                    gameui.clientcommand('gameui_hide'); // go back ingame
                }
            });*/
        } else {
            gameui.servercommand('player_forfeit');
            gameui.clientcommand('disconnect');
        }
    }

    return (<section>
        <main>
            <h1><FormattedMessage id='IG_HeadLine'></FormattedMessage></h1>
            <SubMenuList>
                <li>
                    <a href onClick={returnToGame}>
                        <FormattedMessage id='IG_ReturnToGame'></FormattedMessage>
                        <p><FormattedMessage id='IG_ReturnToGame_Desc'></FormattedMessage></p>
                    </a>
                </li>
                { userState.isOffline && userInfo.betaBranch === 'dev' ? <li>
                    <a href="#!/LoadGame">
                        <FormattedMessage id='LG_LoadMenu'></FormattedMessage>
                        <p><FormattedMessage id='LG_LoadMenu_Desc'></FormattedMessage></p>
                    </a>
                </li> : undefined }
                { userState.isOffline && userInfo.betaBranch === 'dev' ? <li>
                    <a href="#!/LoadGame?mode=save">
                        <FormattedMessage id='LG_SaveMenu'></FormattedMessage>
                        <p><FormattedMessage id='LG_SaveMenu_Desc'></FormattedMessage></p>
                    </a>
                </li> : undefined }
                <li>
                    <a href onClick={forfeit}>
                        <FormattedMessage id='IG_Forfeit'></FormattedMessage>
                        <p><FormattedMessage id='IG_Forfeit_Desc'></FormattedMessage></p>
                    </a>
                </li>
                <li>
                    <a href onClick={disconnect}>
                        <FormattedMessage id='IG_Disconnect'></FormattedMessage>
                        <p><FormattedMessage id={getDisconnectDescriptionId(userState)}></FormattedMessage></p>
                    </a>
                </li>
            </SubMenuList>
        </main>
    </section>);
}

function getDisconnectDescriptionId(userState) {
    if (!userState.isHosting) {
        return 'IG_Disconnect_Desc';
    }
    if (userState.isHosting && !userState.isOffline) {
        return 'IG_DisconnectHost_Desc';
    }
    return 'IG_DisconnectOffline_Desc';
}
