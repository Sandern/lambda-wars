import React, { useState } from 'react';
import { FormattedMessage } from 'react-intl';
import styled from 'styled-components';
import { useObservable } from '../../observable-hook';
import { getStoreObservableAt, GENERAL_USER_INFO } from '../../gameui-store';

const DevWarning = styled.div`
	color: white;
	background-color: red;
	margin: 0;
	padding: 5px;
`;

export function OnlineGamesFinder() {
    const userInfo = useObservable(getStoreObservableAt(GENERAL_USER_INFO), {});
    const [lobbies, setLobbies] = useState([]);

    setTimeout(() => {
        setLobbies([]);
    }, 1000);

    return <main>
        {renderHeader(lobbies)}
        {userInfo.devBranch === 'dev' ? <DevWarning><b>You are currently running the developers build and may not find games here. Opt-out of the beta to switch to the public build.</b></DevWarning> : undefined }
        <ul>{ lobbies.map(renderLobbyRow) }</ul>
    </main>;
}

function renderHeader(lobbies) {
    return <h1>{lobbies.length > 0 ? `${lobbies.length} ` : ''}<FormattedMessage id={getHeaderText(lobbies)}></FormattedMessage></h1>;
}

function getHeaderText(lobbies) {
    const lobbiesCount = lobbies.length;

    switch (lobbiesCount) {
        case 0:
            return 'OG_NoLobbies';
        case 1:
            return 'OG_OneLobbyFound';
        default:
            return 'OG_LobbiesFound';
    }
}

function renderLobbyRow(lobby) {
    return (<li>
        <img className="map-preview" src={ `${lobby.overviewsrc}.vtf` } />
        <div className="lobby-name">{ lobby.name }</div>
        <div className="lobby-map">{ lobby.map }</div>
        <div className="lobby-mode">{ lobby.mode }</div>
        <div className="lobby-players">{ lobby.numtakenslots } / { lobby.numslots }</div>
        <button className="lobby-join-btn btn btn-highlight" onClick={() => joinLobby(lobby.steamid)}></button>
    </li>);
}

function joinLobby(steamid) {
    gamelobby.joinlobby(steamid)
}
