
import { useStoreObservableAt } from '../../gameui-store';

/**
 * String defining the state of the lobby.
 * 
 * @returns {'none'|'creating'|'lobbying'}
 */
export const useGamelobbyStateObservable = function() { return useStoreObservableAt('gamelobby:state', ...arguments); }

/**
 * Information about the current lobby owner.
 * 
 * @returns {{
   *     steamid: string,
   *     islobbyowner: string,
   *     ready: boolean
   * }}
   */
  export const useGamelobbyOwnerObservable = function() { return useStoreObservableAt('gamelobby:lobby-owner', ...arguments); }

/**
 * Settings for current selected mode and map.
 * 
 * @returns {{
 *     steamid: string,
 *     name: string,
 *     num_members: number,
 *     lobbytype: string,
 *     mode: string,
 *     map: string,
 *     teamsetup: string,
 *     islobbyowner: boolean,
 *     spectators: string,
 *     numslots: number,
 *     numtakenslots: number,
 *     localslotid: string,
 *     match_uuid: string,
 *     availablemodes: string[],
 *     availablemaps: string[],
 *     customfields: object[],
 * }}
 */
export const useGamelobbySettingsObservable = function() { return useStoreObservableAt('gamelobby:settings', ...arguments); }

/**
 * Player colors.
 * 
 * @returns {{
 *     name: string,
 *     color: string,
 *     available: boolean,
 * }[]}
 */
export const useGamelobbyPlayerColorsObservable = function() { return useStoreObservableAt('gamelobby:player-colors', ...arguments); }

/**
 * CPU difficulties
 * 
 * @returns {{
 *     id: string,
 *     displayname: string, // Localization key
 * }[]}
 */
export const useGamelobbyCpuDifficultiesObservable = function() { return useStoreObservableAt('gamelobby:cpu-difficulties', ...arguments); }


/**
 * Player factions
 * 
 * @returns {{
 *     name: string,
 *     displayname: string, // Localization key
 * }[]}
 */
export const useGamelobbyPlayerFactionsObservable = function() { return useStoreObservableAt('gamelobby:player-factions', ...arguments); }

/**
 * Available slots for the current selected mode and map.
 * 
 * @returns {{
 *     slotid: number,
 *     type: string,
 *     team: number,
 *     availablepositions: string[],
 *     iscpu: boolean,
 *     cputype: string,
 *     difficulty: string,
 *     player: { ready: boolean },
 *     editable: boolean, // set when user is local, or when lobby is owner and slot is taken by a cpu
 * }[]} -
 */
export const useGamelobbySlotsObservable = function() { return useStoreObservableAt('gamelobby:slots', ...arguments); }

/**
 * Members in lobby. This includes members without slot, such as spectators.
 * 
 * @returns {{
    *     [steamid: string]: {
    *         steamid: string,
    *         username: string,
    *         faction: string,
    *         color: string,
    *         ready: boolean,
    *         islocaluser: boolean,
    *         localslotid: number,
    *     }
    * }[]} -
    */
   export const useGamelobbyMembersObservable = function() { return useStoreObservableAt('gamelobby:lobby-members', ...arguments); }
   