import React from 'react';
import { useGamelobbySettingsObservable } from './stores';
import styled from 'styled-components';
import { FormattedMessage } from 'react-intl';
import { Select } from '../common/Select';

const MinimapImg = styled.img`
    width: 8em;
`;

/**
 * Renders settings like the selected map, mode, etc
 */
export function Settings() {
    const settings = useGamelobbySettingsObservable();
    console.log('settings: ', settings);

    if (!settings) {
        return <div>No settings yet</div>;
    }

    const minimapsrc = settings.availablemaps[settings.map]?.overviewsrc;

    return <div>
        <MinimapImg src={ `${minimapsrc}.vtf` } />
        {renderModes(settings)}
        {renderMaps(settings)}
        {renderTeamSetups(settings)}
    </div>
}

function renderModes(settings) {
    const options = Object.values(settings.availablemodes).map((mode) => ({
        value: mode.id, 
        label: mode.name
    }));
    return <div>
        <label><FormattedMessage id="GL_GameMode"></FormattedMessage></label>
        <Select 
            isSearchable={false}
            value={options.find((mode) => mode.value === settings.mode)} 
            onChange={(mode) => window.gamelobby.setSetting('mode', mode.value)}
            options={options}
        ></Select>
    </div>;
}

function renderMaps(settings) {
    const options = Object.values(settings.availablemaps).map((map) => ({
        value: map.id, 
        label: map.displayname
    }));
    return <div>
        <label><FormattedMessage id="GL_Map"></FormattedMessage></label>
        <Select 
            value={options.find((map) => map.value === settings.map)} 
            onChange={(map) => window.gamelobby.setSetting('map', map.value)}
            options={options}
        ></Select>
    </div>;
}

function renderTeamSetups(settings) {
    const options = Object.values(teamSetupsForMap(settings.availablemaps[settings.map])).map((teamsetup) => ({
        value: teamsetup.id, 
        label: teamsetup.name
    }))
    return <div>
        <label><FormattedMessage id="GL_Teams"></FormattedMessage></label>
        <Select 
            isSearchable={false}
            value={options.find((teamsetup) => teamsetup.value === settings.teamsetup)} 
            onChange={(teamsetup) => window.gamelobby.setSetting('teamsetup', teamsetup.value)}
            options={options}
        ></Select>
    </div>;
}

function teamSetupsForMap(mapEntry) {
    const supportedmodes = [];
    if (mapEntry) {
        for (const mode of mapEntry.supportedmodes) {
            supportedmodes[mode] = {
                id: mode,
                name: mode,
            };
        }
    }
    return supportedmodes;
}
