import React from 'react';
import { useGamelobbySlotsObservable } from './stores';
import { FormattedMessage } from 'react-intl';
import { Slot } from './slots/Slot';
import styled from 'styled-components';

const SlotsList = styled.ul`
    list-style-type: none;
`;

/**
 * Renders the available slots for the current selected map and mode.
 * Players can take slots and change their settings.
 */
export function Slots() {
    const slots = useGamelobbySlotsObservable([], []);
    if (!slots) {
        return <div></div>;
    }
    return buildSlotsPerTeam(slots).map((slotsGroup) => renderSlotsGroup(slotsGroup));
}

/**
 * Transform slots in groups per team.
 * Each group will get a header with the team.
 * If there are no teams for the map or mode, then there is only one group with "free for all" header.
 * 
 * @returns {{
 *     headerMsgId: string,
 *     team: number|undefined,
 *     slots: ReturnType<useGamelobbySlotsObservable>,
 * }}
 */
function buildSlotsPerTeam(slots) {
    const slotsPerTeam = [];
    let curSlots = [];

    for( let i = 0; i < slots.length; i++ ) {
        const slot = slots[i];

        curSlots.push(slot);

        if( i === slots.length -1 || (i < slots.length -1 && slot.team !== slots[i+1].team) ) {
            slotsPerTeam.push({
                headerMsgId: slot.team === 0 ? 'GL_FreeForAll' : 'GL_Team',
                team: slot.team === 0 ? undefined : slot.team - 1,
                slots: curSlots,
            });
            curSlots = [];
        }
    }
    
    return slotsPerTeam;
}

/**
 * Renders a group of slots with a header.
 * 
 * @param {{
 *     headerMsgId: string,
 *     team: number|undefined,
 *     slots: ReturnType<useGamelobbySlotsObservable>,
 * }} slotsGroup 
 */
function renderSlotsGroup(slotsGroup) {
    return <div key={slotsGroup.team || 'free-for-all'}>
        <h2><FormattedMessage id={slotsGroup.headerMsgId}></FormattedMessage>{slotsGroup.team ? <span>{` ${slotsGroup.team}`}</span> : undefined}</h2>
        {renderSlots(slotsGroup.slots)}
    </div>;
}

/**
 * Renders a list of slots.
 * 
 * @param {ReturnType<useGamelobbySlotsObservable>} slots 
 */
function renderSlots(slots) {
    return <SlotsList>
        {slots.map((slot) => <Slot key={slot.slotid} slot={slot}></Slot>)}
    </SlotsList>;
}