export function updatePlayerData(setting, value, slot) {
    // CPU is aways ready
    if( setting === 'ready' && slot.iscpu ) {
        value = '1';
    }
    
    // Slot id is not set for real players. This will make the data be stored on the player, and 
    // not on the slot. This is used for changing slots.
    window.gamelobby.setPlayerData(setting, value, slot.iscpu ? slot.slotid : undefined);
}