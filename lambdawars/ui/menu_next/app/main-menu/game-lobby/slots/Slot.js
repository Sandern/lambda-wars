import React from 'react';

import unknownPlayerAvatar from '../../../images/steam-avatar-unknown.jpg';
import styled from 'styled-components';

import { OpenSlot } from './OpenSlot';
import { PlayerSlot } from './PlayerSlot';

const SlotListItem = styled.li`
    display: flex;
    align-items: center;
`;

const AvatarImg = styled.img`
    width: 2em;
`;

/**
 * Renders one slot.
 * 
 * @param {{ 
 *     slot: ReturnType<import('../stores.js').useGamelobbySlotsObservable>[0],  
 * }} props
 */
export function Slot(props) {
    const { slot } = props;

    return <SlotListItem>
        <AvatarImg src={slot.player?.steamid && `avatar://medium/${slot.player.steamid}` || unknownPlayerAvatar } />

        {slotRenderMap[slot.type] ? slotRenderMap[slot.type](slot) : undefined}
    </SlotListItem>;
}

const slotRenderMap = {
    player: renderPlayerSlot,
    open: renderOpenSlot,
}

function renderPlayerSlot(slot) {
    return <PlayerSlot slot={slot}></PlayerSlot>
}
function renderOpenSlot(slot) {
    return <OpenSlot slot={slot}></OpenSlot>
}
