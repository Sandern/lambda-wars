import React from 'react';
import { useGamelobbyPlayerColorsObservable } from '../stores';
import styled from 'styled-components';
import { updatePlayerData } from './data-setter-helpers';

const ColorDiv = styled.div`
    display: inline-block;
    width: 3.2em;
    height: 3.2em;
    margin-right: 3em;
`;

const ColorSelect = styled.select`
    width: 3.2em;
    height: 3.2em;
    overflow: hidden;
    background: rgba(255, 255, 255, 0.1);
    outline: none;
    color: white;
    margin-right: 3em;
`;

export function ColorSelector(props) {
    const { slot } = props;

    const playerColors = useGamelobbyPlayerColorsObservable();
    if (!playerColors) {
        return <span></span>;
    }

    const slotColor = playerColors[slot.player.color];
    if (!slotColor) {
        return <span></span>;
    }

    const handleChange = (event) => {
        updatePlayerData('color', playerColors[event.target.value].name, slot); 
    }

    return <span>
        { !slot.editable ? <ColorDiv style={{ background: slotColor.color }}></ColorDiv> : undefined }
        { slot.editable ? <ColorSelect disabled={slot.ready} value={slotColor.color} onChange={handleChange} style={{ background: slotColor.color }}>
            {Object.values(playerColors).map((colorOption) => <option key={colorOption.name} disabled={!colorOption.available} value={colorOption.name} style={{ background: colorOption.color }}></option>)}
        </ColorSelect> : undefined }
    </span>;
}