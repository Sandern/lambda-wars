import React from 'react';
import { FormattedMessage } from 'react-intl';
import { Button } from '../../common/Button';
import { useGamelobbyOwnerObservable } from '../stores';

/**
 * Renders an open slot.
 */
export function OpenSlot(props) {
    const { slot } = props;

    const lobbyOwnerInfo = useGamelobbyOwnerObservable();

    return <span>
        <Button onClick={() => gamelobby.requestSlot(slot.slotid)}><FormattedMessage id="GL_TakeSlot"></FormattedMessage></Button>
        {lobbyOwnerInfo?.islobbyowner ? <Button disabled={lobbyOwnerInfo.ready} onClick={() => gamelobby.addCPU(slot.slotid)}><FormattedMessage id="GL_AddCPU"></FormattedMessage></Button> : undefined }
    </span>;
}

