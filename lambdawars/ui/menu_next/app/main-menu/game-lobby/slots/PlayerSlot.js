import React from 'react';
import { FormattedMessage } from 'react-intl';
import { Button } from '../../common/Button';
import { useGamelobbyOwnerObservable, useGamelobbyPlayerFactionsObservable } from '../stores';
import { ColorSelector } from './ColorSelector';
import { updatePlayerData } from './data-setter-helpers';
import { Select } from '../../common/Select';

import crownImg from '../../../images/crown_orange.png';
import styled from 'styled-components';

const CrownImg = styled.img`
    width: 1em;
`;

const FactionSelect = styled(Select)`
    display: inline-block;
    width: 10em;
`;

const PlayerReadyButton = styled.a`
    font-size: 2.4rem;
    width: 3.2rem;
    padding: 0;
    border: 1px solid var(--button-text-color-op50);
    color: rgba(255, 255, 255, 0.1);
    background: var(--secondary-theme-color-op20);
    text-align: center;
    float: right;
    transition: all .2s;
`;

const PlayerReadyCheckedButton = styled(PlayerReadyButton)`
    color: var(--primary-theme-color);
    border-color: var(--button-text-color);
    background: var(--secondary-theme-color-op90);
`;

/**
 * Render a slot taken by a player.
 */
export function PlayerSlot(props) {
    const { slot } = props;

    return <span style={{ width: '100%' }}>
        { slot.player.isLobbyLeader ? <CrownImg src={crownImg}></CrownImg> : undefined }
        { slot.player.name + (slot.iscpu ? ' - ' + slot.difficulty : '') }
        <ColorSelector slot={slot}></ColorSelector>
        { renderFactionSelector(slot) }
        { renderRemoveCPUButton(slot) }
        { renderKickButton(slot) }
        { renderReadyButton(slot) }
    </span>;
}

function renderFactionSelector(slot) {
    const factions = useGamelobbyPlayerFactionsObservable();
    if (!factions) {
        return undefined;
    }

    const faction = factions[slot.player.faction];
    if (!faction) {
        return undefined;
    }

    if (!slot.editable) {
        return <span>{ faction.displayname }</span>;
    }

    const options = Object.values(factions).map((factionOption) => ({
        value: factionOption.name, 
        label: factionOption.displayname
    }));
    return <FactionSelect 
        isSearchable={false}
        value={options.find((factionOption) => factionOption.value === faction.name)} 
        onChange={(factionOption) => updatePlayerData('faction', factionOption.value, slot)}
        options={options}
    ></FactionSelect>;
}

function renderRemoveCPUButton(slot) {
    const lobbyOwnerInfo = useGamelobbyOwnerObservable();

    if (!slot.editable || !slot.iscpu) {
        return undefined;
    }

    return <Button disabled={lobbyOwnerInfo?.ready} onClick={() => gamelobby.removeCPU(slot.slotid)}><FormattedMessage id="GL_RemoveCPU"></FormattedMessage></Button>;
}

function renderKickButton(slot) {
    const lobbyOwnerInfo = useGamelobbyOwnerObservable();

    if (!slot.editable || slot.iscpu || slot.player.isLobbyLeader) {
        return undefined;
    }

    return <Button disabled={lobbyOwnerInfo?.ready} onClick={() => gamelobby.kickPlayer(slot.slotid)}><FormattedMessage id="GL_Kick"></FormattedMessage></Button>;
}

function renderReadyButton(slot) {
    const onClick = slot.editable && !slot.iscpu ? () => {
        updatePlayerData('ready', slot.player.ready ? '0' : '1', slot.slotid);
    } : undefined;
    const extraStyles = { cursor: (onClick ? 'pointer' : 'none') };

    return slot.player.ready ? <PlayerReadyCheckedButton style={extraStyles} onClick={onClick}>&#x2713;</PlayerReadyCheckedButton> : <PlayerReadyButton style={extraStyles} onClick={onClick}>&#x2713;</PlayerReadyButton>;
}