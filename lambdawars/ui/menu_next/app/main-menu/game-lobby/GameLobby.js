import React, { useEffect, useState } from 'react';
import { useGamelobbyStateObservable, useGamelobbyOwnerObservable } from './stores';
import { useLocation } from 'react-router-dom';
import { Slots } from './Slots';
import { Button } from '../common/Button';
import { FormattedMessage } from 'react-intl';
import styled from 'styled-components';
import { Settings } from './Settings';
import { LobbyChat } from './LobbyChat';

const GamelobbyGrid = styled.div`
    display: grid;
    height: 100%;
    grid-template-columns: 80% 20%;
    grid-template-rows: 5% 35% 55% 5% ;
    grid-template-areas: 
      "header header"
      "slots settings"
      "slots chat"
      "footer footer";
`;

export function GameLobby() {
    const location = useLocation();
    const lobbyState = useGamelobbyStateObservable(undefined, []);
    const lobbyOwnerInfo = useGamelobbyOwnerObservable();

    const isLobbyOwner = lobbyOwnerInfo?.islobbyowner;

    initLogic(location, lobbyState);

    const statusRenderer = renderStatus[lobbyState] || (() => undefined);

    return <main style={{ height: '100%' }}>
        <GamelobbyGrid>
            <div style={{ gridArea: 'header' }}><FormattedMessage id="GL_Status"></FormattedMessage>: {statusRenderer()}</div>
            <div style={{ gridArea: 'slots' }}><Slots></Slots></div>
            <div style={{ gridArea: 'settings' }}><Settings></Settings></div>
            <div style={{ gridArea: 'chat' }}><LobbyChat></LobbyChat></div>
            <div style={{ gridArea: 'footer' }}>
                <Button onClick={() => leaveLobby()}><FormattedMessage id="GL_LeaveLobby"></FormattedMessage></Button>
                <Button onClick={() => spectate()}><FormattedMessage id="GL_Spectate"></FormattedMessage></Button>
                { isLobbyOwner ? <Button onClick={() => invite()}><FormattedMessage id="GL_Invite"></FormattedMessage></Button> : undefined }

                { isLobbyOwner ? <span style={{ float: "right" }}>
                    { notStartedOrEnded(lobbyState) ? <Button onClick={() => launch()}><FormattedMessage id="GL_Launch"></FormattedMessage></Button> : undefined }
                </span> : undefined }
            </div>
        </GamelobbyGrid>
    </main>;
}

/**
 * Setup logic for creating or joining a lobby.
 * - When the player enters the gamelobby (causing this component to be created), check if lobby state turns to none.
 *   When this is the case automatically create the lobby if the route origin was a "create".
 */
function initLogic(location, lobbyState) {
    if (!location.state || !location.state.create) {
        return;
    }
    const [lobbyCreated, setLobbyCreated] = useState(false);
    useEffect(() => {
        if (!lobbyCreated && lobbyState === 'none') {
            console.log('create lobby!');
            createLobby();
            setLobbyCreated(true);
        }
    }, [lobbyState]);
}

const renderStatus = {
    'creating': () => renderNormalStatus('GL_StatusCreating'),
    'joining': () => renderNormalStatus('GL_StatusJoining'),
    'searchingserver': () => renderNormalStatus('GL_StatusFindingServer'),
    'startinglocalserver': () => renderNormalStatus('GL_StatusHostStartingLocal'),
    'gamestarted': () => renderNormalStatus('GL_StatusInProgress'),
    'gameended': () => renderNormalStatus('GL_StatusGameEnded'),
};

function renderNormalStatus(localizationKey) {
    return <FormattedMessage id={localizationKey}></FormattedMessage>;
}

function createLobby() {
    window.gamelobby.createlobby('New lobby');
}

function leaveLobby() {
    window.gamelobby.leavelobby();
}

function spectate() {
    window.gamelobby.goSpectate();
}

function invite() {
    window.gamelobby.invite();
}

function launch() {
    window.gamelobby.launch();
}

function notStartedOrEnded(state) {
    return !['gamestarted', 'gameended'].includes(state);
}