import React from 'react';
import { ChatLines } from '../common/chat/ChatLines';
import { ChatInput } from '../common/chat/ChatInput';

/**
 * Chat section of lobby
 */
export function LobbyChat() {
    return <div style={{ height: '100%' }}>
        <ChatLines source="gamelobby"></ChatLines>
        <ChatInput chatApi={gamelobby}></ChatInput>
    </div>
}
