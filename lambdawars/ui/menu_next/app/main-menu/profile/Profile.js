import React from 'react';
import { MatchHistory } from './MatchHistory';
import { getStoreObservableAt, GENERAL_USER_INFO } from '../../gameui-store';
import { useObservable } from '../../observable-hook';

export function Profile() {
    const userInfo = useObservable(getStoreObservableAt(GENERAL_USER_INFO), {});

    return (<section>
        <main>
            <MatchHistory steamid={userInfo.steamid}></MatchHistory>
        </main>
    </section>);
}