import React, { useState, useEffect } from 'react';
import { FormattedMessage } from "react-intl";
import { MatchResult } from './MatchResult';
import styled from 'styled-components';
import { getMatchesList } from './matches-service';

const MatchesContent = styled.div`
    padding-bottom: 2rem;
`;

const MatchesList = styled.ul`
    list-style-type: none;
`;

export function MatchHistory(props) {
    const { steamid } = props;
    const [matchUuid, setMatchUuid] = useState(null);
    const [page, setPage] = useState(1);
    const [busy, setBusy] = useState(false);
    const [matchHistory, setMatchHistory] = useState(null);

    function backToMatchHistory() {
        setMatchUuid(null);
    }

    useEffect(() => {
        if (!steamid) {
            return;
        }

        console.log('Requesting history for: ', steamid);
        setBusy(true);

        getMatchesList(steamid, page)
        .then((response) => {
            console.log('matches response: ', response);
            setBusy(false);
            setMatchHistory(response);
        })
        .catch((e) => {
            console.error('Failed to get match history: ', e);
            setBusy(false);
        });
    }, [steamid, page]);

    if (!matchHistory) {
        return <MatchesContent><h1><FormattedMessage id='MA_Fetching'></FormattedMessage></h1></MatchesContent>;
    }

    if (!matchUuid) {
        return (<MatchesContent>
            <h1><FormattedMessage id="MA_MatchHistory"></FormattedMessage>:</h1>
            <MatchesList>
                {matchHistory.matches.map((match) => (
                    <li key={match.match_uuid}>
                        {match.start_date} - {match.mode} - {match.map} - {match.type} - {match.end_state} <a className="btn" href onClick={() => setMatchUuid(match.match_uuid)}>Show</a>
                    </li>
                ))}

            </MatchesList>
            {!busy && page > 1 ? <a href onClick={() => setPage(Math.max(1, page - 1))} className="btn"><FormattedMessage id="Button_Previous"></FormattedMessage></a> : undefined }
            {!busy && page < numPages(matchHistory) ? <a href onClick={() => setPage(Math.min(matchHistory.matches.length - 1, page + 1))} className="btn"><FormattedMessage id="Button_Next"></FormattedMessage></a> : undefined }
        </MatchesContent>);
    }

    return (<MatchesContent>
        <MatchResult matchUuid={matchUuid}></MatchResult>
        <br />
        <a className="btn" href onClick={backToMatchHistory}><FormattedMessage id="Button_Back"></FormattedMessage></a>
    </MatchesContent>);
}

function numPages(matchHistory) {
    return matchHistory.total / matchHistory.per_page;
}