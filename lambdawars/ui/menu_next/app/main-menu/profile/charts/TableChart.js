import styled from "styled-components";

export const TableChart = styled.table`
    width: 100%;
    padding: 2em;
    background-color: var(--secondary-theme-color-op60);

    th, td {
        line-height: 1.5;
        text-align: center;
    }
`;