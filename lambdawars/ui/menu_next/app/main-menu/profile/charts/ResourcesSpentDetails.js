import React from 'react';
import { Line } from 'react-chartjs-2';

export function ResourcesSpentDetails(props) {
    const { matchData, owner } = props;

    // Line graph resources over time
    const resource_labels = [];
    const datasets = [];

    // Fixed categories for now
    const categories = [
        {label: 'economy', color: '#00FF00'}, 
        {label: 'army', color: '#FF0000'}, 
        {label: 'defense', color: '#FFFF00'}, 
        {label: 'technology', color: '#0000FF'}, 
    ];

    categories.forEach((category) => {
        datasets.push({
            label: category.label,
            borderColor: category.color,
            lineTension: 0,
            data: [],
        });
    });

    // Calculate sample points
    const sampleStep = Math.round(Math.max(1, matchData.events.length / 25.0));

    function addSampleAt(idx) {
        const e = matchData.events[idx][owner];

        for (let catIdx = 0; catIdx < categories.length; catIdx++) {
            const category = categories[catIdx];
            let value = 0;
            for (let resource in e.spent_resources_per_category) {
                value += e.spent_resources_per_category[resource][category.label] || 0;
            }

            datasets[catIdx].data.push(value);
        }

        resource_labels.push(Math.round(parseFloat(matchData.events[idx].timestamp)));
    }

    for (let i = 0; i < matchData.events.length - 1; i = i + sampleStep) {
        addSampleAt(i);
    }
    addSampleAt(matchData.events.length-1);

    return (<Line data={{
        labels: resource_labels,
        datasets: datasets,
    }} />);
}