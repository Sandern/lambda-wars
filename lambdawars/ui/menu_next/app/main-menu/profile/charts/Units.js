import React from 'react';

import { TableChart } from './TableChart';

export function Units(props) {
    const { matchData } = props;

    return (<TableChart>
        <thead>
            <tr>
                <th>Team</th>
                <th>Player</th>
                <th>Units Trained</th>
                <th>Units killed</th>
                <th>Structures built</th>
                <th>Structures Razed</th>
            </tr>
        </thead>
        <tbody>
            {Object.entries(matchData.players).map((playerEntry) => renderUnitsRow(playerEntry, matchData))}
        </tbody>
    </TableChart>);
}

function renderUnitsRow(playerEntry, matchData) {
    const [key, player] = playerEntry;
    return <tr key={key}>
        <td>{player.team}</td>
        <td>{player.name}</td>
        <td>{matchData.units_produced[key] || 0}</td>
        <td>{matchData.units_killed[key] || 0}</td>
        <td>{matchData.buildings_constructed[key] || 0}</td>
        <td>{matchData.buildings_killed[key] || 0}</td>
    </tr>;
}