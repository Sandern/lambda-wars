import React from 'react';

import { TableChart } from './TableChart';

export function Scores(props) {
    const { matchData } = props;

    const scores = {};

    function computeResourcesScore(owner) {
        const collectedResources = matchData.collected_resources[owner];

        let score = 0;
        for (let resType in collectedResources) {
            score += collectedResources[resType];
        }
        return Math.round(score*100);
    }

    function computeArmyScore(owner) {
        const spentResourcesPerCategory = matchData.spent_resources_per_category[owner];

        let score = 0;
        for (let resType in spentResourcesPerCategory) {
            score += spentResourcesPerCategory[resType].army || 0;
        }

        return score * 100;
    }

    function computeTechScore(owner) {
        const spentResourcesPerCategory = matchData.spent_resources_per_category[owner];

        let score = 0;
        for (let resType in spentResourcesPerCategory) {
            score += spentResourcesPerCategory[resType].technology || 0;
        }

        return score * 100;
    }

    for (let owner in matchData.players) {
        const playerInfo = matchData.players[owner];

        scores[owner] = {
            team: playerInfo.team,
            name: playerInfo.name,

            // Scores
            resources: computeResourcesScore(owner),
            army: computeArmyScore(owner),
            technology: computeTechScore(owner)
        };

        scores[owner].overview = (scores[owner].resources +
            scores[owner].army + scores[owner].technology);
    }

    return (<TableChart>
        <thead>
            <tr>
                <th>Team</th>
                <th>Player</th>
                <th>Resources</th>
                <th>Army</th>
                <th>Technology</th>
                <th>Overview</th>
            </tr>
        </thead>
        <tbody>
            {Object.entries(scores).map(renderScoreRow)}
        </tbody>
    </TableChart>);
}

function renderScoreRow(entry) {
    const [key, scoreInfo] = entry;
    return <tr key={key}>
        <td>{scoreInfo.team}</td>
        <td>{scoreInfo.name}</td>
        <td>{scoreInfo.resources}</td>
        <td>{scoreInfo.army}</td>
        <td>{scoreInfo.technology}</td>
        <td>{scoreInfo.overview}</td>
    </tr>;
}