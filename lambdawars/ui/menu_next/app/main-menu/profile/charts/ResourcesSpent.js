import React from 'react';
import { Line } from 'react-chartjs-2';

export function ResourcesSpent(props) {
    const { matchData } = props;

    // Line graph resources over time
    const resource_labels = [];
    const datasets = [];

    // Fixed categories for now
    const owner_to_index = {};

    for (let owner in matchData.players) {
        owner_to_index[owner] = datasets.length;
        datasets.push({
            label: matchData.players[owner].name,
            borderColor: matchData.players[owner].color,
            lineTension: 0,
            data: [],
        });
    }

    // Calculate sample points
    const sampleStep = Math.round(Math.max(1, matchData.events.length / 25.0));

    function addSampleAt(idx) {
        const e = matchData.events[idx];

        for (let owner in e) {
            if (!owner_to_index[owner]) {
                continue;
            }

            const owner_idx = owner_to_index[owner];

            let value = 0;
            for (var resource in e[owner].spent_resources) {
                value += e[owner].spent_resources[resource] || 0;
            }

            datasets[owner_idx].data.push(value);
        }

        resource_labels.push(Math.round(parseFloat(e.timestamp)));
    }

    for (let i = 0; i < matchData.events.length - 1; i = i + sampleStep) {
        addSampleAt(i);
    }
    addSampleAt(matchData.events.length-1);

    return (<Line data={{
        labels: resource_labels,
        datasets: datasets,
    }} />);
}