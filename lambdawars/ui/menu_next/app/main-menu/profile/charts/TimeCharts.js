import React, { useState } from 'react';
import { ResourcesSpent } from './ResourcesSpent';
import { ResourcesSpentDetails } from './ResourcesSpentDetails';

export function TimeCharts(props) {
    const { matchData } = props;

    const [selectedChart, setSelectedChart] = useState('resources_spent');

    return (<div>
        <select name="charts" value={selectedChart} onChange={(e) => setSelectedChart(e.target.value)}>
            <option value="resources_spent">Spent Resources</option>
            {Object.keys(matchData.players).map((owner) => <option key={resourcesSpentId(owner)} value={resourcesSpentId(owner)}>Spent Resources Details - {matchData.players[owner].name}</option>)}
        </select>

        {renderChart(selectedChart, matchData)}
    </div>);
}

function resourcesSpentId(owner) {
    return `resource_spent_detail_${owner}`;
}

function resourcesSpentIdToOwner(id) {
    return parseInt(id.split('resource_spent_detail_')[1], 10);
}

function renderChart(selectedChart, matchData) {
    if (selectedChart === 'resources_spent') {
        return <ResourcesSpent matchData={matchData}></ResourcesSpent>
    }

    if (selectedChart.startsWith('resource_spent_detail_')) {
        return <ResourcesSpentDetails matchData={matchData} owner={resourcesSpentIdToOwner(selectedChart)}></ResourcesSpentDetails>
    }
}