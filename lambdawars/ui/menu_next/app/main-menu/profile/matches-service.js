const baseUrl = 'https://api.lambdawars.com/';

export async function getMatchesList(steamid, page) {
    return fetch(`${baseUrl}player/matches/list/${steamid}/${page}?per_page=${10}`, { 
        headers: {
            'Content-Type': 'application/json' 
        }
    })
    .then((response) => response.json());
}

export async function getMatchData(matchUuid) {
    return fetch(`${baseUrl}player/matches/get/${matchUuid}`, { 
        headers: {
            'Content-Type': 'application/json' 
        }
    })
    .then((response) => response.json());
}