import React, { useEffect, useState } from 'react';
import { getMatchData } from './matches-service';
import { FormattedMessage } from 'react-intl';

import { Scores } from './charts/Scores';
import { Units } from './charts/Units';
import { TimeCharts } from './charts/TimeCharts';

export function MatchResult(props) {
    const { matchUuid } = props;

    const [matchData, setMatchData] = useState(null);
    const [selectedChart, setSelectedChart] = useState('scores');

    useEffect(() => {
        getMatchData(matchUuid)
        .then((data) => {
            setMatchData(data);
        });
    }, [matchUuid]);

    if (!matchData) {
        return <p><FormattedMessage id="MA_Fetching"></FormattedMessage></p>;
    }
    
    const seconds = parseFloat(matchData.duration);
    let durationMinutes = Math.floor(seconds / 60.0);
    let durationSeconds = Math.round(seconds % 60);

    if (durationMinutes < 10) durationMinutes = '0' + durationMinutes;
    if (durationSeconds < 10) durationSeconds = '0' + durationSeconds;

    return (<section>
        <h1>{matchData.map} | {durationMinutes}:{durationSeconds}</h1>

        <a className="btn" href onClick={() => setSelectedChart('scores')}><FormattedMessage id="MA_Scores"></FormattedMessage></a>
        <a className="btn" href onClick={() => setSelectedChart('units')}><FormattedMessage id="MA_Units"></FormattedMessage></a>
        <a className="btn" href onClick={() => setSelectedChart('time_charts')}><FormattedMessage id="MA_Graphs"></FormattedMessage></a>

        {renderChart(selectedChart, matchData)}
    </section>);
}

function renderChart(selectedChart, matchData) {
    if (selectedChart === 'scores') {
        return <Scores matchData={matchData}></Scores>;
    }
    if (selectedChart === 'units') {
        return <Units matchData={matchData}></Units>;
    }
    if (selectedChart === 'time_charts') {
        return <TimeCharts matchData={matchData}></TimeCharts>
    }
}