import React, { useEffect, useState } from 'react';
import styled from 'styled-components';

const NewsSection = styled.section`
    min-height: 100%;

    main {
        min-height: 100%;
    }
    
    h1 {
        color: var(--primary-theme-color);
    }

    section.post {
        font-size: 1.4rem;
        line-height: 2rem;
        color: rgba(255, 255, 255, 0.8);
        margin-bottom: 4rem;

        @media (max-width: 1180px) {
            font-size: 1.0rem;
        }

        h1 {
            margin: 0;
            background-color: transparent;
        }

        a {
            color: var(--primary-theme-color);
            transition: all .2s;

            &:hover {
                color: white;
            }
        }

        img {
            max-width: 100%;
        }
    }

    ul {
        list-style-type: disc;
        margin-left: 2em;
    }
`;

const NewsItem = styled.div`
    &:hover {
        background-color: green;
    }
`;

/**
 * Displays news fetched from lambdawars.com.
 */
export function News() {
    const [ news, setNews ] = useState();

    useEffect(() => {
        fetchNews()
        .then(news => setNews(news))
        .catch((err) => {
            setNews(undefined);
            console.error('Failed to fetch news: ', err);
        });
    }, []);

    return (<NewsSection>
        <main>{ news ? news.appnews.newsitems.map(renderNewsItem) : undefined }</main>
    </NewsSection>);
}

// const newsUrl = 'https://api.steampowered.com/ISteamNews/GetNewsForApp/v0002/?appid=270370&maxlength=0&format=json';
const newsUrl = 'https://lambdawars.com/game/news.php'; // Proxy for api.steampowered to by pass CORS...

async function fetchNews() {
    return await(
        await fetch(newsUrl)
    ).json();
}

function renderNewsItem(newsItem) {
    const onClick = () => window.open(newsItem.url, '_blank');

    return (<NewsItem key={newsItem.gid} onClick={onClick}>
        <h1 style={{ pointerEvents: 'none'}}>{ newsItem.title }</h1>
        <div className="subtitle" style={{ pointerEvents: 'none'}}>{ `News Posted by ${newsItem.author} | on ${new Intl.DateTimeFormat("en-GB", {
          year: "numeric",
          month: "long",
          day: "2-digit"
        }).format(new Date(newsItem.date * 1000))}` }</div>
        <section className="post" style={{ pointerEvents: 'none'}}>{ newsItem.contents }</section>
    </NewsItem>);
}