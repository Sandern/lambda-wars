import React, { useState, useEffect } from 'react';

export function Missions() {
    const [missions, setMissions] = useState([]);

    useEffect(() => {
        window['gameui'].retrievemissions((receivedMissions) => {
            setMissions(receivedMissions);
        });
    }, []);

    return <main><ul>{missions.map(renderMission)}</ul></main>;
}

function renderMission(mission) {
    const launch = () => {
        gameui.launchmission(mission.mapname, 'medium')
    };
    return <li key={mission.mapname}><span>{ mission.mapname }: { mission.description }</span><button className="btn btn-highlight" onClick={launch}>Launch</button></li>;
}