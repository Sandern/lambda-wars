import React from 'react';
import { FormattedMessage } from "react-intl";
import { SubMenuList } from '../common/SubMenuList';
import { MenuLink } from '../common/MenuLink';

export function MultiPlayerMenu() {
    return (<section>
        <main>
            <h1><FormattedMessage id='MP_HeadLine'></FormattedMessage></h1>
            <SubMenuList>
                <li>
                    <MenuLink to="online-games-finder">
                        <FormattedMessage id='MP_FindOnlineGame'></FormattedMessage>
                        <p><FormattedMessage id='MP_FindOnlineGame_Desc'></FormattedMessage></p>
                    </MenuLink>
                </li>
                <li>
                    <MenuLink to={{ pathname: "game-lobby", state: { create: true  } }}>
                        <FormattedMessage id='MP_CreateGame'></FormattedMessage>
                        <p><FormattedMessage id='MP_CreateGame_Desc'></FormattedMessage></p>
                    </MenuLink>
                </li>
            </SubMenuList>
        </main>
    </section>);
}