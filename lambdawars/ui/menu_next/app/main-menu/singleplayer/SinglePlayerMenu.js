import React from 'react';
import { FormattedMessage } from "react-intl";
import { SubMenuList } from '../common/SubMenuList';
import { MenuLink } from '../common/MenuLink';

import { useObservable } from '../../observable-hook';
import { getStoreObservableAt, GENERAL_USER_INFO } from '../../gameui-store';

export function SinglePlayerMenu() {
    const userInfo = useObservable(getStoreObservableAt(GENERAL_USER_INFO), {});

    return (<section id="content-singleplayer">
        <main>
            <h1><FormattedMessage id='SP_HeadLine'></FormattedMessage></h1>
            <SubMenuList>
                <li>
                    <MenuLink to={`missions`}>
                        <FormattedMessage id='SP_Missions'></FormattedMessage>
                        <p><FormattedMessage id='SP_Missions_Desc'></FormattedMessage></p>
                    </MenuLink>
                </li>
                <li>
                    <MenuLink to="game-lobby">
                        <FormattedMessage id='SP_AISkirmish'></FormattedMessage>
                        <p><FormattedMessage id='SP_AISkirmish_Desc'></FormattedMessage></p>
                    </MenuLink>
                </li>
                <li>
                    <MenuLink to="/" onClick={playtutorial}>
                        <FormattedMessage id='SP_Tutorial'></FormattedMessage>
                        <p><FormattedMessage id='SP_Tutorial_Desc'></FormattedMessage></p>
                    </MenuLink>
                </li>
                { userInfo.betaBranch === 'dev' ? <li>
                    <MenuLink to="load-game">
                        <FormattedMessage id='LG_LoadMenu'></FormattedMessage>
                        <p><FormattedMessage id='LG_LoadMenu_Desc'></FormattedMessage></p>
                    </MenuLink>
                </li> : undefined }
            </SubMenuList>
        </main>
    </section>);
}

function playtutorial() {
    gameui.creategametutorial();
}
