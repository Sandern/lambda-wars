import styled from 'styled-components';

/**
 * Generic button for in main menu.
 * 
 * TODO: add different versions such as primary and secondary buttons.
 */
export const Button = styled.button`
    background: var(--button-background-color);
    color: black;
    display: inline-block;
    padding: 0 1rem;
    height: 3.4rem;
    margin-right: 1rem;
    font-family: 'Roboto Condensed';
    font-size: 2.2rem;
    font-weight: 400;
    letter-spacing: 0.08rem;
    line-height: 3.4rem;
    text-transform: uppercase;
    opacity: 0.8;
    transition: all .2s;

    &:last-of-type {
        margin-right: 0;
    }

    &:hover {
        opacity: 1;
        background: var(--button-background-color-hover);
        color: white;
    }
`;
