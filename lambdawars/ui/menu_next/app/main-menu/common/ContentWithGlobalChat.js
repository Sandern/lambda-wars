import React from 'react';
import styled from 'styled-components';
import { GlobalChat } from './chat/GlobalChat';

const Container = styled.div`
    display: flex;
    flex: 1 1 auto;
    justify-content: center;
    overflow-y: hidden;
`;

export const ContentItem = styled.div`
    width: calc(120rem - 48rem);
    margin-top: 2rem;
    height: calc(100% - 5rem); 
    overflow-y: auto;
    overflow-x: hidden;
`;

const GlobalChatContent = styled.div`
	width: 46rem;
    margin-top: 2rem;
    margin-left: 2rem;
    height: calc(100% - 5rem); 
`;

/**
 * Shows global chat on right side.
 */
export function ContentWithGlobalChat(props) {
    return (<Container>
        <ContentItem>{ props.children }</ContentItem>
        <GlobalChatContent><GlobalChat></GlobalChat></GlobalChatContent>
    </Container>)
}