import { MenuList } from './MenuList';
import styled from 'styled-components';

export const SubMenuList = styled(MenuList)`
    padding-bottom: 2rem; 

    li {
        margin-bottom: 1rem;

        &:last-of-type {
            margin-bottom: 0;
        }
    }

    a {
        display: block;
        background: var(--secondary-theme-color-op80);
        color: var(--primary-theme-color);
        border-left: 0.4rem solid var(--primary-theme-color);
        font-size: 2.8rem;
        font-family: 'Roboto Condensed';
        text-transform: uppercase;
        line-height: 1.2;
        padding: 1.6rem 2.4rem;
        opacity: 0.8;
        transition: all .2s;

        @media (max-width: 1180px) {
            font-size: 2.0rem;
        }
        @media (max-width: 1024px) {
            font-size: 1.2rem;
        }

        p {
            color: var(--accent-theme-color);
            font-size: 1.8rem;
            line-height: 1.5;
            margin: 0;
            font-family: 'Open Sans';
            text-transform: none;
            transition: all .3s;

            @media (max-width: 1180px) {
                font-size: 1.2rem;
            }

            @media (max-width: 1024px) {
                font-size: 1.0rem;
            }
        }

        &:hover {
            background: var(--secondary-theme-color);
            border-left: 1rem solid var(--primary-theme-color);
            opacity: 1;

            p {
                color: white;
            }
        }
    }
`;