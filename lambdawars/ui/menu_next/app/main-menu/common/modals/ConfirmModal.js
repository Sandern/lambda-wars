import React from 'react';
import PropTypes from 'prop-types';
import Modal from 'react-modal';
import { Button } from '../Button';
import { FormattedMessage } from 'react-intl';

const customStyles = {
    content : {
      backgroundColor       : 'var(--secondary-theme-color)',
      top                   : '50%',
      left                  : '50%',
      right                 : 'auto',
      bottom                : 'auto',
      marginRight           : '-50%',
      transform             : 'translate(-50%, -50%)'
    }
};

export function ConfirmModal(props) {
    const onConfirm = props.onConfirm;
    const onCancel = props.onCancel;

    return <Modal ariaHideApp={false} style={customStyles} isOpen={props.isOpen} onRequestClose={() => onCancel()}>
        <h2>{props.title}</h2>
        <p>{props.description}</p>
        <Button onClick={() => onCancel()}><FormattedMessage id="GameUI_Cancel"></FormattedMessage></Button>
        <Button onClick={() => onConfirm()}><FormattedMessage id="GameUI_OK"></FormattedMessage></Button>
    </Modal>
}

ConfirmModal.propTypes = {
    title: PropTypes.any,
    description: PropTypes.any,
    onConfirm: PropTypes.func.isRequired,
    onCancel: PropTypes.func.isRequired,
    isOpen: PropTypes.bool.isRequired,
};