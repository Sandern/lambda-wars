import React from 'react';
import styled from 'styled-components';

const Container = styled.div`
    display: flex;
    flex: 1 1 auto;
    justify-content: center;
    overflow-y: hidden;
`;

export const ContentItem = styled.div`
    width: 100%;
    max-width: 120rem;
    margin-top: 2rem;
    height: calc(100% - 5rem); 
    overflow-y: auto;
    overflow-x: hidden;
`;

/**
 * Content block for main menu that fills the used space.
 */
export function Content(props) {
    return (<Container>
        <ContentItem>{ props.children }</ContentItem>
    </Container>)
}