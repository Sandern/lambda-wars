import ReactSelect from 'react-select';
import styled from 'styled-components';

// Reset font color
export const Select = styled(ReactSelect)`
    color: black;
`;