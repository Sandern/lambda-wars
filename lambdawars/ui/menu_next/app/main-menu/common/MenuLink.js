import { Link } from "react-router-dom";
import styled from 'styled-components';

/**
 * A link in sub menus.
 * 
 */
export const MenuLink = styled(Link)`

`;
