import styled from "styled-components";

export const MenuList = styled.ul`
    margin: 0;
    padding: 0;
    list-style-type: none;

    li {
        margin: 0;
        padding: 0;
    }
`;
