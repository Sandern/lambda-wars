/**
 * Very simple helper that keeps a list of muted services globally.
 * This is reset when the game restarts.
 */

import { BehaviorSubject } from "rxjs";

const mutedUsers = new Set([]);
const mutedUsersSubject = BehaviorSubject(mutedUsers);

export const mutedUsersObservable = mutedUsersSubject.asObservable();

export function muteUser(steamid) {
    mutedUsers.add(steamid);
    mutedUsersSubject.next(mutedUsers);
}
