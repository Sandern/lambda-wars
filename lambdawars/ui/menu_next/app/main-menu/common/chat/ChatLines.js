import React from 'react';
import { FormattedMessage } from 'react-intl';
import styled from 'styled-components';

import { useObservable } from '../../../observable-hook';
import { map } from 'rxjs/operators';
import { getStoreObservableAt } from '../../../gameui-store';
import ReactTooltip from 'react-tooltip';

const ChatMessages = styled.div`
    height: calc(100% - 10rem);
    overflow-y: auto;
    overflow-x: hidden;
`;

const ChatLine = styled.section`
	color: rgb(207, 136, 13);
	font-size: 1.4rem;

    padding: 0;
    margin: 0;
    word-wrap: break-word;
`;

const UserName = styled.span`
    color: rgb(255, 0, 0);

	&:hover{
		text-decoration: underline;
	}
`;

const UserNameAdmin = styled(UserName)`
    color: rgb(255, 0, 0);
`;

const GlobalMessage = styled.span`
    font-weight: bold;
    color: #80BFFF;
`;

import starImg from '../../../../../mainmenu/smilies/star.png';
import rebelImg from '../../../../../mainmenu/smilies/rebel.png';
import combineImg from '../../../../../mainmenu/smilies/combine.png';
import lambdaImg from '../../../../../mainmenu/smilies/lambda.png';
import nuiImg from '../../../../../mainmenu/smilies/nui.png';

import smileImg from '../../../../../mainmenu/smilies/smile.png';
import sadImg from '../../../../../mainmenu/smilies/sad.png';
import bigSmileImg from '../../../../../mainmenu/smilies/bigsmile.png';
import deadImg from '../../../../../mainmenu/smilies/dead.png';
import angryImg from '../../../../../mainmenu/smilies/angry.png';
import stareImg from '../../../../../mainmenu/smilies/stare.png';
import cryImg from '../../../../../mainmenu/smilies/cry.png';

import noImg from '../../../../../mainmenu/smilies/no.png';
import yesImg from '../../../../../mainmenu/smilies/yes.png';
import okImg from '../../../../../mainmenu/smilies/ok.png';
import goImg from '../../../../../mainmenu/smilies/go.png';
import glImg from '../../../../../mainmenu/smilies/gl.png';
import hfImg from '../../../../../mainmenu/smilies/hf.png';
import ggImg from '../../../../../mainmenu/smilies/gg.png';

import headcrabImg from '../../../../../mainmenu/smilies/headcrab.png';
import cookieImg from '../../../../../mainmenu/smilies/cookie.png';
import zzzImg from '../../../../../mainmenu/smilies/zzz.png';
import errorImg from '../../../../../mainmenu/smilies/error.png';
import fireImg from '../../../../../mainmenu/smilies/fire.png';
import luckImg from '../../../../../mainmenu/smilies/luck.png';

const emojiToImg = {
    '#star': <img src={starImg} />,
    '#st': <img src={starImg} />,
    '#rebel': <img src={rebelImg} />,
    '#r': <img src={rebelImg} />,
    '#combine': <img src={combineImg} />,
    '#c': <img src={combineImg} />,
    '#lambda': <img src={lambdaImg} />,
    '#l': <img src={lambdaImg} />,
    '#nui': <img src={nuiImg} />,

    '#smile': <img src={smileImg} />,
    '#s': <img src={smileImg} />,
    '#sad': <img src={sadImg} />,
    '#ss': <img src={sadImg} />,
    '#bigsmile': <img src={bigSmileImg} />,
    '#bs': <img src={bigSmileImg} />,
    '#dead': <img src={deadImg} />,
    '#ds': <img src={deadImg} />,
    '#angry': <img src={angryImg} />,
    '#a': <img src={angryImg} />,
    '#stare': <img src={stareImg} />,
    '#sta': <img src={stareImg} />,
    '#cry': <img src={cryImg} />,

    '#no': <img src={noImg} />,
    '#yes': <img src={yesImg} />,
    '#ok': <img src={okImg} />,
    '#go': <img src={goImg} />,
    '#gl': <img src={glImg} />,
    '#hf': <img src={hfImg} />,
    '#gg': <img src={ggImg} />,

    '#headcrab': <img src={headcrabImg} />,
    '#hc': <img src={headcrabImg} />,
    '#cookie': <img src={cookieImg} />,
    '#coo': <img src={cookieImg} />,
    '#zzz': <img src={zzzImg} />,
    '#error': <img src={errorImg} />,
    '#fire': <img src={fireImg} />,
    '#luck': <img src={luckImg} />,

    '#br': <br/>
};
const emojiRegExp = /(#star|#st|#rebel|#r|#combine|#c|#lambda|#luck|#l|#nui|#smile|#s|#sad|#ss|#bigsmile|#bs|#dead|#ds|#angry|#a|#stare|#sta|#cry|#no|#yes|#ok|#go|#gg|#gl|#hf|#gg|#headcrab|#hc|#cookie|#coo|#zzz|#error|#fire|#br)/gi;

import vortalStormImg from '../../../../../mainmenu/smilies/special/vortalstorm_a.gif';

const devIds = new Set([
    '76561197967932376', // Sandern
    '76561197970934689', // ProgSys
    '76561197974982763', // JJ
    '76561198034846730', // HevCrab
    '76561197989598431', // Pandango
    '76561197995806465', // Mr Darkness
    '76561197975989080', // SBeast
    '76561198188813547', // BarraBarraTigr
    '76561198223952276', // Mr.Lazy
]);

export function ChatLines(props) {
    const { source, welcomeMsgId } = props;
    const lobbyMembers = useObservable(getStoreObservableAt(`${source}:lobby-members`), {});
    const chatHistory = useObservable(getStoreObservableAt(`${source}:history`).pipe(map((entries) => {
        for (const entry of entries) {
            if (!entry.steamid) {
                entry.type = 'global';
            } else {
                entry.type = devIds.has(entry.steamid) ? 'admin' : 'user';
            }
        }
        return entries;
    })), []);

    return (<ChatMessages>
        { welcomeMsgId ? <GlobalMessage>
            <FormattedMessage id={welcomeMsgId}>{ text => renderText(text[0]) }</FormattedMessage>
        </GlobalMessage> : undefined }
        {chatHistory.map((entry) => <ChatLine key={entry.id}>{renderChatHistoryEntry(entry, lobbyMembers)}</ChatLine>)}
    </ChatMessages>);
}

function renderChatHistoryEntry(entry, lobbyMembers) {
    if (entry.type === 'global') {
        return <GlobalMessage >{renderText(entry.text)}</GlobalMessage>;
    } else if (entry.type === 'admin') {
        return [
            <img key="admin"  src={vortalStormImg} data-tip data-for='developer' />,
            <UserNameAdmin key="userName">{lobbyMembers[entry.steamid]?.username || 'Error'}:</UserNameAdmin>, 
            <span key="text"> {renderText(entry.text)}</span>,
            <ReactTooltip key="developerTooltip" id="developer">Vortal Storm Developer</ReactTooltip>,
        ];
    }

    return [<UserName key="userName">{lobbyMembers[entry.steamid]?.username || 'Error'}:</UserName>, <span key="text"> {renderText(entry.text)}</span>];
}

function renderText(text) {
    return text.split(emojiRegExp).map((textPart, idx) => {
        return <span key={idx}>{emojiToImg[textPart] || textPart}</span>;
    })
}