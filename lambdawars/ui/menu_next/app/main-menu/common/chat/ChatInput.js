import React, { useState } from 'react';
import { FormattedMessage } from "react-intl";

import { useObservable } from '../../../observable-hook';
import { getStoreObservableAt, GENERAL_INPUT_LANGUAGE } from '../../../gameui-store';
import { Button } from '../Button';

export function ChatInput(props) {
    const { chatApi } = props;
    const inputLanguage = useObservable(getStoreObservableAt(GENERAL_INPUT_LANGUAGE), '');

    const [text, setText] = useState('');

    function sendChat(event) {
        event.preventDefault();
        
        if (text.replace(/\s/g, '').length === 0  ) {
            return false;
        }
        chatApi.sendchatmessage(text);
        setText('');
        // Prevent reload of page because of using a form
        return false;
    }    

    return (<div className="chat-controls">
        <form onSubmit={sendChat} className="chat-form">
            <input maxLength={100} type="text" className="chat-input" value={text} onChange={(e) => setText(e.target.value)} placeholder={`[${inputLanguage}]`} />
            <Button onClick={sendChat}><FormattedMessage id='Chat_Send'></FormattedMessage></Button>
        </form>
    </div>);
}
