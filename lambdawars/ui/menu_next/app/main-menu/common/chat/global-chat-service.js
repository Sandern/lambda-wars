/**
 * Automatically join/leave the global chat lobby when in the main menu.
 */
import { getStoreObservableAt, GENERAL_USER_STATE } from '../../../gameui-store';

let lastWasIngame = undefined;

getStoreObservableAt(GENERAL_USER_STATE).subscribe((userState) => {
    if (userState.inGame === lastWasIngame) {
        return;
    }
    updateGlobalChatLobby(userState.inGame);
    lastWasIngame = userState.inGame;
});

function updateGlobalChatLobby(inGame) {
    console.log('updateGlobalChatLobby: ', inGame);
    if( inGame ) {
        window.globalchat.leavelobby();
    } else {
        window.globalchat.listlobbies(function(chatLobbies) {
            console.log('chatLobbies: ', chatLobbies);
            if( chatLobbies.length === 0 ) {
                window.globalchat.createlobby('Global Chat Lobby');
            } else {
                window.globalchat.joinlobby(chatLobbies[0].steamid);
            }
        });
    }
}
