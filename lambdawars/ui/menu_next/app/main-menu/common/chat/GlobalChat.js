import React from 'react';
import { FormattedMessage } from "react-intl";
import { ChatLines } from './ChatLines';
import { ChatInput } from './ChatInput';

import { useObservable } from '../../../observable-hook';
import { getStoreObservableAt, GENERAL_NUM_CURRENT_PLAYERS } from '../../../gameui-store';

import './global-chat-service';
import styled from 'styled-components';

const GlobalChatSection = styled.section`
    height: 100%;

    h1 {
        color: var(--primary-theme-color);

        span {
            float: right;
            font-size: 1.4rem;
            padding-top: 0.5rem;

            @media (max-width: 1180px) {
                font-size: 1.0rem;
            }
        }
    }
`;

export function GlobalChat() {
    const numberOfCurrentPlayers = useObservable(getStoreObservableAt(GENERAL_NUM_CURRENT_PLAYERS));

    return (<GlobalChatSection>
        <h1><FormattedMessage id='Chat_Global'></FormattedMessage><span>{ numberOfCurrentPlayers } <FormattedMessage id='Chat_PlayersOnline'></FormattedMessage></span></h1>
        <ChatLines source="globalchat" welcomeMsgId="Chat_WelcomeMsg"></ChatLines>
        <ChatInput chatApi={globalchat}></ChatInput>
    </GlobalChatSection>);
}
