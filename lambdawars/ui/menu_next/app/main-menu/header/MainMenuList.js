import { MenuList } from '../common/MenuList';
import styled from 'styled-components';

export const MainMenuList = styled(MenuList)`
    margin: 0;
    float: left;

    li {
        display: inline-block;
    }
`;