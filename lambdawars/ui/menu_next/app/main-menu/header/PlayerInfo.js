
/**
 * Shows player name and avatar.
 */
import React from 'react';
import {FormattedMessage} from 'react-intl';
import { Link, useRouteMatch } from "react-router-dom";
import styled from "styled-components";

import { useObservable } from '../../observable-hook';
import { getStoreObservableAt, GENERAL_USER_INFO } from '../../gameui-store';
import ReactTooltip from 'react-tooltip';

const PlayerInfoDiv = styled.div`
	float: right;
	color: var(--button-text-color);
	text-align: left;
	font-size: 1.1rem;
	height: 7.2rem;
	line-height: 1.4rem;
	font-family: 'Roboto';
	text-transform: uppercase;
	position: relative;
	padding-right: 6.2rem;
	padding-top: 1.6rem;
	padding-left: 1rem;
`;

const AvatarLink = styled(Link)`
    img {
        position: absolute;
        top: 1rem;
        right: 0;
        width: 5.2rem;
        height: 5.2rem;
    }
`;

const PlayerLink = styled(Link)`
    color: white;
    font-size: 2.2rem;
    line-height: 2.6rem;
    text-transform: none;
    display: block;
    max-width: 20rem;
    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;

    @media (max-width: 1300px) {
        max-width: 15rem;
    }
`;

const PlayerNameTooltip = styled.span`
    color: orange;
`;

export function PlayerInfo() {
    const userInfo = useObservable(getStoreObservableAt(GENERAL_USER_INFO), {});
    const { url } = useRouteMatch();

    return (<PlayerInfoDiv>
        <AvatarLink data-tip data-for='openProfile' to={`${url}/profile`}><img src={ `avatar://medium/${userInfo.steamid}?${new Date().getTime()}` }/></AvatarLink>
        <PlayerLink data-tip data-for='openProfile' to={`${url}/profile`}>
            { userInfo.name }    
        </PlayerLink>
        <ReactTooltip id='openProfile'>
            <PlayerNameTooltip><FormattedMessage id="Menu_TooltipOpenProfile"></FormattedMessage> <span>{ userInfo.name }</span></PlayerNameTooltip>
        </ReactTooltip>
    </PlayerInfoDiv>);
}