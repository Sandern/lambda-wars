import { Link } from "react-router-dom";
import styled from 'styled-components';

/**
 * Lambda Wars main menu button.
 * Big buttons with an orange underline on hover.
 * 
 */
export const MenuLink = styled(Link)`
    position: relative;
    display: block;
    font-family: 'Roboto Condensed';
    font-size: 2.8rem;
    line-height: 2.8rem;
    padding: 2.2rem 0;
    margin: 0 2rem;

    @media (max-width: 1600px) {
        max-width: 15rem;
        overflow: hidden;
        text-overflow: ellipsis;
        white-space: nowrap;
    }

    text-transform: uppercase;
    background: transparent;
    color: var(--button-text-color);

    transition: all .4s;

    :hover {
        color: var(--button-text-color-hover);

        ::after {
            width: 100%;
            left: 0;
        }
    }

    ::after {
        content: "";
        position: absolute;
        display: block;
        top: 0;
        left: 50%;
        height: 100%;
        width: 0;
        border-bottom: 0.4rem solid var(--button-text-color-hover);
        transition: all .4s;
    }
`;
