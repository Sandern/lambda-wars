import { Link } from "react-router-dom";
import styled from 'styled-components';

/**
 * Menu link having an image as content. Used for the Lambda wars logo.
 */
export const LogoLink = styled(Link)`
    margin: 1rem 0 0 2rem;
    float: left;
    opacity: 0.85;
    transition: all .2s;

    img {
        float: left;
    }

    &:hover {
        opacity: 1;
    }
`;