// Import available fonts
import './_font_open_sans.css';
import './_font_roboto.css';
import './_font_roboto_condensed.css';

// Main.css should only contain general styling!
// Prefer styled components over adding global css.
import './main.css';
