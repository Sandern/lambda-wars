/**
 * Defines the main routes for main menu, loading and intro.
 */
import React from 'react';
import ReactDOM from 'react-dom';
import {
    HashRouter as Router,
    Switch,
    Route,
    Redirect,
  } from "react-router-dom";
import {IntlProvider} from 'react-intl';

import { MainMenu } from './main-menu/MainMenu';
import { LoadingScreen } from './loading-screen/LoadingScreen';

import './styles/bootstrap';

export function App() {
    return (<Router>
        <Switch>
            <Route path="/main-menu">
                <MainMenu></MainMenu>
            </Route>
            <Route path="/intro">
                <h1>INSERT INTRO HERE</h1>
            </Route>
            <Route path="/loading">
                <LoadingScreen></LoadingScreen>
            </Route>
            <Route>
                <Redirect to="main-menu" />
            </Route>
        </Switch>
    </Router>);
}

// create and bootstrap application
// This function is called from the game code (python/menu/mainmenu.py)
function init_mainmenu(translations) {
    ReactDOM.render(<IntlProvider  locale="en" messages={translations}><App /></IntlProvider>, document.getElementById('appRoot'));
}
window['init_mainmenu'] = init_mainmenu;


function onBodyResize(/*e*/) {
    const fontSize = 10 * (document.body.clientHeight / 1080);
    console.log('set font size to: ', fontSize);
    document.documentElement.style.fontSize = `${fontSize}px`;
    document.body.style.fontSize = `${fontSize}px`;
}

window.addEventListener('resize', onBodyResize);
onBodyResize();