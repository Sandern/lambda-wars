import React from 'react';
import styled, { keyframes } from "styled-components";

const showHideDot = keyframes`
    0% { opacity: 0; }
    50% { opacity: 1; }
    60% { opacity: 1; }
    100% { opacity: 0; }
`;

const LoadingContainer = styled.div`
    display: flex;
    align-items: center;
    justify-content: center;
`;

const LoadingDots = styled.div`
    text-align: center;
    z-index: 5;
    font-size: 8em;
`;

const Dot = styled.h1`
    display: inline;
    margin-left: 0.2em;
    margin-right: 0.2em;
    position: relative;
    top: -1em;
    font-size: 3.5em;
    opacity: 0;
    animation: ${showHideDot} 2.5s ease-in-out infinite;
`;

const DotOne = styled(Dot)`
    animation-delay: 0.2s;
`;

const DotTwo = styled(Dot)`
    animation-delay: 0.4s;
`;

const DotThree = styled(Dot)`
    animation-delay: 0.6s;
`;

export function LoadingScreen() {
    return (<LoadingContainer>
        <LoadingDots>
            <h1>Loading</h1>
            <DotOne>.</DotOne><DotTwo>.</DotTwo><DotThree>.</DotThree>
        </LoadingDots>
    </LoadingContainer>);
}