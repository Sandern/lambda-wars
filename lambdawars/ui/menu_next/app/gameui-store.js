/**
 * A simple store around rxjs Observables to which the game code can push updates.
 * The store values here are all global and more general information.
 */
import { ReplaySubject } from "rxjs";
import { useObservable } from "./observable-hook";

// List of possible store keys

/**
 * {
 *     name: string,
 *     steamid: string,
 *     steamAvailable: boolean,
 *     devBranch: string,
 *     gameVersion: string,
 * }
 */
export const GENERAL_USER_INFO = 'general:user-info';

/**
 * {
 *     inGame: boolean,
 *     isHosting: boolean,
 *     isOffline: boolean,
 * }
 */

export const GENERAL_USER_STATE = 'general:user-state';

/**
 * User Input language (string value)
 */
export const GENERAL_INPUT_LANGUAGE = 'general:input-language';

/**
 * Number of current players in Lambda Wars
 */
export const GENERAL_NUM_CURRENT_PLAYERS = 'general:number-of-current-players';

/**
 * Number of players when the player is hosting a server.
 */
export const HOSTING_NUM_PLAYERS = 'hosting:host-num-players';

/**
 * Lobby stores
 */
export const GLOBAL_CHAT_LOBBY_MEMBERS = 'globalchat:lobby-members';
export const GLOBAL_CHAT_HISTORY = 'globalchat:history';

/**
 * Game lobby stores
 */
export const GAMELOBBY_STATE = 'gamelobby:state';
export const GAMELOBBY_SLOTS = 'gamelobby:slots';

/**
 * Hotkeys/bindings.
 */
export const SETTINGS_HOTKEYS = 'settings:hotkeys';

/**
 * Video settings.
 */
export const SETTINGS_VIDEO = 'settings:video';

/**
 * Store implementation. A map from store key to BehaviorSubjects.
 * Observables can be retrieved from these stores.
 * Values can be pushed, usually from the game code.
 */
const store = new Map();

export function getStoreObservableAt(key) {
    return getStore(key).asObservable();
}

export function useStoreObservableAt(key, initialValue, deps) {
    return useObservable(getStoreObservableAt(key), initialValue, deps);
}

/**
 * @param {string} key 
 * @param {any} value 
 */
export function pushValueToStore(key, value) {
    getStore(key).next(value);   
}

// Called by game code to push values to a behavior subject
window['pushValueToStore'] = pushValueToStore;

/**
 * @param {string} key
 * @returns {import('rxjs').Observable<any>} 
 */
function getStore(key) {
    if (!store.has(key)) {
        store.set(key, new ReplaySubject(1))
    }
    return store.get(key);
}


