# Requirements:
- NodeJS 12 or higher: https://nodejs.org
- NPM 6.0 or higher: https://www.npmjs.com/

# Bundle
```sh
$ npm install
$ npm run build
```

You can add `npm run watch` command to development purposes.

# Long Setup instructions:
1. Install NodeJS & NPM
1. Open terminal or Windows PowerShell
1. `$ cd <path to this directory>`
1. `$ npm install`
1. `$ npm run watch`
1. Start the game with the launch parameter `-cef_remote_dbg_port 8080`. Make sure to run in `windowed` mode.
1. Open Chrome and navigate to `http://localhost:8080`.
1. Select main menu page. Now you can use the dev tools to inspect and reload the page!

# Before committing:
1. Run: `$ npm run build`
1. Run: `$ npm run test`
1. Make sure all files in the `dist` folder are added or removed
