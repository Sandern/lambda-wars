import React from 'react';
import { render } from '@testing-library/react';
import { getTranslations } from './translations';
import { IntlProvider } from 'react-intl';

export function renderWithTranslations(component) {
    render(<IntlProvider locale='en' messages={getTranslations()}>{component}</IntlProvider>);
}