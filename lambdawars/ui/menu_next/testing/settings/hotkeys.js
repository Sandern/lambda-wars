/**
 * Setup mock implementation for hotkeys
 */
export function setupHotkeys(defaultHotkeys) {
    const hotkeys = JSON.parse(JSON.stringify(defaultHotkeys));

    window.hotkeys = {
        refreshHotkeys: function () {
            window['pushValueToStore']('settings:hotkeys', hotkeys);
        },
        resetToDefault: function () {

        },
    };
}

export const defaultHotkeysConfig = [
    {
        'displayName': 'Bindings Title 1',
        'type': 'sectionTitle',
    },
    {
        'displayName': 'Hotkey 1',
        'binding': '+hotkey1',
        'key': 'a',
    },
    {
        'displayName': 'Hotkey 2',
        'binding': '+hotkey2',
        'key': '',
    },
    {
        'displayName': 'Bindings Title 2',
        'type': 'sectionTitle',
    },
    {
        'displayName': 'Hotkey 3',
        'binding': '+hotkey3',
        'key': 'b',
    },
    {
        'displayName': 'Hotkey 4',
        'binding': '+hotkey4',
        'key': 'c',
    },
];
