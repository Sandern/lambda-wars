/**
 * Setup mock implementation for video
 */
export function setupVideoSettings(videoSettings) {
    window.video = {
        refreshVideoSettings: function () {
            window['pushValueToStore']('settings:video', JSON.parse(JSON.stringify(videoSettings)));
        },
        apply: function (newVideoSettings) {
            videoSettings = newVideoSettings;
            window['pushValueToStore']('settings:video', JSON.parse(JSON.stringify(videoSettings)));
        }
    };
}

export const defaultVideoSettings = {
    // 0 = windowed, 1 = fullscreen, 2 = fullscreen windowed
    displayMode: 1,
    // width and height represents the active resolution
    resolution: { width: 2560, height: 1440 },

    // desktop width and height is that of the desktop, usually the monitor resolution
    // In Windowed modes the resolution can't be higher than this.
    desktopResolution: { width: 2560, height: 1440 },

    // The list of possible resolutions for fullscreen
    fullScreenResolutions: [
        { width: 640, height: 480 },
        { width: 1024, height: 768 },
        { width: 1920, height: 1080 },
        { width: 1920, height: 1200 },
        { width: 2560, height: 1440 },
    ],
    windowedResolutions: [
        { width: 640, height: 480 },
        { width: 1024, height: 768 },
        { width: 1920, height: 1080 },
        { width: 1920, height: 1200 },
        { width: 2560, height: 1440 },
    ],
}