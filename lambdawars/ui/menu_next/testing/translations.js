/**
 * Quick and dirty solution for translations from valve resource files.
 */
const fs = require('fs');
const path = require('path');

let cachedTranslations;

export function getTranslations() {
    if (!cachedTranslations) {
        cachedTranslations = Object.assign(
            {},
            readTranslations(path.resolve(__dirname, '../../../resource/gameui_english.txt'), 'utf16le'),
            readTranslations(path.resolve(__dirname, '../../../resource/lambdawars_ui_english.txt'), 'utf-8'),
        )
    }
    return cachedTranslations;
}

export function translate(id) {
    return getTranslations()[id];
}

function readTranslations(filePath, encoding) {
    const messages = {};
    for (const line of fs.readFileSync(filePath, encoding).split(/\r?\n/)) {
        const trimmedLine = line.trim();
        if (!trimmedLine || trimmedLine.startsWith('//') || trimmedLine.startsWith('{') || trimmedLine.startsWith('}')) {
            continue;
        }
        const match = trimmedLine.match(/"(.*)"\s.*"(.*)"/);
        if (!match) {
            continue;
        }
        messages[match[1]] = match[2];
    }
    return messages;
}
