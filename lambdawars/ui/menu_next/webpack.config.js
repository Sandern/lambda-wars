const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyPlugin = require('copy-webpack-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');

module.exports = (env) => {
  console.log(env);
  const isProduction = env === 'production';
  return {
    entry: './app/app.js',
    output: {
      filename: 'bundle.[hash].js',
      path: path.resolve(__dirname, 'dist')
    },
    optimization: {
      minimize: false
    },
    plugins: (isProduction ? [
      new CleanWebpackPlugin()
    ] : []).concat([
      new webpack.optimize.ModuleConcatenationPlugin(),
      new HtmlWebpackPlugin({
        title: 'Lambda Wars Main Menu',
        template: './app/index.ejs', // Load a custom template (ejs by default see the FAQ for details)
      }),
      new CopyPlugin({
        patterns: [
          // Copy images that are not required through webpack yet
          { from: 'app/images/missionimg', to: 'images/missionimg' },
        ]
      })
    ]),
    module: {
      rules: [
        {
          test: /\.jsx?$/,
          exclude: /node_modules/,
          use: [
            "babel-loader",
            "eslint-loader",
          ]
        },
        {
          test: /\.css$/,
          use: [ 'style-loader', 'css-loader' ]
        },
        {
          test: /\.html$/,
          use: [ {
            loader: 'html-loader',
            options: { minimize: false }
          }],
        },
        {
          test: /\.(png|jpg|gif)$/,
          use: [
            {
              loader: 'url-loader',
              options: {
                limit: 8192
              }
            }
          ]
        },
        {
          test: /\.(woff|woff2)$/,
          loader: "file-loader",
          options: {
            name: "fonts/[name].[ext]",
          },
        }
      ]
    },
    devtool: isProduction ? undefined : 'inline-source-map'
  };
};