const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');

module.exports = (env) => {
  console.log(env);
  const isProduction = env === 'production';
  return {
    entry: {
      app: './viewport.js'
    },
    output: {
      filename: '[name].[hash].js',
      path: path.resolve(__dirname, 'dist')
    },
    optimization: {
      splitChunks: {
        cacheGroups: {
          commons: {
            test: /[\\/]node_modules[\\/]|[\\/]jqwidgets[\\/]|[\\/]libs[\\/]/,
            name: "vendors",
            chunks: "all"
          }
        }
      }
    },
    plugins: [
      new webpack.optimize.ModuleConcatenationPlugin(),
      new webpack.ProvidePlugin({
          $: "jquery",
          jQuery: "jquery",
          "window.jQuery": "jquery"
      }),
      new HtmlWebpackPlugin({
        title: 'Viewport',
        template: 'src/index.ejs', // Load a custom template (ejs by default see the FAQ for details) 
      }),
      new CleanWebpackPlugin(),
    ],
    module: {
      rules: [
        {
          test : /\.jsx?/,
          exclude: [
            /node_modules/,
            /jqwidgets/
          ],
          use: [
            "babel-loader",
            "eslint-loader",
          ]
        },
        {
          test: /\.scss$/,
          use: [{
              loader: "style-loader" // creates style nodes from JS strings
          }, {
              loader: "css-loader" // translates CSS into CommonJS
          }, {
              loader: "sass-loader" // compiles Sass to CSS
          }]
        },
        {
          test: /\.css$/,
          use: [ 'style-loader', 'css-loader' ]
        },
        {
          test: /\.(png|jpg|gif)$/,
          use: [
            {
              loader: 'url-loader',
              options: {
                limit: 8192
              }
            }
          ]
        },
        { 
          test: /\.(woff|woff2)$/,
          loader: 'url-loader',
          options: {
            limit: 65000,
            mimetype: 'application/font-woff2',
            name: 'dist/fonts/[name].[ext]'
          }
        }
      ]
    },
    devtool: isProduction ? undefined : 'inline-source-map'
  };
};