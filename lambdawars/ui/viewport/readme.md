# Requirements:
- NodeJS 8 or higher: https://nodejs.org
- Yarn 1.7.0 or higher: https://yarnpkg.com/en/docs/install

# Install Dependencies
$ yarn install

# Creating the production bundle
To create the production bundle, run:

$ yarn run build

These files should be committed when you make changes

# Development using "watch" mode.
In watch mode it will continuesly update the development bundle to the dist folder:

$ yarn run watch

In this mode you need to refresh the main menu page to see the changes. To do so, start the game with `-cef_remote_dbg_port 8081` and navigate in your Chrome browser to http://localhost:8081. You will now see the inspectable pages listed. You can simply hit the refresh button when inspecting the page to reload for changes. Of course you can also inspect elements and use the console!
