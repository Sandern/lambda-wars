const $ = require('jquery');

// Tell game process if the body element has focus or not. This way we can decide if key input should be processed in game.
$("body").focusin(function() {
	window['interface'].setCefFocus(true);
});

$("body").focusout(function() {
	window['interface'].setCefFocus(false);
});

require('./src/main');

// jqWidgets
window.getTheme = function () {
	return 'wars';
}
