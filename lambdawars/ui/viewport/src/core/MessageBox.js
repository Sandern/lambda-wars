import React from 'react';
import WarsComponent from '../WarsComponent';
import Typist from 'react-typist';
import { TransitionGroup, CSSTransition } from 'react-transition-group';
import './messagebox.scss';

import boxBgImage from '../../images/messagebox/box_bg.png';
import buttonImage from '../../images/messagebox/button.png';
import buttonDisabledImage from '../../images/messagebox/button_disabled.png';
import buttonOverImage from '../../images/messagebox/button_over.png';
import boxImage from '../../images/messagebox/box.png';

/**
 * Controls and renders the message box.
 * Mostly intended for Single Player.
 * Allows providing information to the player.
 */
export default class MessageBox extends WarsComponent {
    constructor(props) {
        super(props);

        this.state = {
            // Text displayed in message box.
            text: '',
            // Disable interaction with message box, preventing clicking on buttons.
            locked: false,
            // Indicates typing effect is done for current message
            typingDone: false,
            // Speedup typing (on first click message box)
            speedupTyping: false,
        };
    }

    /**
     * Locks message box from being closable by clicking.
     * Called by game code from core/ui/messageboxdialog.py
     */
    lockMessageBox() {
        this.setState({
            locked: true,
        });
    }

    /**
     * Unlocks message box, making it closable again by clicking.
     * Called by game code from core/ui/messageboxdialog.py
     */
    unlockMessageBox() {
        this.setState({
            locked: false,
        });
    }

    /**
     * Smooth close of message box.
     * Without smooth option the message box disappears directly upon click.
     * Called by game code from core/ui/messageboxdialog.py
     */
    smoothCloseMessageBox() {
        // TODO: still needed?
    }

    /**
     * Sets message box text.
     * Called by game code from core/ui/messageboxdialog.py
     */
    setText(text) {
        this.setState({
            text: text,
            typingDone: false,
            speedupTyping: false,
        });
    }

    /**
     * Render the message box.
     */
    render() {
        const { visible, text, locked, mouseOver, speedupTyping, typingDone } = this.state;

        const buttonEnabled = !locked && (speedupTyping || typingDone);

        return (
            <TransitionGroup>
                {visible ? (<CSSTransition classNames="wars_messagebox" timeout={{ enter: 200, exit: 400 }}>
                    <div key="messagebox" style={this.getContainerStyle()}>
                        <img id="wars_messagebox_bg" src={boxBgImage} alt="bg" />
                        <div id="wars_messagebox_container" 
                            onClick={this.onClick.bind(this)} 
                            onMouseEnter={this.onMouseEnter.bind(this)}
                            onMouseLeave={this.onMouseLeave.bind(this)}
                        >
                            <img id="wars_messagebox_button" src={buttonEnabled ? (mouseOver ? buttonOverImage : buttonImage) : buttonDisabledImage} alt="button" />
                            <img id="wars_messagebox_box" src={boxImage} alt="box" />

                            <div id="wars_messagebox_text_container">
                                <div id="wars_messagebox_text">
                                    {this.state.text ? (
                                    <Typist 
                                        onTypingDone={this.onTypingDone.bind(this)}
                                        avgTypingDelay={speedupTyping ? 1 : 70}
                                    >{text}</Typist>
                                    ) : null}
                                </div>
                            </div>
                        </div>
                    </div>
                </CSSTransition>) : null}
            </TransitionGroup>
        );
    }

    /**
     * Returns the overall style.
     * Controls visiblity and scale.
     */
    getContainerStyle() {
        return {
            transform: `scale(${window.outerHeight / 900.0})`,
        };
    }

    /**
     * Handles click on the message box.
     */
    onClick() {
        if (this.state.speedupTyping || this.state.typingDone) {
            if (!this.state.locked) {
                this.gameApi.onClose();
            }
        } else {
            // Speedup typing on first click
            this.setState({
                speedupTyping: true,
            });
        }
    }

    /**
     * Sets mouse over state.
     */
    onMouseEnter() {
        this.setState({ mouseOver: true });
    }

    /**
     * Clears mouse over state.
     */
    onMouseLeave() {
        this.setState({ mouseOver: false });
    }

    /**
     * Handles typing done.
     */
    onTypingDone() {
        this.setState({
            typingDone: true,
        });
    }
}