import React from 'react';
import WarsComponent from '../WarsComponent';
import Countdown from 'react-countdown';
import { FormattedMessage } from 'react-intl';
import { zeroPad } from '../utils/common';
import './waitingforplayers.scss';

/**
 * Shows waiting for players panel, before match is started.
 */
export default class WaitingForPlayersPanel extends WarsComponent {
    constructor(props) {
        super(props);

        this.state.banner = {};
        this.state.motd = {};
        this.state.gamePlayers = [];
    }

    /**
     * Called from game to update banner by url.
     * 
     * @param {*} bannerUrl 
     */
    updateBanner(bannerUrl) {
        this.setState({
            banner: {
                url: bannerUrl,
                content: null,
            },
        });
    }

    /**
     * Called by game to update banner by html content.
     * @param {*} bannerContent 
     */
    updateBannerFromContent(bannerContent) {
        this.setState({
            banner: {
                url: null,
                content: bannerContent,
            },
        });
    }
    
    /**
     * Called by game to update message of the day by url.
     * 
     * @param {*} motdUrl 
     */
    updateMOTD(motdUrl) {
        this.setState({
            motd: {
                url: motdUrl,
                content: '',
            },
        });
    }
    /**
     * Called by game to update message of the day by html content.
     * 
     * @param {*} motdContent 
     */
    updateMOTDFromContent(motdContent) {
        this.setState({
            motd: {
                url: '',
                content: motdContent,
            },
        });
    }

    /**
     * Called by game to update players.
     * 
     * @param {*} timeoutSeconds 
     * @param {*} title 
     * @param {*} gamePlayers 
     */
    updatePanel(timeoutSeconds, title, gamePlayers) {
        this.setState({
            timeoutSeconds: timeoutSeconds,
            title: title,
            gamePlayers: gamePlayers,
        });
    }

    render() {
        return (
            <div id="wars_waitingforplayers_container" style={this.getContainerStyle()} unselectable="on">
                <div id="wars_waitingforplayers_banner">
                    <iframe 
                        sandbox=""
                        width="100%" 
                        height="100%" 
                        scrolling="no" 
                        seamless="seamless" 
                        frameBorder="0"
                        srcDoc={this.state.banner.content}
                        src={this.state.banner.url}
                    ></iframe>
                </div>
                
                <h1 id="wars_waitingforplayers_title">
                    <span><FormattedMessage id="WFP_LobbyName" />:</span> 
                    <span id="wars_waitingforplayers_lobbyname">{this.state.title || <FormattedMessage id="WFP_WaitText" />}</span>
                    <div id="wars_waitingforplayers_countdown">
                        <Countdown 
                            date={Date.now() + (this.state.timeoutSeconds * 1000)}
                            renderer={props => <span>{zeroPad(props.minutes, 2)}:{zeroPad(props.seconds, 2)}</span>}
                         />
                    </div>
                </h1>
                    
                <ul id="wars_waitingforplayers_playerlist">
                    {this.state.gamePlayers.map(this.renderPlayerEntry.bind(this))}
                </ul>
                
                <div id="wars_waitingforplayers_motd">
                <iframe 
                        sandbox=""
                        width="100%" 
                        height="100%" 
                        scrolling="no" 
                        seamless="seamless" 
                        frameBorder="0"
                        srcDoc={this.state.motd.content}
                        src={this.state.motd.url}
                    ></iframe>
                </div>
    
            </div>
        );
    }

    getContainerStyle() {
        return {
            display: this.state.visible ? 'block' : 'none',
        };
    }

    renderPlayerEntry(player) {
        return (
            <li key={player.steamid}>
                {player.playername} <span>- state: {player.state}</span>
            </li>
        );
    }
}
