import React from 'react';
import { Component } from 'react';

import starImg from '../../../../mainmenu/smilies/star.png';
import rebelImg from '../../../../mainmenu/smilies/rebel.png';
import combineImg from '../../../../mainmenu/smilies/combine.png';
import lambdaImg from '../../../../mainmenu/smilies/lambda.png';
import nuiImg from '../../../../mainmenu/smilies/nui.png';

import smileImg from '../../../../mainmenu/smilies/smile.png';
import sadImg from '../../../../mainmenu/smilies/sad.png';
import bigSmileImg from '../../../../mainmenu/smilies/bigsmile.png';
import deadImg from '../../../../mainmenu/smilies/dead.png';
import angryImg from '../../../../mainmenu/smilies/angry.png';
import stareImg from '../../../../mainmenu/smilies/stare.png';
import cryImg from '../../../../mainmenu/smilies/cry.png';

import noImg from '../../../../mainmenu/smilies/no.png';
import yesImg from '../../../../mainmenu/smilies/yes.png';
import okImg from '../../../../mainmenu/smilies/ok.png';
import goImg from '../../../../mainmenu/smilies/go.png';
import glImg from '../../../../mainmenu/smilies/gl.png';
import hfImg from '../../../../mainmenu/smilies/hf.png';
import ggImg from '../../../../mainmenu/smilies/gg.png';

import headcrabImg from '../../../../mainmenu/smilies/headcrab.png';
import cookieImg from '../../../../mainmenu/smilies/cookie.png';
import zzzImg from '../../../../mainmenu/smilies/zzz.png';
import errorImg from '../../../../mainmenu/smilies/error.png';
import fireImg from '../../../../mainmenu/smilies/fire.png';
import luckImg from '../../../../mainmenu/smilies/luck.png';

const chatTextReplacements = {
    '#star': <img src={starImg} />,
    '#st': <img src={starImg} />,
    '#rebel': <img src={rebelImg} />,
    '#r': <img src={rebelImg} />,
    '#combine': <img src={combineImg} />,
    '#c': <img src={combineImg} />,
    '#lambda': <img src={lambdaImg} />,
    '#l': <img src={lambdaImg} />,
    '#nui': <img src={nuiImg} />,
    
    '#smile': <img src={smileImg} />,
    '#s': <img src={smileImg} />,
    '#sad': <img src={sadImg} />,
    '#ss': <img src={sadImg} />,
    '#bigsmile': <img src={bigSmileImg} />,
    '#bs': <img src={bigSmileImg} />,
    '#dead': <img src={deadImg} />,
    '#ds': <img src={deadImg} />,
    '#angry': <img src={angryImg} />,
    '#a': <img src={angryImg} />,
    '#stare': <img src={stareImg} />,
    '#sta': <img src={stareImg} />,
    '#cry': <img src={cryImg} />,
    
    '#no': <img src={noImg} />,
    '#yes': <img src={yesImg} />,
    '#ok': <img src={okImg} />,
    '#go': <img src={goImg} />,
    '#gl': <img src={glImg} />,
    '#hf': <img src={hfImg} />,
    '#gg': <img src={ggImg} />,
    
    '#headcrab': <img src={headcrabImg} />,
    '#hc': <img src={headcrabImg} />,
    '#cookie': <img src={cookieImg} />,
    '#coo': <img src={cookieImg} />,
    '#zzz': <img src={zzzImg} />,
    '#error': <img src={errorImg} />,
    '#fire': <img src={fireImg} />,
    '#luck': <img src={luckImg} />,
    
    '#br': <br/>,
};

function buildMatchRegex() {
    // Sort descending to ensure longest keys match first
    const keys = Object.keys(chatTextReplacements);
    keys.sort((a, b) => {
        return b.length - a.length;
    });
    return new RegExp(`(${keys.join('|')})`, 'gi');
}

const splitRegex = buildMatchRegex();

/**
 * Displays a chat line.
 */
export default class ChatLine extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        const line = this.props.line;

        // HMMMM....
        // TODO: Fix this up
        const msgParts = line.msg.split(splitRegex);

        if (line.notification) {
            return <div><span><b>{msgParts.map(this.renderMsgPart)}</b></span><br /></div>;
        } else {
            const style = {
                color: line.color,
                WebkitTextFillColor: line.color,
            };
            return (<div>
                <span>
                    <b><span style={style}>{line.playerName}</span>: {msgParts.map(this.renderMsgPart)}</b>
                </span><br />
            </div>);
        }
    }

    renderMsgPart(msgPart) {
        return chatTextReplacements[msgPart] || msgPart;
    }
}

