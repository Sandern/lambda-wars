import React from 'react';
import WarsComponent from '../../WarsComponent';
import ChatLine from './ChatLine';
import { TransitionGroup, CSSTransition } from 'react-transition-group';
import { FormattedMessage } from 'react-intl';
import './chat.scss';

const MODE_NONE = 0;
// const MODE_SAY = 1;
const MODE_TEAM_SAY = 2;

// How long just added lines show until they fade away
const ADDED_LINE_SHOW_TIME = 5000;

/**
 * Controls ingame player chat
 */
export default class Chat extends WarsComponent {
    constructor(props) {
        super(props);

        this.state.mode = MODE_NONE;
        this.state.inputValue = '';
        this.state.languageId = 'EN';
        this.state.history = [];
    }

    componentDidMount() {
        super.componentDidMount();

        this.scrollToBottom();
    }

    componentDidUpdate() {
        this.scrollToBottom();
    }

    scrollToBottom() {
        this.historyContainer.scrollTop = this.historyContainer.scrollHeight;
    }

    componentWillUnmount() {
        super.componentWillUnmount();

        clearTimeout(this.updateHistoryTimeout);
    }

    /**
     * Called by game code to open the chat panel for typing a message.
     * 
     * @param {*} mode 
     * @param {*} languageId 
     */
    startChat(mode, languageId) {
        this.setState({
            mode: mode,
            languageId: languageId,
        });

        this.chatInput.focus();
    }

    /**
     * Called when player switches keyboard language.
     * 
     * @param {*} languageId 
     */
    updateChatPlaceholder(languageId) {
        this.setState({
            languageId: languageId,
        });
    }

    /**
     * Called by game code to stop chat active mode.
     */
    stopChatActiveMode() {
        this.setState({
            mode: MODE_NONE,
        });
    }

    /**
     * Called by game code to print a chat message from another player.
     * 
     * @param {*} playerName 
     * @param {*} color 
     * @param {*} msg 
     */
    printChat(playerName, color, msg) {
        this.addChatLine({
            playerName: playerName,
            color: color,
            msg: msg,
        });
    }

    /**
     * Called by game code to print a chat notification.
     * 
     * @param {*} msg 
     */
    printChatNotification(msg) {
        this.addChatLine({
            notification: true,
            msg: msg,
        });
    }

    /**
     * Adds a chat line to the history.
     * 
     * @param {*} line 
     */
    addChatLine(line) {
        const now = new Date().getTime();
        const key = (line.playerName || '____global') + now;

        line.key = key;
        line.time = now;
        line.alwaysShow = true;

        this.setState((prevState/*, props*/) => ({
            history: [...prevState.history, ...[line]]
        }));

        clearTimeout(this.updateHistoryTimeout);
        this.updateHistoryTimeout = setTimeout(this.updateHistory.bind(this), ADDED_LINE_SHOW_TIME + 1);
    }

    /**
     * Update history for lines that were just added.
     * They are always shown for a short time.
     */
    updateHistory() {
        const now = new Date().getTime();

        const history = this.state.history;

        for (let i = history.length - 1; i >= 0; i-- ) {
            if (!history[i].alwaysShow) {
                break;
            }

            if (now - history[i].time < ADDED_LINE_SHOW_TIME) {
                continue;
            }

            history[i].alwaysShow = false;
        }

        // Trigger history update
        this.setState(() => ({
            history: history
        }));
    }

    /**
     * Listens to key events of input element.
     * @param {*} e 
     */
    handleKeyPress(e) {
        if (e.key === 'Enter') {
            this.sendChat();
        }
    }

    /**
     * Updates input.
     * 
     * @param {*} event 
     */
    handleChange(event) {
        this.setState({inputValue: event.target.value});
    }

    /**
     * Send chat.
     */
    sendChat() {
        window['interface'].clientCommand((this.mode === MODE_TEAM_SAY ? 'say_team ' : 'say ') + this.state.inputValue);
        this.setState({inputValue: ''});
        this.stopChatActiveMode();
    }

    /**
     * Controls visiblity of entire component
     */
    getContainerStyle() {
        return {
            display: this.state.visible ? 'block' : 'none',
        };
    }

    /**
     * Hides input field when player does not want to type.
     */
    getInputStyle() {
        return {
            display: this.state.mode > 0 ? 'block' : 'none',
        };
    }

    render() {
        const inputPlaceholder = `[${this.state.languageId}] ${this.state.mode === MODE_TEAM_SAY ? 'message (team)' : 'message'}`;

        // Filter down history to display
        const history = this.state.mode === MODE_NONE ? this.state.history.filter((line) => line.alwaysShow) : this.state.history;

        const chatHistoryClass = this.state.mode === MODE_NONE ? 'wars_chat_history wars_chat_history_hide_overflow' : 'wars_chat_history';

        return (
            <div id="wars_chat_container" style={this.getContainerStyle()}>
                <div className={chatHistoryClass} ref={(el) => { this.historyContainer = el; }}>
                    <TransitionGroup>
                        {history.map((line) => (
                            <CSSTransition key={line.key} classNames="wars_chat_history" timeout={{ enter: 50, exit: 100 }} >
                                <ChatLine line={line} />
                            </CSSTransition>)
                        )}
                    </TransitionGroup>
                </div>
                <span id="wars_chat_input_container" style={this.getInputStyle()}>
                    <span id="wars_chat_text" className="wars_chat_line wars_chat_white"><FormattedMessage id="Chat_SendLineText" />:</span>
                    <input 
                        ref={(input) => { this.chatInput = input; }} 
                        id="wars_chat_input" maxLength="100" placeholder={inputPlaceholder} 
                        value={this.state.inputValue} onChange={this.handleChange.bind(this)} 
                        onKeyPress={this.handleKeyPress.bind(this)}>
                    </input>
                    <div>
                        <span id="wars_send_button" className="wars_chat_line wars_chat_white wars_chat_button" onClick={this.sendChat.bind(this)}>
                            <FormattedMessage id="Chat_SendButton" />
                        </span>
                        <span id="wars_send_team_button" className="wars_chat_line wars_chat_white wars_chat_button" onClick={this.sendChat.bind(this)}>
                            <FormattedMessage id="Chat_SendTeamButton" />
                        </span>
                    </div> 
                </span>
            </div>
        );
    }
}
