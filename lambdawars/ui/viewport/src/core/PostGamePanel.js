import React from 'react';
import WarsComponent from '../WarsComponent';
import { FormattedMessage } from 'react-intl';
import './postgame.scss';

/**
 * Shows which players won and lost after game ends.
 */
export default class PostGamePanel extends WarsComponent {
    constructor(props) {
        super(props);

        this.state.winners = [];
        this.state.losers = [];
        this.state.type = '';
    }

    updatePanel(winners, losers, type) {
        this.setState({
            winners: winners,
            losers: losers,
            type: type,
        });
    }

    doDisconnect() {
        window.interface.clientCommand('disconnect\n');
    }

    doDisconnectAndShowScores() {
        window.interface.clientCommand('disconnect;mainmenu_gamelobby_show_scores\n');
    }

    render() {
        return (
            <div id="wars_postgame_container" style={this.getContainerStyle()} unselectable="on">
                <h1>{this.getTitle()}</h1>
                <p>{this.getWinnersText()}</p>
                <p>{this.getLosersText()}</p>
                <button className="btn" onClick={this.doDisconnect}><FormattedMessage id="PostGame_BackToLobbyButton" /></button>
                <button className="btn" onClick={this.doDisconnectAndShowScores}><FormattedMessage id="PostGame_ScoresButton" /></button>
            </div>
        );
    }

    getContainerStyle() {
        return {
            display: this.state.visible ? 'block' : 'none',
        };
    }

    getTitle() {
        if( this.state.type === 'won' ) {
            return <FormattedMessage id="PostGame_YouWon" />;
        } else if( this.state.type === 'lost' ) {
            return <FormattedMessage id="PostGame_YouLost" />;
        }
        
        return <FormattedMessage id="PostGame_YouOutsider" />;
    }

    getWinnersText() {
        return <FormattedMessage id="PostGame_Winners" 
            values={{
                players: this.state.winners
            }}
        />;
    }

    getLosersText() {
        return <FormattedMessage id="PostGame_Losers" 
            values={{
                players: this.state.losers
            }}
        />;
    }
}
