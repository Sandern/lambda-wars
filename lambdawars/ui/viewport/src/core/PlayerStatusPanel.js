import React from 'react';
import WarsComponent from '../WarsComponent';
import './playerstatuspanel.scss';
import { FormattedMessage } from 'react-intl';

const avatarUnknown = require('../../../menu_next/app/images/steam-avatar-unknown.jpg');

/**
 * Shows players ingame.
 */
export default class PlayerStatusPanel extends WarsComponent {
    constructor(props) {
        super(props);

        this.state.gamePlayers = [];
        this.state.players = [];
    }

    /**
     * Invoked by game logic to update the player list.
     * 
     * @param {*} game_players 
     * @param {*} players 
     */
    updatePlayers(gamePlayers, players)
    {
        this.setState({
            gamePlayers: gamePlayers,
            players: players,
        });
    }

    render() {
        return (
            <div id="wars_playerstatuspanel_container" style={this.getContainerStyle()} unselectable="on">
                <h2><FormattedMessage id="PlayerStatus_Players" />:</h2>
                <ul className="wars_playerstatuspanel_playerlist">
                    {this.state.gamePlayers.map(this.renderPlayerEntry)}
                </ul>
                <ul className="wars_playerstatuspanel_playerlist">
                    {this.state.players.map(this.renderPlayerEntry)}
                </ul>
            </div>
        );
    }

    getContainerStyle() {
        return {
            display: this.state.visible ? 'block' : 'none',
        };
    }

    renderPlayerEntry(playerEntry) {
        const playerSteamId = playerEntry['steamid'];
        const team = playerEntry['team'];

        return <li key={playerSteamId || playerEntry['name']}>
            <img height="24" width="24" src={ playerSteamId ? 'avatar://small/' + playerSteamId : avatarUnknown} />
            {playerEntry['name']}
            <span> - ping {playerEntry['ping']} {team > 0 ? `- team ${team}` : ''} - {playerEntry['state']}</span>
        </li>;
    }
}
