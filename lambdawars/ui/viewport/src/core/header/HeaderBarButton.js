import React from 'react';
import { Component } from 'react';

/**
 * Button for header bar.
 */
export default class HeaderBarButton extends Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {
    }

    componentWillUnmount() {
    }

    handleClick() {
        this.props.gameApi.onButtonPressed(this.props.buttonDef.name);
    }

    render() {
        const buttonDef = this.props.buttonDef;
        const cssclass = !buttonDef.floatRight ? 'wars_topbar_button' : 'wars_topbar_button_right';

        return (
            <button onClick={this.handleClick.bind(this)} type="button" className={cssclass}>
                {buttonDef.text}
            </button>
        );
    }
}

