import React from 'react';
import WarsComponent from '../../WarsComponent';
import HeaderBarButton from './HeaderBarButton';
import './headerbar.scss';

/**
 * Generic header bar component.
 * Holds buttons configured by gamerules.
 * Main purpose is tool buttons for Sandbox and Ingame editor.
 */
export default class HeaderBar extends WarsComponent {
    constructor(props) {
        super(props);

        this.state = {
            buttons: [],
        };
    }

    componentDidMount() {
        super.componentDidMount();
    }

    componentWillUnmount() {
        super.componentWillUnmount();
    }

    /**
     * Called by game code in insert a button.
     * 
     * @param {*} name 
     * @param {*} text 
     * @param {*} imagepath 
     * @param {*} order 
     * @param {*} floatright 
     */
    insertButton(name, text, order, floatRight) {
        this.setState((prevState/*, props*/) => ({
            buttons: [...prevState.buttons, ...[{ name: name, text: text, order: order, floatRight: floatRight }]]
        }));
    }

    render() {
        const buttons = [];
        for ( let i = 0; i < this.state.buttons.length; i++ ) {
            buttons.push(<HeaderBarButton key={this.state.buttons[i].name} buttonDef={this.state.buttons[i]} gameApi={this.gameApi} />)
        }

        return (
            <div id="wars_topbar_container">
                {buttons}
            </div>
        );
    }
}
