import React from 'react';
import WarsComponent from '../WarsComponent';
import { TransitionGroup, CSSTransition } from 'react-transition-group';
import Countdown from 'react-countdown';
import { FormattedMessage } from 'react-intl';
import './overrun.scss';
import { zeroPad } from '../utils/common';

// Currently theme is handled by this hud itself, but might be nice to have a better theming system if we have more cases like this.
import rebelsBgImage from '../../images/overrun/rebels/rebels_bg.png';
import combineBgImage from '../../images/overrun/combine/combine_bg.png';

/**
 * Controls and renders Overrun hud.
 */
export default class Overrun extends WarsComponent {
    constructor(props) {
        super(props);

        this.state = {
            waveNumber: 0,
            waveProgress: 0.0,
            waveCountdownTime: null,
            faction: 'rebels',
        };
    }


	/** 
     * Clears the wave count down timer if active
     */
	clearWaveCountdownTimer() {
        this.setState({
            waveCountdownTime: null,
        });
	}

	/** 
     * Shows and starts countdown for next wave
     */
	setNextWaveCountdown(waveNumber, nextWaveTime) {
        this.setState({
            waveNumber: waveNumber,
            waveCountdownTime: Date.now() + (parseInt(nextWaveTime) * 1000),
        });
    }
    
	/** 
     * Sets wave as in progress
     */
	setWaveActive(waveNumber) {
        this.setState({
            waveNumber: waveNumber,
        });
    }
    
	/** 
     * Updates progress of the current wave
     */
	updateWaveProgress(progress) {
        this.setState({
            waveProgress: progress,
        });
    }
	/** 
     * 
     * Updates faction for hud styling.
     */
	updateFaction(faction) {
        this.setState({
            faction: faction,
        });
    }

    /**
     * Renders Overrun hud.
     */
    render() {
        const { waveNumber, waveCountdownTime, waveProgress, faction } = this.state;
        const now = Date.now();

        const inCountDown = waveCountdownTime > now;
        const progressBarWidth = 150;

        const bgImage = /.*combine.*/.test(faction) ? combineBgImage : rebelsBgImage;

        return (
            <TransitionGroup>
                {this.state.visible ? (<CSSTransition classNames="wars_overrun_container" timeout={{ enter: 200, exit: 400 }}>
                    <div id="wars_overrun_container" style={this.getContainerStyle()}>
                        <img id="wars_overrun_bg" src={bgImage} alt="buttom" />
                        {!inCountDown ? (<div id="wars_overrun_waveinfo" className="wars_text_format_16">
                            <FormattedMessage
                                id='Overrun_WaveInProgress'
                                description='Text for wave in progress'
                                values={{
                                    waveNumber: waveNumber
                                }}
                            />
                        </div>) : (<div id="wars_overrun_waveinfo" className="wars_text_format_16">
                            <FormattedMessage
                                id='Overrun_WaveCountdown'
                                description='Text for wave countdown'
                                values={{
                                    countdownTime: <Countdown 
                                        date={waveCountdownTime}
                                        renderer={props => <span>{zeroPad(props.minutes, 2)}:{zeroPad(props.seconds, 2)}</span>}
                                    />
                                }}
                            />
                        </div>)}
                            
                        {inCountDown ? (<div id="wars_overrun_ready_button">
                            <button className="wars_button_right wars_text_format_20" onClick={this.onClickReady.bind(this)}><b><FormattedMessage id="Overrun_Ready" /></b></button>
                        </div>) : null}
                        
                        {inCountDown ? null : (<div id="wars_overrun_progress" className="wars_overrun_color_black">
                            <div id="wars_overrun_progress_bar" 
                                className="wars_overrun_color_orange"
                                style={{ width: (waveProgress * progressBarWidth) + 'px' }}></div>
                        </div>)}
                    </div>
                </CSSTransition>) : null}
            </TransitionGroup>
        );
    }
    /**
     * Returns the overall style.
     * Controls visiblity and scale.
     */
    getContainerStyle() {
        return {
            transform: `scale(${window.outerHeight / 900.0})`,
        };
    }

    /**
     * Player clicks ready button.
     */
    onClickReady() {
        this.gameApi.onReady();
    }
}