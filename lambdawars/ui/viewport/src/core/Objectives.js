import React from 'react';
import WarsComponent from '../WarsComponent';
import { TransitionGroup, CSSTransition } from 'react-transition-group';
import Countdown from 'react-countdown';
import { FormattedMessage } from 'react-intl';
import { zeroPad } from '../utils/common';
import './objectives.scss';

import frameHideImage from '../../images/objectives/frame_hide.png';
import frameHideOverImage from '../../images/objectives/frame_hide_over.png';
import objectiveIconProgress from '../../images/objectives/pg_objective_icon_progress.png';
import objectiveIconSuccess from '../../images/objectives/pg_objective_icon_success.png';
import objectiveIconFail from '../../images/objectives/pg_objective_icon_fail.png';

import objectiveBoxNormal from '../../images/objectives/objective_box_normal.png';
import objectiveBoxSuccess from '../../images/objectives/objective_box_success.png';
import objectiveBoxFail from '../../images/objectives/objective_box_fail.png';

/**
 * Controls and renders the objective list
 * Mostly intended for Single Player.
 */
export default class Objectives extends WarsComponent {
    constructor(props) {
        super(props);

        this.state = {
            objectiveInfo: [],
            objectiveListHidden: false,
            mouseOverHideButton: false,
        };
    }

    /**
     * Main function called from game code to update the objective list.
     */
    rebuildObjectiveList(objectiveInfo) {
        this.setState({
            objectiveInfo: objectiveInfo,
        });
    }    

    /**
     * Renders the objectives list.
     */
    render() {
        const counts = this.getObjectiveTypeCounts();

        const hideButtonStyle = {
            backgroundImage: `url(${this.state.mouseOverHideButton ? frameHideOverImage : frameHideImage})`,
        };

        return (
            <TransitionGroup>
                {this.state.visible ? (<CSSTransition classNames="wars_objectivelist_container" timeout={{ enter: 1000, exit: 1000 }}>
                    <div key="wars_objectivelist" style={this.getContainerStyle()}>
                        <a 
                            style={hideButtonStyle} 
                            className="wars_objective_button"
                            onMouseEnter={this.onMouseEnter.bind(this)}
                            onMouseLeave={this.onMouseLeave.bind(this)}
                            onClick={this.toggleHideObjectiveList.bind(this)}
                        ></a>
                        <div className="header">
                            <span className="title">
                                <b><FormattedMessage id="Objectives_Title" />:</b>
                            </span>

                            <span className="counts">
                                <span className="group">
                                    <img width="21px" src={objectiveIconProgress} align="left" alt="point" />
                                    <b id="wars_objectiv_normnum" style={{ color: 'white'}} >{counts.normal}</b>
                                </span>
                                <span className="group">
                                    <img width="21px" src={objectiveIconSuccess} align="left" alt="point" />
                                    <b id="wars_objectiv_sucnum" style={{ color: 'green' }}>{counts.success}</b>
                                </span>
                                <span className="group">
                                    <img width="21px" src={objectiveIconFail} align="left" alt="point" />
                                    <b id="wars_objectiv_failnum" style={{ color: 'red' }}>{counts.failed}</b>
                                </span>
                            </span>
                        </div>

                        <TransitionGroup
                            transitionName="wars_objectivelist"
                            transitionEnterTimeout={200}
                            transitionLeaveTimeout={400}
                        >
                            {!this.state.objectiveListHidden ?
                                <CSSTransition classNames="wars_objectivelist" timeout={{ enter: 200, exit: 400 }}>
                                    <ul key="wars_objectivelist">
                                        {this.renderObjectiveList()}
                                    </ul>
                                </CSSTransition>
                            : null}
                        </TransitionGroup>
                    </div>
                </CSSTransition>) : null}
            </TransitionGroup>
        );
    }

    /**
     * Sets mouse over state.
     */
    onMouseEnter() {
        this.setState({ mouseOverHideButton: true });
    }

    /**
     * Clears mouse over state.
     */
    onMouseLeave() {
        this.setState({ mouseOverHideButton: false });
    }

    /**
     * Toggle hiding objective list state.
     */
    toggleHideObjectiveList() {
        this.setState({ objectiveListHidden: !this.state.objectiveListHidden });
    }

    /**
     * Gets the counts per objective type.
     */
    getObjectiveTypeCounts() {
        return this.state.objectiveInfo.reduce((counts, info) => {
            switch(info.state) {
                case 0:
                case 3:
                case 4:
                    counts.normal += 1;
                    break;
                case 1:
                    counts.success += 1;
                    break;
                case 2:
                    counts.failed += 1;
                    break;
            }
            return counts;
        }, { normal: 0, success: 0, failed: 0 });
    }

    /**
     * Renders all objective list items.
     */
    renderObjectiveList() {
        return this.state.objectiveInfo.map((info, index) => {
            return this.renderObjectiveItem(info, index);
        });
    }

    /**
     * Renders a single objective list item.
     * @param info 
     * @param index
     */
    renderObjectiveItem(info, index) {
        switch(info.state) {
            // Objective to do
            case 0:
            {
                return (
                    <li key={index} className="wars_objectivelist_element normal">
                        <img src={objectiveBoxNormal} width="25" height="25" align="center" alt="point"/>
                        <font color="#FFFFFF" className="font-objective-items">
                        <b>{info.description}</b>
                        </font>
                    </li>
                );
            }
            // Success
            case 1:
            {
                return (
                    <li key={index} className="wars_objectivelist_element success">
                        <img src={objectiveBoxSuccess} width="25" height="25" align="center" alt="point"/>
                        <font color="#00FF00" className="font-objective-items">
                            <b>{info.description}</b>
                        </font>
                    </li>
                );  
            }
            // Failed
            case 2:
            {
                return (
                    <li key={index} className="wars_objectivelist_element failed">
                        <img src={objectiveBoxFail} width="25" height="25" align="center" alt="point" />
                        <font color="#FF0000" className="font-objective-items">
                            <b>{info.description}</b>
                        </font>
                    </li>
                );  
            }
            // Countdown
            case 3:
            {
                return (
                    <li key={index} className="wars_objectivelist_element countdown">
                        <img src={objectiveBoxNormal} width="25" height="25" align="center" alt="point"/>
                        <font color="#FFFFFF" className="font-objective-items">
                            <b>
                                <Countdown 
                                    date={Date.now() + (parseInt(info['timeleft']) * 1000)}
                                    renderer={props => <span>{zeroPad(props.minutes, 2)}:{zeroPad(props.seconds, 2)}</span>}
                                />
                                {info.description}
                            </b>
                        </font>
                    </li>
                );  
            }
            // Paused countdown
            case 4:
            {
                return (
                    <li key={index} className="wars_objectivelist_element coutndown_paused">
                        <img src={objectiveBoxNormal} width="25" height="25" align="center" alt="point"/>
                        <font color="#FFFFFF" className="font-objective-items">
                            <b><FormattedMessage id="Objectives_TimerPaused" /> { parseInt(info['timeleft']) } | {info.description}</b>
                        </font>
                    </li>
                );  
            }
        }
    }

    /**
     * Returns the overall style.
     * Controls visiblity and scale.
     */
    getContainerStyle() {
        return {
            transform: `scale(${window.outerHeight / 900.0})`,
        };
    }
}