import React from 'react';
import WarsComponent from '../WarsComponent';
import { HudThemeContext } from './HudThemeContext';
import { getScaleFactor, getAspectRatio } from './common/scaling';

import { 
    Abilities,
    Minimap,
    Units,
    ResourcesAndPopCap,
    Notifier,
    Groups,
} from './main';

import './combine.scss';

/**
 * Component defining the Combine faction HUD.
 * This top level component deals with positioning the sub elements of the hud,
 * including adjustments for different resolutions.
 */
export default class CombineHud extends WarsComponent {
    constructor(props) {
        super(props);

        this.state = {
        };
    }

    componentDidMount() {
        super.componentDidMount();
    }

    componentWillUnmount() {
        super.componentWillUnmount();
    }

    render() {
        const minimapWidth = 158;
        const abilitiesWidth = 159;
        let abilitiesOverlap = 9;
        let unitsRight, unitsWide, unitsTall;
        const aspect = getAspectRatio();
        const scaleFactor = getScaleFactor();

        // The units selection hud is adjusted based on the aspect ratio
        if (aspect > 1.7) {
            // Glorious wide screen master race
            unitsWide = 311;
            unitsTall = 110;
        } else if (aspect >= 1.5) {
            // Glorious wide screen master race with some extra height
            abilitiesOverlap += 9;
            unitsWide = 311;
            unitsTall = 110;
        } else if (aspect === 1.25) {
            // 5:4 suckers
            unitsWide = 200;
            unitsTall = 94;
        } else {
            // 4:3 suckers
            unitsWide = 235;
            unitsTall = 94;
        }
        unitsRight = abilitiesWidth - abilitiesOverlap;

        const theme = {
            color: '#1a99e6',

            style: {
                units: {
                    padding: `${25 * scaleFactor}px ${4 * scaleFactor}px 0 ${8 * scaleFactor}px`,
                },
            },
        };

        return (
            <HudThemeContext.Provider value={theme}>
                <div className="combine_hud">
                    <Groups width={unitsWide} height={30} right={abilitiesWidth - abilitiesOverlap} bottom={94} name='groups' />
                    <Minimap width={minimapWidth} height={151} left={0} bottom={0} name="minimap" />
                    <ResourcesAndPopCap width={107} height={110} left={minimapWidth-10} bottom={0} name="resources_popcap" />
                    <Units width={unitsWide} height={unitsTall} right={unitsRight} bottom={0} name="units" />
                    <Abilities width={abilitiesWidth} height={135} right={0} bottom={0} name="abilities" />
                    <Notifier width={350} height={100} left={25} top={100} name='notifier' />
                </div>
            </HudThemeContext.Provider>
        );
    }
}
