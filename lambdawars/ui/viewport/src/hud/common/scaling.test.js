import { getScaleFactor, getAspectRatio } from './scaling'

test('compute scale factor for height 1080', () => {
    global.outerHeight = 1080
    expect(getScaleFactor()).toBeCloseTo(2.25)
});

test('compute scale factor for height 1200', () => {
    global.outerHeight = 1200
    expect(getScaleFactor()).toBeCloseTo(2.5)
});

test('compute aspect ratio for height 1080 and width 1920', () => {
    global.outerHeight = 1080
    global.outerWidth = 1920
    expect(getAspectRatio()).toBeCloseTo(1.7777)
});

test('compute aspect ratio for height 1200 and width 1920', () => {
    global.outerHeight = 1200
    global.outerWidth = 1920
    expect(getAspectRatio()).toBeCloseTo(1.6)
});