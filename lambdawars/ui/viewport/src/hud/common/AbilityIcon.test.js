import React from 'react';
import AbilityIcon from './AbilityIcon';
import renderer from 'react-test-renderer';

test('Should render enabled ability icon', () => {
    const info = {
        image: 'url',
        canDoAbility: true,
    };
    const component = renderer.create(
        <AbilityIcon info={info}></AbilityIcon>,
    );
    let tree = component.toJSON();
    expect(tree).toMatchSnapshot();
});

test('Should render enabled ability icon with separate image mask', () => {
    const info = {
        image: 'url',
        imageColor: 'url_color',
        canDoAbility: true,
    };
    const component = renderer.create(
        <AbilityIcon info={info}></AbilityIcon>,
    );
    let tree = component.toJSON();
    expect(tree).toMatchSnapshot();
});

test('Should render enabled ability icon when canDoAbility is undefined', () => {
    const info = {
        image: 'url',
    };
    const component = renderer.create(
        <AbilityIcon info={info}></AbilityIcon>,
    );
    let tree = component.toJSON();
    expect(tree).toMatchSnapshot();
});

test('Should render disabled ability icon', () => {
    const info = {
        image: 'url',
        canDoAbility: false,
    };
    const component = renderer.create(
        <AbilityIcon info={info}></AbilityIcon>,
    );
    let tree = component.toJSON();
    expect(tree).toMatchSnapshot();
});
