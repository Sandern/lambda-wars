
/**
 * Scale factor used by hud components.
 * Based on logic from VGUI, so we can position and size the components the same way.
 * Not recommended for new UIs from scratch.
 */
export function getScaleFactor() {
    return (window.outerHeight / 480);
}

/**
 * Returns aspect ratio.
 */
export function getAspectRatio() {
    return window.outerWidth / window.outerHeight;
}
