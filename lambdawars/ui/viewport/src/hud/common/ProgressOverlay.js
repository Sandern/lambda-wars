import React from 'react';
import { Component } from 'react';
import PropTypes from 'prop-types';
import { Line } from 'rc-progress';

/**
 * Progress overlay for buttons.
 * Draws a bar from left to right.
 */
export default class ProgressOverlay extends Component {
    constructor(props) {
        super(props);

        this.state = {
            progress: this.calculateProgress(props),
        };
    }

    render() {
        const { onLeftClick, onDoubleClick, onRightClick } = this.props;

        const containerStyle = {
            width: 96,
            height: 96,
            left: 0,
            top: 0,
            position: 'absolute',
        };

        return <Line 
            percent={this.state.progress*100} 
            strokeWidth="4" 
            strokeColor="rgba(220, 170, 150, 0.7)"
            trailColor="rgba(60, 30, 30, 0)"
            style={containerStyle}

            onClick={onLeftClick}
            onDoubleClick={onDoubleClick}
            onContextMenu={onRightClick}
        />
    }

    componentDidMount() {
        this.checkUpdateTick();
    }

    componentWillUnmount() {
        clearInterval(this.tickInterval);
    }

    componentDidUpdate(/*prevProps, prevState*/) {
        this.checkUpdateTick();
    }

    checkUpdateTick() {
        const { progressEndTime } = this.props;

        if (progressEndTime && !this.tickInterval) {
            this.tickInterval = setInterval(() => this.tick(), 50);
        } else if (!progressEndTime && this.tickInterval) {
            clearInterval(this.tickInterval);
            this.tickInterval = null;
        }
    }

    tick() {
        const progress = this.calculateProgress(this.props);

        this.setState({
            progress: progress,
        });

        // Clear ourself from updating the progress bar at this point.
        if (progress === 0) {
            clearInterval(this.tickInterval);
            this.tickInterval = null;
        }
    }

    calculateProgress(props) {
        const { progressEndTime, totalTime } = props;

        const now = new Date().getTime() / 1000.0;
        return Math.max(0, (progressEndTime - now) / totalTime);
    }
}

ProgressOverlay.defaultProps = {
    progressEndTime: undefined,
    totalTime: undefined,
    onLeftClick: undefined,
    onDoubleClick: undefined,
    onRightClick: undefined,
};

ProgressOverlay.propTypes = {
    progressEndTime: PropTypes.number,
    totalTime: PropTypes.number,
    onLeftClick: PropTypes.func,
    onDoubleClick: PropTypes.func,
    onRightClick: PropTypes.func,
};