import React from 'react';
import PropTypes from 'prop-types';
import { HudThemeContext } from '../HudThemeContext';
import ProgressOverlay from './ProgressOverlay'

/**
 * Renders icon of an ability.
 * Takes care of handling color masks.
 */
export default function AbilityIcon(props) {
    const { info, onLeftClick, onDoubleClick, onRightClick } = props;

    const canDoAbility = info.canDoAbility === undefined || info.canDoAbility;
    const iconClasses = canDoAbility ? 'ability_icon' : 'ability_icon disabled';

    return (
        <HudThemeContext.Consumer>
            {theme =>
            <div style={{ display: 'inline-block', position: 'relative', width: '100%', height: '100%' }} className={iconClasses}>
                <a 
                    style={getIconStyle(info.image, info.imageColor, canDoAbility ? theme.color : '#c32929')}
                    onClick={onLeftClick}
                    onDoubleClick={onDoubleClick}
                    onContextMenu={onRightClick}
                />
                {info.rechargeEndTime ? <ProgressOverlay 
                    totalTime={info.rechargeTime}
                    progressEndTime={info.rechargeEndTime}
                    onLeftClick={onLeftClick}
                    onDoubleClick={onDoubleClick}
                    onRightClick={onRightClick}
                ></ProgressOverlay> : undefined}
            </div>}
        </HudThemeContext.Consumer>
    );
}

/**
 * Creates style for <a> element with ability image as background.
 */
function getIconStyle(image, imageColor, factionColor) {
    return {
        display: 'inline-block',
        WebkitMaskImage: `url(${imageColor || image})`,
        WebkitMaskSize: 'contain',
        WebkitMaskRepeat: 'no-repeat',
        WebkitMaskPosition: 'center',
        backgroundColor: factionColor,
        backgroundImage: imageColor && `url(${image})`,
        backgroundSize: 'contain',
        backgroundRepeat: 'no-repeat',
        backgroundPosition: 'center',
        width: '100%',
        height: '100%',
    };
}

AbilityIcon.defaultProps = {
    onLeftClick: undefined,
    onDoubleClick: undefined,
    onRightClick: undefined,
};

AbilityIcon.propTypes = {
    info: PropTypes.shape({
        image: PropTypes.string,
        imageColor: PropTypes.string,
        canDoAbility: PropTypes.bool,
    }),
    onLeftClick: PropTypes.func,
    onDoubleClick: PropTypes.func,
    onRightClick: PropTypes.func,
};
