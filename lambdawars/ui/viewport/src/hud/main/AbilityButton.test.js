import React from 'react';
import AbilityButton from './AbilityButton';
import renderer from 'react-test-renderer';

test('Should render ability button', () => {
    const info = {
        key: 'Unique Ability Key',
        name: 'AbilityName',
        displayName: 'Nice Ability Name',
        description: 'Description of this ability',
        costs: [],
        requirements: [],
        techRequirements: [],
        image: 'url',
        autocastSupported: false,
    };
    const gameApi = {};
    const component = renderer.create(
        <AbilityButton info={info} gameApi={gameApi}></AbilityButton>,
    );
    let tree = component.toJSON();
    expect(tree).toMatchSnapshot();
});