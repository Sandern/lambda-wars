import React from 'react';
import BaseScalableHudElement from './BaseScalableHudElement';
import AbilitiesButton from './AbilityButton';
import { getScaleFactor } from '../common/scaling';

/**
 * Component defining the abilities hud.
 */
export default class Abilities extends BaseScalableHudElement {
    constructor(props) {
        super(props);

        this.state = {
            slots_info: []
        };
    }

    /**
     * Updates slots in ability panel with all info, such as names, descriptions, requirements, costs, etc.
     * 
     * @param {object[]} slots_info List of ability objects corresponding to the slots in the UI.
     */
    updateSlots(slots_info) {
        this.setState({
            slots_info: slots_info,
        });
    }

    /**
     * Renders all ability slots.
     */
    render() {
        return (
            <div className="abilities" style={this.getContainerStyle()}>
                {this.state.slots_info.map(this.renderButton.bind(this))}
            </div>
        );
    }

    getContainerStyle() {
        const scaleFactor = getScaleFactor();

        return Object.assign({}, super.getContainerStyle(), {
            padding: 7 * scaleFactor,
            gridTemplateColumns: `repeat(auto-fill, ${30*scaleFactor}px)`,
            gridTemplateRows: `repeat(auto-fill, ${33*scaleFactor}px)`,
            gridGap: 4 * scaleFactor,
        });
    }

    /**
     * Render a single ability slot.
     * 
     * @param {object} info 
     */
    renderButton(info) {
        return (
            <AbilitiesButton key={info.key} info={info || {}} gameApi={this.gameApi} />
        );
    }
}
