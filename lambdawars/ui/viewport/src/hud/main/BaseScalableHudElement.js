import { Component } from 'react';
import { getScaleFactor } from '../common/scaling';

/**
 * Component that's absolutely positioned and scaled relative to 480 height.
 */
export default class BaseScalableHudElement extends Component {
    constructor(props) {
        super(props);

        // Create reference to interface to game instance of this component.
        // It's globally set based on the name.
        this.gameApi = window[`${props.name}_obj`];

        if (!this.gameApi) {
            console.error(`No game binding for component ${props.name}!`);
        }
    }

    componentDidMount() {
        // Sets a reference in the game code code this component, used for calling methods.
        this.gameApi.componentDidMount(this);
    }

    componentWillUnmount() {
        this.gameApi.componentWillUnmount(this);
    }

    componentDidCatch(/*error, info*/) {
        // Prevent component from crashing other viewport elements
    }

    getContainerStyle() {
        const scale = getScaleFactor();

        return {
            position: 'absolute',
            width: scale * this.props.width, 
            height: scale * this.props.height,
            left: this.props.left !== undefined ? this.props.left * scale : undefined,
            right: this.props.right !== undefined ? this.props.right * scale : undefined,
            bottom: this.props.bottom !== undefined ? this.props.bottom * scale : undefined,
            top: this.props.top !== undefined ? this.props.top * scale : undefined,
        }
    }
}
