import React from 'react';
import BaseScalableHudElement from './BaseScalableHudElement';
import { HudThemeContext } from '../HudThemeContext';

import { 
    UnitsSelection, 
    UnitSimple,
    UnitBuildSimple,
    UnitCombat,

    UnitBuildConstruction,
    UnitBuildGarrison,
    UnitBuildQueue,
    UnitBuildScrap,
    UnitBuildScrapPowerExtractor,
} from './units_components';

/**
 * Component defining the units hud.
 * Receives updates about the current unit selection from the game code.
 * Then depending on the type of panel renders another component.
 */
export default class Units extends BaseScalableHudElement {
    constructor(props) {
        super(props);

        this.state = {
            type: 'multi_selection',
            units: [],
        };
    }

    /**
     * Performs a full update based on the player unit selection.
     * @param { { type: string, units: object[] } } update containing type and units (player selection)
     */
    fullUpdate(update) {
        this.setState({
            type: update.type,
            units: update.units,
        });
    }

    /**
     * Updates attributes of a single unit.
     * The new attributes are merged in, overwriting old attributes.
     * @param {object} update 
     */
    unitUpdate(update) {
        this.setState((prevState) => {
            const units = prevState.units.slice();
            for (let i = 0; i < units.length; i++) {
                if (units[i].entIndex === update.entIndex) {
                    units[i] = Object.assign({}, units[i], update);
                    break;
                }
            }
            return {units: units};
          });
    }

    /**
     * Renders the unit panel.
     * Really just looks at the type and then renders another component.
     */
    render() {
        const { type, units } = this.state;
        const classNames = this.isSmallHud() < 1.5 ? 'units small' : 'units';

        return (
            <HudThemeContext.Consumer>
                {theme => 
                <div className={classNames} style={this.getContainerStyle(theme.style.units)}>
                    <span className="units_container">
                        { this.renderPanelType(type, units)}
                    </span>
                </div>}
            </HudThemeContext.Consumer>
        );
    }

    /**
     * Merges theme.
     * 
     * @param {*} style 
     */
    getContainerStyle(style) {
        return Object.assign({}, super.getContainerStyle(), style);
    }

    /**
     * Returns true when this is the non wide version of units hud.
     */
    isSmallHud() {
        return (window.outerWidth / window.outerHeight) < 1.5;
    }

    /**
     * Selects which panel should be rendered based on type.
     * @param {string} type 
     * @param {object[]} units 
     */
    renderPanelType(type, units) {
        if (type === 'multi_selection') {
            return this.renderMultiSelection(units);
        } else if (type === 'simple') {
            return this.renderSimple(units);
        } else if ( type === 'build_simple') {
            return this.renderBuildSimple(units);
        } else if (type === 'combat') {
            return this.renderCombat(units);
        } else if (type === 'build_construction') {
            return this.renderBuildConstruction(units);
        } else if (type === 'build_queue') {
            return this.renderBuildQueue(units);
        } else if (type === 'build_garrison') {
            return this.renderBuildGarrison(units);
        } else if (type === 'scrap') {
            return this.renderBuildScrap(units);
        } else if (type === 'scrap_power_extractor') {
            return this.renderBuildScrapPowerExtractor(units);
        } else {
            console.error(`Unknown units panel type "${type}"`);
        }

        return this.renderMultiSelection(units);
    }

    /**
     * Renders selection with multiple units in it.
     * 
     * @param {*} units 
     */
    renderMultiSelection(units) {
        return <UnitsSelection 
            units={units} 
            gameApi={this.gameApi}
            isSmallHud={this.isSmallHud}
        />;
    }

    /**
     * View for single unit selections that should work for all.
     * Displays names, health, attributes, etc.
     * 
     * @param {*} units 
     */
    renderSimple(units) {
        return <UnitSimple info={units[0]} gameApi={this.gameApi} />;
    }

    /**
     * View for single building selections that should work for all.
     * Displays names, health, attributes, etc.
     * 
     * @param {*} units 
     */
    renderBuildSimple(units) {
        return <UnitBuildSimple info={units[0]} gameApi={this.gameApi} />;
    }

    /**
     * View that shares base with Simple view.
     * Shows additional properties.
     * @param {*} units 
     */
    renderCombat(units) {
        return <UnitCombat info={units[0]} gameApi={this.gameApi} />;
    }

    /**
     * Displays info for building under construction
     * 
     * @param {*} units 
     */
    renderBuildConstruction(units) {
        return <UnitBuildConstruction info={units[0]} gameApi={this.gameApi} />;
    }

    /**
     * Displays info for building factories
     * 
     * @param {*} units 
     */
    renderBuildQueue(units) {
        return <UnitBuildQueue info={units[0]} gameApi={this.gameApi} />;
    }

    /**
     * Displays info for buildings that can be garrisoned
     * 
     * @param {*} units 
     */
    renderBuildGarrison(units) {
        return <UnitBuildGarrison info={units[0]} gameApi={this.gameApi} />;
    }

    /**
     * Displays info for scrap piles.
     * 
     * @param {*} units 
     */
    renderBuildScrap(units) {
        return <UnitBuildScrap info={units[0]} gameApi={this.gameApi} />;
    }

    /**
     * Displays info for scrap power extractors.
     * 
     * @param {*} units 
     */
    renderBuildScrapPowerExtractor(units) {
        return <UnitBuildScrapPowerExtractor info={units[0]} gameApi={this.gameApi} />;
    }
}
