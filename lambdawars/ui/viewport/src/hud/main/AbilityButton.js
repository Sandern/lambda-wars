import React from 'react';
import { Component } from 'react';
import { getScaleFactor } from '../common/scaling';
import AbilityIcon from '../common/AbilityIcon'

import ReactTooltip from 'react-tooltip';
import { FormattedMessage } from 'react-intl';

/**
 * Represents a clickable ability button
 */
export default class AbilitiesButton extends Component {
    constructor() {
        super();

        this.state = {
            rechargeProgress: 0,
        };
    }

    render() {
        const { info } = this.props;

        if (!info.name) {
            // Add empty element to grid to fill space
            return <a></a>;
        }

        return this.renderButton(info);
    }

    componentWillUnmount() {
        clearInterval(this.tickInterval);
    }

    componentDidUpdate(/*prevProps, prevState*/) {
        const { info } = this.props;

        if (info.rechargeEndTime && !this.tickInterval) {
            this.tickInterval = setInterval(() => this.tick(), 50);
        } else if (!info.rechargeEndTime && this.tickInterval) {
            clearInterval(this.tickInterval);
            this.tickInterval = null;
        }
    }

    /**
     * Updates recharge/build progress bar.
     */
    tick() {
        const { info } = this.props;

        const now = new Date().getTime() / 1000.0;
        const progress = Math.max(0, (info.rechargeEndTime - now) / info.rechargeTime);

        this.setState({
            rechargeProgress: progress,
        });

        // Clear ourself from updating the progress bar at this point.
        if (progress === 0) {
            clearInterval(this.tickInterval);
            this.tickInterval = null;
        }
    }

    /**
     * Renders ability button with tooltip.
     * @param {*} info 
     */
    renderButton(info) {
        const buttonClasses = info.canDoAbility ? 'ability_button' : 'ability_button disabled';

        return (
            <div style={this.getButtonStyle()}>
                <div className={buttonClasses} 
                    style={{ width: '100%', height: '100%' }} 
                    data-tip data-for={this.tooltipId(info)}
                >
                    <AbilityIcon
                        info={info}
                        onLeftClick={() => this.onClick()}
                        onRightClick={() => this.onContextMenu()}
                    ></AbilityIcon>
                    
                </div>
                {this.renderToolTip(info)}
            </div>
        );
    }

    getButtonStyle() {
        return {
            marginLeft: this.getButtonInset(),
            marginTop: this.getButtonInset(),
            width: this.getButtonSize(),
            height: this.getButtonSize(),
        };
    }

    renderToolTip(info) {
        return (<ReactTooltip id={this.tooltipId(info)} effect="solid">
            <h4>{info.displayName}<span >{ this.renderHotkey(info) }</span></h4>
            <p>{info.description}</p>
            { this.renderAbilityCosts(info) }
            { this.renderRechargeTime(info) }
            { this.renderBuildTime(info) }
            { this.renderPopulation(info) }
            { this.renderEnergy(info) }
            { this.renderRequirements(info) }
            { this.renderTechRequirements(info) }
            <p style={{ color: 'yellow' }}>{info.autocastSupported ? <FormattedMessage id="PlayerHud_AutoCastSupported" />: ''}</p>
        </ReactTooltip>);
    }

    tooltipId(info) {
        return String(info.key);
    }

    /**
     * Renders the hotkey.
     * 
     * @param {object} info
     */
    renderHotkey(info) {
        return info.hotkey && <span style={{ marginLeft: '6px', color: 'yellow' }}>[{info.hotkey.toUpperCase()}]</span>
    }

    /**
     * Renders a list of costs.
     * @param {object} info 
     */
    renderAbilityCosts(info) {
        return info.costs.map((cost) => {
            const style = {
                color: cost.hasEnough ? 'white' : 'red',
            };

            return <p key={cost.name} style={style}>{`${cost.amount} ${cost.displayName}`}</p>;
        });
    }

    /**
     * Renders recharge time.
     * @param {object} info 
     */
    renderRechargeTime(info) {
        return info.rechargeTime && <p><FormattedMessage id="PlayerHud_RechargeTime" values={{ rechargeTime: info.rechargeTime }}/></p>;
    }

    /**
     * Renders build time.
     * @param {object} info 
     */
    renderBuildTime(info) {
        return info.buildTime && <p><FormattedMessage id="PlayerHud_BuildTime" values={{ buildTime: info.buildTime }}/></p>;
    }


    /**
     * Renders required Population
     * @param {object} info 
     */
    renderPopulation(info) {
        return info.population && <p><FormattedMessage id="PlayerHud_Population" values={{ population: info.population }}/></p>;
    }

    /**
     * Renders required Energy
     */
    renderEnergy(info) {
        return info.energy && <p><FormattedMessage id="PlayerHud_EnergyRequired" values={{ energy: info.energy }}/></p>;
    }

    /**
     * Renders list of tech requirements
     * 
     * @param {*} info 
     */
    renderTechRequirements(info) {
        return info.techRequirements.map((techRequirement) => {
            const style = {
                color: techRequirement.enabled ? 'white' : 'red',
            };

            return (
                <p key={techRequirement.name} style={style}><FormattedMessage id="PlayerHud_TechRequirement" 
                    values={{ requirement: techRequirement.displayName }}
                /></p>
            );
        });
    }

    /**
     * Renders general list of requirements.
     */
    renderRequirements(info) {
        return info.requirements.map((requirement) => {
            const style = {
                color: 'red',
            };

            return (
                <p key={requirement} style={style}>{requirement}</p>
            );
        });
    }

    getButtonSize() {
        return `calc(100% - ${this.getButtonInset() * 2}px)`
    }

    getButtonInset() {
        return 1.1 * getScaleFactor();
    }

    /**
     * Handles left click, which should do the ability.
     */
    onClick() {
        this.props.gameApi.doAbility(this.props.info.key);
    }

    /**
     * Handles right click, which is usually toggling autocast.
     */
    onContextMenu() {
        this.props.gameApi.doAbilityAlt(this.props.info.key);
    }
}
