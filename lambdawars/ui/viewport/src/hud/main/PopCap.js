import React from 'react';
import BaseScalableHudElement from './BaseScalableHudElement';
import { HudThemeContext } from '../HudThemeContext';
import { getScaleFactor } from '../common/scaling';

/**
 * Component defining the population and caps hud.
 * Currently used for Rebels hud.
 * Combine uses combined components for resources and population.
 */
export default class PopCap extends BaseScalableHudElement {
    constructor(props) {
        super(props);

        this.state = {
            pop: 0,
            popCap: 0,
        };
    }

    update(state) {
        this.setState(state);
    }

    render() {
        const { pop, popCap } = this.state;

        return (
            <HudThemeContext.Consumer>
                {theme => 
                <div className="popcap" style={this.getContainerStyle()}>
                    <span className="pop_icon" style={this.getIconMaskedStyle(theme.color)} />
                    <p style={this.getTextStyle()}>{`${pop} / ${popCap}`}</p>
                </div>}
            </HudThemeContext.Consumer>
        );
    }

    getContainerStyle() {
        return Object.assign({}, super.getContainerStyle(), {
            display: 'flex',
            alignItems: 'center',
        });
    }

    getTextStyle() {
        return {
            margin: 0,
            fontSize: 10 * getScaleFactor(), 
            fontWeight: 'bold', 
            color: 'white',
            verticalAlign: 'center',
        };
    }

    getIconMaskedStyle(factionColor) {
        const scaleFactor = getScaleFactor();

        return {
            display: 'inline-block',
            width: 25 * scaleFactor,
            height: 25 * scaleFactor,
            marginLeft: 10 * scaleFactor,
            backgroundColor: factionColor,
        }
    }
}
