import React from 'react';
import { Component } from 'react';
import { FormattedMessage } from 'react-intl';
import ReactTooltip from 'react-tooltip';

/**
 * Component defining the selection view of a single simple unit. Basically just display name and description.
 */
export default class UnitSimple extends Component {
    render() {
        const { info } = this.props;

        return (
            <div style={{ color: 'white', fontWeight: 'bold' }}>
                <h4>{info.displayName}</h4>
                {this.renderHealth(info)}
                {this.renderEnergy(info)}
                {this.renderAttributes(info)}
            </div>
        );
    }

    renderHealth(info) {
        if (!info.maxHealth) {
            return null;
        }
        return (
            <p style={{ color: 'green' }}><FormattedMessage id="PlayerHud_Health" 
                values={{ health: info.health, maxHealth: info.maxHealth }}
            /></p>
        )
    }

    renderEnergy(info) {
        if (!info.maxEnergy) {
            return null;
        }
        return (
            <p style={{ color: 'blue' }}><FormattedMessage id="PlayerHud_Energy" 
                values={{ energy: info.energy, maxEnergy: info.maxEnergy }}
            /></p>
        )
    }

    /**
     * Render list of attributes.
     * 
     * @param {object} info ability information object
     */
    renderAttributes(info) {
        return <div className="attributeList">{info.attributes.map((attr) => this.renderAttribute(attr))}</div>;
    }

    /**
     * Render single attribute.
     */
    renderAttribute(attribute) {
        const tooltipId = attribute.description ? `unitsimple_${attribute.name}` : undefined;
        return <span key={attribute.name}>
            <span data-tip data-for={tooltipId}>{attribute.name}</span>
            {tooltipId ? <ReactTooltip id={tooltipId} effect="solid"><p>{attribute.description}</p></ReactTooltip> : undefined}
        </span>;
    }
}
