import React from 'react';
import UnitSimple from './UnitSimple';
import UnitsButton from './UnitsButton';

/**
 * Displays info for buildings that can be garrisoned.
 */
export default class UnitBuildGarrison extends UnitSimple {
    render() {
        const { info } = this.props;



        return (
            <div style={{ color: 'white', fontWeight: 'bold' }}>
                <h4>{info.displayName}</h4>
                {this.renderHealth(info)}
                {this.renderEnergy(info)}
                {this.renderAttributes(info)}
                {info.units.map(this.renderGarrisonedUnit.bind(this))}
            </div>
        );
    }

    renderGarrisonedUnit(info, idx) {
        const ungarrisonUnit = this.ungarrisionUnit.bind(this);

        return (
            <UnitsButton key={info.entIndex} 
                info={info || {}} 
                onLeftClick={ungarrisonUnit}
                onRightClick={ungarrisonUnit}
                idx={idx}></UnitsButton>
        );
    }

    /**
     * Handler for clicking the unit button.
     * Ejects the unit from the building.
     * 
     * @param {*} evt 
     * @param {*} info 
     */
    ungarrisionUnit(evt, idx, info) {
        this.props.gameApi.ungarrisionUnit(info['entIndex']);
    }
}
