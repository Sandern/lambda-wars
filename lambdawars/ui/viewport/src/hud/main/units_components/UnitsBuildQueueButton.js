import React from 'react';
import { Component } from 'react';
import ProgressOverlay from '../../common/ProgressOverlay';
import ReactTooltip from 'react-tooltip';

/**
 * Represents a clickable build queue button
 */
export default class UnitsBuildQueueButton extends Component {
    render() {
        const { info, idx, onLeftClick } = this.props;

        const overlayAmountStyle = {
            position: 'absolute',
            left: 0,
            top: 0,
            fontSize: '16px',
            color: 'white',
            pointerEvents: 'none',
            zIndex: 10,
        };

        let buildProgressOverlay;
        if (info.buildTime) {
            buildProgressOverlay = (<ProgressOverlay 
                progressEndTime={info.buildEndTime}
                totalTime={info.buildTime}
                onLeftClick={(e) => onLeftClick(e, idx, info)}
            />);
        }

        const tooltipId = `unitqueue_${idx}`;
        return (<div style={{ display: 'inline-block' }}>
            <span data-tip data-for={tooltipId} style={{ display: 'inline-block', position: 'relative' }}>
                <span style={overlayAmountStyle}>{info.amount}</span>
                <a 
                    className='rebels_units_button' 
                    style={this.getButtonStyle(info)}
                    onClick={(e) => onLeftClick(e, idx, info)}
                />
                { buildProgressOverlay }
            </span>
            <ReactTooltip id={tooltipId} effect="solid">
                <h4>{info.displayName}</h4>
                <p>{info.description}</p>
            </ReactTooltip>
        </div>);
    }

    /**
     * Creates button style for <a> element with ability image as background.
     * @param {*} info 
     */
    getButtonStyle(info) {
        return {
            display: 'inline-block',
            backgroundImage: `url(${info.image})`,
            backgroundSize: 'cover',
            width: 96,
            height: 96,
        };
    }
}
