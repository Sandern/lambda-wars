import React from 'react';
import UnitSimple from './UnitSimple';
import { FormattedMessage } from 'react-intl';

/**
 * Displays info for scrap power extractor
 */
export default class UnitBuildScrapPowerExtractor extends UnitSimple {
    render() {
        const { info } = this.props;

        return (
            <div style={{ color: 'white', fontWeight: 'bold' }}>
                <h4>{info.displayName}</h4>
                {this.renderHealth(info)}
                {this.renderEnergy(info)}
                {this.renderAttributes(info)}
                <p><FormattedMessage id="PlayerHud_PowerLeft"
                    values={{ power: info.resourceGeneration.left !== -1 ? info.resourceGeneration.left : 'infinite' }}
                /></p>
            </div>
        );
    }
}
