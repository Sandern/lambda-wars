import React from 'react';
import UnitSimple from './UnitSimple';
import { FormattedMessage } from 'react-intl';

/**
 * Component defining the selection view of a single combat unit.
 */
export default class UnitCombat extends UnitSimple {
    render() {
        const { info } = this.props;

        return (
            <div style={{ color: 'white', fontWeight: 'bold' }}>
                <h4>{info.displayName}</h4>
                {this.renderHealth(info)}
                {this.renderEnergy(info)}
                {this.renderAttributes(info)}
                {this.renderAttacks(info)}
                {this.renderKills(info)}
            </div>
        );
    }

    /**
     * Renders attacks information of this unit.
     * Already preformatted message by game code.
     * 
     * @param {object} info 
     */
    renderAttacks(info) {
        if (!info.attacks) {
            return null;
        }
        return (
            <p>{info.attacks}</p>
        )
    }

    /**
     * Renders number of kills.
     * 
     * @param {object} info
     */
    renderKills(info) {
        if (info.kills === undefined) {
            return null;
        }
        return (
            <p style={{ color: 'white' }}><FormattedMessage id="PlayerHud_Kills" 
                values={{ kills: info.kills }}
            /></p>
        )
    }
}
