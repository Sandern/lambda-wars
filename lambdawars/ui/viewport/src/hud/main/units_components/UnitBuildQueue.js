import React from 'react';
import UnitSimple from './UnitSimple';
import UnitsBuildQueueButton from './UnitsBuildQueueButton';

/**
 * Displays info for building factories, displaying the queue.
 */
export default class UnitBuildQueue extends UnitSimple {
    render() {
        const { info } = this.props;

        const queue = [];

        info.queue.forEach((info, key) => {
            if (!info) {
                return true;
            } else {
                queue.push(
                    <UnitsBuildQueueButton 
                        key={key}
                        idx={key}
                        info={info}
                        onLeftClick={this.onLeftClick.bind(this)}
                    />
                );
            }
        });

        return (
            <div style={{ color: 'white', fontWeight: 'bold' }}>
                <h4>{info.displayName}</h4>
                {this.renderHealth(info)}
                {this.renderEnergy(info)}
                {this.renderAttributes(info)}
                {queue}
            </div>
        );
    }

    onLeftClick(evt, idx/*, info*/) {
        this.props.gameApi.removeUnitFromQueue(idx);
    }
}
