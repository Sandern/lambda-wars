import React from 'react';
import { Component } from 'react';
import { getScaleFactor } from '../../common/scaling';

import UnitsButton from './UnitsButton';

/**
 * Component defining the selection view of the units hud part.
 */
export default class UnitsSelection extends Component {
    render() {
        const { units } = this.props;

        return (
            <div style={this.getContainerStyle()} className="unit_selection">
                {units.map(this.renderUnit.bind(this))}
            </div>
        );
    }

    getContainerStyle() {
        const { units, isSmallHud } = this.props;
        const scaleFactor = getScaleFactor();

        // Just some guess work, but will look good enough.
        const unitsDivideFactor = isSmallHud ? 7 : 20;
        const additionalSpaceFactor = Math.min(1, 1 / Math.sqrt(Math.ceil(units.length / unitsDivideFactor)));

        return {
            width: '100%',
            height: '100%',
            gridTemplateColumns: `repeat(auto-fill, ${30*scaleFactor*additionalSpaceFactor}px)`,
            gridTemplateRows: `repeat(auto-fill, ${33*scaleFactor*additionalSpaceFactor}px)`,
        };
    }

    renderUnit(info, idx) {
        return (
            <UnitsButton key={info.entIndex} 
                info={info || {}}
                onLeftClick={this.onLeftClick.bind(this)}
                onRightClick={this.onRightClick.bind(this)}
                onDoubleClick={this.onDoubleClick.bind(this)}
                idx={idx}
            ></UnitsButton>
        )
    }

    /**
     * Handler for left click.
     * Changes selected unit type for ability panel.
     * With control pressed it selects all units of that type, discarding other units from the selection.
     * 
     * @param {object} evt 
     * @param {number} idx 
     * @param {object} info 
     */
    onLeftClick(evt, idx, info) {
        if (evt.nativeEvent.ctrlKey) {
            this.props.gameApi.selectUnitsOfTypeInSelection(info.name);
        } else {
            this.props.gameApi.selectUnitType(info.name);
        }
    }

    /**
     * Handler for right click.
     * Removes unit from selection.
     * With control pressed it removes all units of that type from the selection.
     * 
     * @param {object} evt 
     * @param {number} idx 
     * @param {object} info 
     */
    onRightClick(evt, idx, info) {
        if (evt.nativeEvent.ctrlKey) {
            this.props.gameApi.removeUnitsOfTypeFromSelection(info.name);
        } else {
            this.props.gameApi.removeUnitAtIndex(idx);
        }
    }

    /**
     * Handler for double click.
     * Discards all units from selection except one double click.
     * 
     * @param {object} evt 
     * @param {number} idx 
     * @param {object} info 
     */
    onDoubleClick(evt, idx/*, info*/) {
        this.props.gameApi.onlySelectUnitAtIndex(idx);
    }
}
