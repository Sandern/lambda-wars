import React from 'react';
import UnitSimple from './UnitSimple';
import { FormattedMessage } from 'react-intl';

/**
 * Displays info for scrap piles.
 */
export default class UnitBuildSrap extends UnitSimple {
    render() {
        const { info } = this.props;

        return (
            <div style={{ color: 'white', fontWeight: 'bold' }}>
                <h4>{info.displayName}</h4>
                {this.renderHealth(info)}
                {this.renderEnergy(info)}
                {this.renderAttributes(info)}
                <p><FormattedMessage id="PlayerHud_ScrapLeft"
                    values={{ scrap: info.scrap !== -1 ? info.scrap : 'infinite' }}
                /></p>
            </div>
        );
    }
}
