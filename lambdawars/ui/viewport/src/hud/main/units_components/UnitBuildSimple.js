import React from 'react';
import UnitSimple from './UnitSimple';
import { FormattedMessage } from 'react-intl';

/**
 * Component defining the selection view of a single building unit.
 */
export default class UnitBuildSimple extends UnitSimple {
    render() {
        const { info } = this.props;

        return (
            <div style={{ color: 'white', fontWeight: 'bold' }}>
                <h4>{info.displayName}</h4>
                {this.renderHealth(info)}
                {this.renderEnergy(info)}
                {this.renderAttributes(info)}
                {this.renderResourceGeneration(info)}
            </div>
        );
    }

    renderResourceGeneration(info) {
        if (!info.resourceGeneration) {
            // Render nothing
            return undefined;
        }

        return (
            <p><FormattedMessage
                id="PlayerHud_ResourceLeft"
                values={{ resourceLeft: info.resourceGeneration.left }}
            /></p>
        );
    }
}
