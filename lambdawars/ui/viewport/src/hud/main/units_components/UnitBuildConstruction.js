import React from 'react';
import UnitSimple from './UnitSimple';
import { FormattedMessage } from 'react-intl';

/**
 * Displays info for building under construction..
 */
export default class UnitBuildConstruction extends UnitSimple {
    render() {
        const { info } = this.props;

        return (
            <div style={{ color: 'white', fontWeight: 'bold' }}>
                <h4>{info.displayName}</h4>
                {this.renderHealth(info)}
                {this.renderEnergy(info)}
                {this.renderAttributes(info)}
                <p><FormattedMessage id="PlayerHud_ConstructionProgress"
                    values={{ progress: Math.floor(info.constructionProgress * 100) }}
                /></p>
            </div>
        );
    }
}
