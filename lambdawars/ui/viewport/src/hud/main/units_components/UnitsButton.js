import React from 'react';
import { Component } from 'react';
import AbilityIcon from '../../common/AbilityIcon';

import ReactTooltip from 'react-tooltip';
import { FormattedMessage } from 'react-intl';


/**
 * Represents a clickable unit button
 */
export default class UnitsButton extends Component {
    render() {
        const { info, idx, onLeftClick, onRightClick, onDoubleClick } = this.props;

        const healthColor = Math.round(info.healthFraction * 255);
        const healthStyle = {
            width: `calc(${(100 * info.healthFraction)}% - 4px)`,
            height: '10%',
            position: 'absolute',
            bottom: 0,
            left: '2px',
            backgroundColor: `rgba(${255 - healthColor}, ${healthColor}, 0, 0.9)`,
        };

        return (<div style={this.getButtonStyle()}>
            <div data-tip data-for={this.tooltipId(info)} style={{ width: '100%', height: '100%' }} className='unit_button'>
                <div style={healthStyle} />
                <div className="units_button" style={this.getButtonIconStyle()}>
                    <AbilityIcon 
                        info={info}
                        onLeftClick={(e) => onLeftClick(e, idx, info)}
                        onDoubleClick={(e) => onDoubleClick(e, idx, info)}
                        onRightClick={(e) => onRightClick(e, idx, info)}
                    ></AbilityIcon>
                </div>
            </div>
            {this.renderToolTip(info)}
        </div>);
    }

    renderToolTip(info) {
        return <ReactTooltip id={this.tooltipId(info)} effect='solid'>
            <h4>{info.displayName}</h4>
            <p>{info.description}</p>
            { this.renderToolTipHealth(info) }
            { this.renderToolTipEnergy(info) }
        </ReactTooltip>;   
    }

    tooltipId(info) {
        return `unitbutton-${info.entIndex}`;
    }

    /**
     * Render tooltip line for health.
     * @param {object} info 
     */
    renderToolTipHealth(info) {
        return (
            info.maxHealth && <p style={{ color: 'green' }}><FormattedMessage id="PlayerHud_Health" 
                values={{ health: info.health, maxHealth: info.maxHealth }}
            /></p>
        )
    }

    /**
     * Render tooltip line for energy.
     * @param {object} info 
     */
    renderToolTipEnergy(info) {
        return (
            info.maxEnergy && <p style={{ color: 'blue' }}><FormattedMessage id="PlayerHud_Energy" 
                values={{ energy: info.energy, maxEnergy: info.maxEnergy }}
            /></p>
        )
    }

    /**
     * Creates button style for <a> element with ability image as background.
     */
    getButtonStyle() {
        return {
            position: 'relative',
            display: 'inline-block',
        };
    }

    /**
     * Creates button style for <a> element with ability image as background.
     * @param {object} info 
     */
    getButtonIconStyle() {
        return {
            width: '100%',
            // Placed in frame of button, health bar shows at bottom.
            height: '90%',
        };
    }
}
