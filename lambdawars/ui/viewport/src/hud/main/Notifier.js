import React from 'react';
import BaseScalableHudElement from './BaseScalableHudElement';
import AbilityIcon from '../common/AbilityIcon'
import { getScaleFactor } from '../common/scaling';

// Max number of notification
const MAX_NOTIFICATIONS = 5;
// How long it takes before a notification starts fading away
const NOTIFICATION_FADE_TIME = 7000;

/**
 * Component defining the notifier hud.
 * Notifications are events like "Unit completed" or "Control point captured".
 */
export default class Notifier extends BaseScalableHudElement {
    constructor(props) {
        super(props);

        this.state = {
            notifications: [],
        };
    }

    componentWillUnmount() {
        super.componentWillUnmount();

        this.clearUpdateHistoryTimer();
    }

    /**
     * Inserts a new notification with the following properties:
     * 
     * {
     *     id: '<unique id>',
     *     message: '<message text to display',
     *     color: '<color of text to display',
     *     icon: '<icon url to display',
     * }
     * 
     * This method is called from the game code.
     */
    insertNotification(notification) {
        notification.time = new Date().getTime();

        this.setState({
            notifications: this.state.notifications.concat([notification]),
        });
    }

    /**
     * Update history and set timer for updating when state changes.
     */
    componentDidUpdate(/*prevProps, prevState*/) {
        this.updateHistory();
        this.setUpdateHistoryTimer();
    }

    /**
     * Removes notifications when there are too many or when they should fade.
     * Only modifies state if there are changes.
     */
    updateHistory() {
        const now = new Date().getTime();
        const newNotifications = this.state.notifications.slice();

        // Remove when:
        // - oldest notification should fade out
        // - there are more notifications than we would ever show
        while ((newNotifications.length > 0 && (now - newNotifications[0].time) > NOTIFICATION_FADE_TIME) || newNotifications.length > MAX_NOTIFICATIONS ) {
            newNotifications.shift();
        }

        if (this.state.notifications.length !== newNotifications.length) {
            this.setState({
                notifications: newNotifications,
            });
            return true;
        }
        return false;
    }

    /**
     * Clears update notification history timer.
     */
    clearUpdateHistoryTimer() {
        if (this.updateHistoryTimeout) {
            clearTimeout(this.updateHistoryTimeout);
            this.updateHistoryTimeout = null;
        }
    }

    /**
     * Updates timer for updating notification history again.
     * This is always based on the oldest notification, as that's 
     * the notification that should fade soonest.
     */
    setUpdateHistoryTimer() {
        this.clearUpdateHistoryTimer();

        const notifications = this.state.notifications;
        if (notifications.length > 0) {
            const now = new Date().getTime();
            this.updateHistoryTimeout = setTimeout(
                () => {
                    if (!this.updateHistory()) {
                        this.setUpdateHistoryTimer();
                    }
                },
                Math.max(0, now - (notifications[0].time + NOTIFICATION_FADE_TIME))
            );
        }
    }

    /**
     * Renders all notifications.
     */
    render() {
        return (
            <div className="notifier" style={this.getContainerStyle()}>
                {this.state.notifications.map(this.renderNotification.bind(this))}
            </div>
        );
    }

    getContainerStyle() {
        return Object.assign(
            super.getContainerStyle(),
            { pointerEvents: 'none' }
        );
    }

    /**
     * Renders a single notification
     */
    renderNotification(n) {
        const scale = getScaleFactor();
        const iconSize = 20 * scale;

        return (<div key={n.id} className="notifier-line" style={{ color: n.color, height: iconSize, pointerEvents: 'auto' }}>
            <div style={{ display: 'inline-block', width: iconSize, height: iconSize }}>
                <AbilityIcon 
                    info={{ image: n.icon, canDoAbility: true }} 
                    onLeftClick={() => this.jumpToNotification(n)}
                ></AbilityIcon>
            </div>
            <div style={{ display: 'inline-block', height: '100%', verticalAlign: 'middle' }}>{n.message}</div>
        </div>);
    }

    /**
     * Calls game code to jump player to this notification
     */
    jumpToNotification(notification) {
        this.gameApi.jumpToNotification(notification.id);
    }
}
