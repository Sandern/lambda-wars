import React from 'react';
import BaseScalableHudElement from './BaseScalableHudElement';
import ResourceLine from './ResourceLine';
import { getScaleFactor } from '../common/scaling';

/**
 * Component defining the resources hud.
 * Currently used for Rebels hud.
 * Combine hud uses combined component.
 */
export default class Resources extends BaseScalableHudElement {
    constructor(props) {
        super(props);

        this.state = {
            resources: [],
        };
    }

    update(state) {
        this.setState(state);
    }

    render() {
        const { resources } = this.state;

        return (
            <div className="resources" style={this.getContainerStyle()}>
                {resources.map(this.renderResource.bind(this))}
            </div>
        );
    }

    /**
     * Render resource line.
     * 
     * @param {object} resource
     */
    renderResource(resource) {
        const scaleFactor = getScaleFactor();
        return <div key={resource.name} style={{ marginLeft: 10 * scaleFactor, marginTop: 9 * scaleFactor, fontSize: 10 * scaleFactor, display: 'block' }}>
            <ResourceLine resource={resource} scaleFactor={scaleFactor} />
        </div>;
    }
}
