import React from 'react';
import ReactTooltip from 'react-tooltip';
import NumberEasing from 'che-react-number-easing';
import { HudThemeContext } from '../HudThemeContext';

/**
 * Component defining a single resource line
 */
export default function ResourceLine(props) {
    const { scaleFactor, resource } = props;

    const amount = <NumberEasing
        ease="quintInOut"
        precision={0}
        speed={500}
        trail={true}
        useLocaleString={false}
        value={resource.amount}
    />;

    const tooltipId = `resource_line_${resource.name}`;

    return (
        <span>
            <span data-tip data-for={tooltipId} className="resource_line" style={{ fontWeight: 'bold', color: 'white' }}>
                <HudThemeContext.Consumer>{
                    theme => <span style={getIconMaskedStyle(scaleFactor, resource, theme.color)} />
                }</HudThemeContext.Consumer>
                { amount } { resource.cap !== undefined ? ` / ${resource.cap}` : '' }
            </span>
            <ReactTooltip id={tooltipId} effect='solid'>{resource.displayName}</ReactTooltip>
        </span>
    );
}

function getIconMaskedStyle(scaleFactor, resource, factionColor) {
    return {
        display: 'inline-block',
        width: 8 * scaleFactor,
        height: 8 * scaleFactor,
        marginRight: 3 * scaleFactor,
        WebkitMaskImage: `url(${resource.icon})`,
        WebkitMaskSize: 'contain',
        WebkitMaskRepeat: 'no-repeat',
        WebkitMaskPosition: 'center',
        backgroundColor: factionColor,
    }
}
