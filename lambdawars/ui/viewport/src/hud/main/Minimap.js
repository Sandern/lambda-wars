import React from 'react';
import BaseScalableHudElement from './BaseScalableHudElement';

/**
 * Component defining the minimap.
 */
export default class Minimap extends BaseScalableHudElement {
    constructor(props) {
        super(props);

        this.state = {
        };
    }

    render() {
        return (
            <div className="minimap" style={this.getContainerStyle()}>
            </div>
        );
    }
}
