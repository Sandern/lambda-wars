import React from 'react';
import BaseScalableHudElement from './BaseScalableHudElement';
import ResourceLine from './ResourceLine';
import { getScaleFactor } from '../common/scaling';

/**
 * Component defining the resources, population and cap hud in one component.
 * Currently used for Combine hud.
 * Rebels uses split components for resources and population.
 */
export default class ResourcesAndPopCap extends BaseScalableHudElement {
    constructor(props) {
        super(props);

        this.state = {
            resources: [],
            pop: 0,
            popCap: 0,
        };
    }

    update(state) {
        this.setState(state);
    }

    render() {
        const { resources, pop, popCap } = this.state;

        return (
            <div className="resources_popcap" style={this.getContainerStyle()}>
                {resources.map(this.renderResource.bind(this))}
                <p style={{ position: 'absolute', marginLeft: 30 * getScaleFactor(), bottom: 0, 
                    marginBottom: 23 * getScaleFactor(), fontSize: 10 * getScaleFactor(), fontWeight: 'bold', color: 'white' }}>{`${pop} / ${popCap}`}</p>
            </div>
        );
    }

    getContainerStyle() {
        const scaleFactor = getScaleFactor();

        return Object.assign({}, super.getContainerStyle(), {
            paddingLeft: 20 * scaleFactor,
            paddingTop: 5 * scaleFactor,
        });
    }

    /**
     * Render resource line.
     * 
     * @param {object} resource
     */
    renderResource(resource) {
        if (!resource.name) {
            return;
        }
        const scaleFactor = getScaleFactor();
        return <div key={resource.name} style={{ marginLeft: 10 * scaleFactor, marginTop: 12 * scaleFactor, fontSize: 10 * scaleFactor, display: 'block' }}>
            <ResourceLine resource={resource} scaleFactor={scaleFactor}/>
        </div>;
    }
}
