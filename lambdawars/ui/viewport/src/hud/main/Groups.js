import React from 'react';
import BaseScalableHudElement from './BaseScalableHudElement';
import { getScaleFactor } from '../common/scaling';
import AbilityIcon from '../common/AbilityIcon'

/**
 * Component defining the groups hud.
 * This displays a row of "unit group" buttons.
 * Players bind a group of units to a number and can quickly select the group
 * using a hotkey, or like here through the UI by pressing the button.
 */
export default class Groups extends BaseScalableHudElement {
    constructor(props) {
        super(props);

        this.state = {
            groups: [],
        };
    }

    updateGroups(groups) {
        this.setState({
            groups: groups,
        });
    }

    updateGroup(groupNum, groupInfo) {
        const { groups } = this.state;

        if (groupNum < groups.length) {
            groups[groupNum] = groupInfo
        }

        this.setState({
            groups: groups,
        });
    }

    render() {
        const { groups } = this.state;

        return (
            <div className="groups" style={this.getContainerStyle()}>
                { groups.map(this.renderGroup.bind(this)) }
            </div>
        );
    }

    renderGroup(groupInfo, idx) {
        return groupInfo.unitInfo ? this.renderActiveGroup(groupInfo, idx) : this.renderPlaceholderButton(idx);
        
    }

    renderActiveGroup(groupInfo, idx) {
        return <div 
            className="group_holder group_button" 
            key={idx}
            style={{ width: `${this.calcButtonWidth()}px` }}
            onClick={() => this.selectGroup(idx)}
            onContextMenu={() => this.selectGroup(idx)}
            onDoubleClick={() => this.jumpToGroup(idx)}
        >
            <span className="index">{idx + 1}</span>
            <AbilityIcon info={groupInfo.unitInfo}></AbilityIcon>
        </div>;
    }

    renderPlaceholderButton(idx) {
        return <div 
            key={idx}
            className="group_holder"
            style={{ width: `${this.calcButtonWidth()}px`}}></div>
    }

    calcButtonWidth() {
        return getScaleFactor() * this.props.height * 0.90;
    }

    selectGroup(groupIdx) {
        this.gameApi.selectGroup(groupIdx);
    }

    jumpToGroup(groupIdx) {
        this.gameApi.jumpToGroup(groupIdx);
    }
}
