import React from 'react';
import WarsComponent from '../WarsComponent';
import { HudThemeContext } from './HudThemeContext';
import { getScaleFactor, getAspectRatio } from './common/scaling';

import { 
    Abilities,
    Minimap,
    Units,
    Resources,
    PopCap,
    Notifier,
    Groups,
} from './main';

import './rebels.scss';

/**
 * Component defining the Rebels faction HUD.
 */
export default class RebelsHud extends WarsComponent {
    constructor(props) {
        super(props);

        this.state = {
        };
    }

    componentDidMount() {
        super.componentDidMount();
    }

    componentWillUnmount() {
        super.componentWillUnmount();
    }

    render() {
        const minimapWidth = 158;
        const abilitiesWidth = 152;
        let unitsRight, unitsWide, unitsTall;
        const aspect = getAspectRatio();
        const scaleFactor = getScaleFactor();

        // The units selection hud is adjusted based on the aspect ratio
        if (aspect > 1.7) {
            // Glorious wide screen master race
            unitsWide = 341;
            unitsTall = 90;
        } else if (aspect >= 1.5) {
            // Glorious wide screen master race with some extra height
            unitsWide = 255;
            unitsTall = 90;
        } else {
            // 4:3 suckers
            unitsWide = 195;
            unitsTall = 90;
        }
        unitsRight = abilitiesWidth;

        const theme = {
            color: '#e6991a',

            style: {
                units: {
                    padding: 5 * scaleFactor,
                },
            },
        };

        return (
            <HudThemeContext.Provider value={theme}>
                <div className="rebels_hud">
                    <Groups width={unitsWide} height={30} right={abilitiesWidth}  bottom={unitsTall} name='groups' />
                    <Minimap width={minimapWidth} height={144} left={0} bottom={0} name="minimap" />
                    <PopCap width={133} height={41} left={minimapWidth} bottom={0} name="population" />
                    <Resources width={85} height={73} left={minimapWidth} bottom={41} name="resources" />
                    <Units width={unitsWide} height={unitsTall} right={unitsRight} bottom={0} name="units" />
                    <Abilities width={abilitiesWidth} height={135} right={0} bottom={0} name="abilities" />
                    <Notifier width={350} height={100} left={25} top={100} name='notifier' />
                </div>
            </HudThemeContext.Provider>
        );
    }
}
