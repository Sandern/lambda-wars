import React from 'react';
import { Component, StrictMode } from 'react';
import { registry } from './component_register';
import { IntlProvider } from 'react-intl';

/**
 * Represents the viewport.
 * Manages components inserted by game code.
 */
export default class Viewport extends Component {
    constructor(props) {
        super(props);

        this.state = {
            components: new Map(),
        };
    }

    componentDidMount() {
        // These are called globally from game code
        // There should only be one Viewport component for this code.
        window.insertElement = this.insertComponent.bind(this);
        window.removeElement = this.removeComponent.bind(this);
    }

    componentWillUnmount() {
        delete window['insertElement'];
        delete window['removeElement'];
    }

    /**
     * Called from game code to insert a new component into the viewport.
     * 
     * @param {string} componentName component name matching with one in component_register.js
     * @param {string} name 
     * @param {object} config configuration object from game code for component
     */
    insertComponent(componentName, name, config) {
        console.log(`Adding ${componentName} ${name} ${config}`);

        // Use name as key, which should be unique within the viewport
        config.key = name;

        const components = this.state.components;
        components.set(name, { componentName: componentName, name: name, config: config });

        this.setState({
            components: components
        });
    }

    /**
     * Called from game code to remove a component from the viewport.
     * 
     * @param {string} name 
     */
    removeComponent(name) {
        console.log('Removing ' + name);

        const components = this.state.components;
        components.delete(name);

        this.setState({
            components: components
        });
    }

    render() {
        const comps = [];

        for (const compDef of this.state.components.values()) {
            const ComponentRef = registry[compDef.componentName];
            if (!ComponentRef) {
                console.warn(`Missing viewport component for ${compDef.componentName}`);
                continue;
            }
            comps.push(React.createElement(ComponentRef, compDef.config));
        }

        return (
            <StrictMode>
                <IntlProvider locale="en" messages={this.props.translations}>
                    <div className="App">
                        {comps}
                    </div>
                </IntlProvider>
            </StrictMode>
        );
    }
}
