import React from 'react';
import { Component } from 'react';
import { Rnd } from 'react-rnd';
import ReactDataGrid from 'react-data-grid';
import { ChromePicker } from 'react-color';

import { Space, Checkbox, Slider } from 'antd';

const columns = [
    {
      key: 'model',
      name: 'Model'
    }
];

/**
 * Panel that allows selecting a model for flora
 */
export default class FloraListPanel extends Component {
	constructor(props) {
		super(props);

		this.state = {
            width: 500,
            height: 500,

            placeOnNavMesh: true,
            ignoreClips: false,

            selectedPaths: [],
            selectedIndexes: [],

            // Expanded folder paths
            expanded: {},
            // Displayed rows
            rows: [],
            // Cached sub folder contents, retrieved upon expanding a folder.
            // Getting everything at once would cause a big perf spike.
            subFolderModels: {},
		};
	}

    componentDidMount() {
        this.props.gameApi.listModels('models/', (records) => {
            this.setState({
                rows: records,
            });
        });
    }

	render() {
        const { width, height, placeOnNavMesh, ignoreClips } = this.state;

		const windowStyle = {
			display: 'flex',
			justifyContent: 'start',
			flexDirection: 'column',
			border: 'solid 1px #ddd',
		};

		return (<Rnd
            dragHandleClassName="windowHandle"
            default={{
                x: 600,
                y: 100,
                width: width,
                height: height,
            }}
            style={windowStyle}
            className="card"
            minWidth={500}
            minHeight={500} 
            maxWidth={2000}
            maxHeight={2000}
            onResize={this.onResize.bind(this)}
        >
            <div className="windowHandle"></div>
            <div className="card-content">
                <Space direction="vertical">
                    <div>
                        <span>Density</span>
                        <Slider 
						min={0.0}
						max={5.0}
                        defaultValue={1.0}
                        step={0.1}
                        onChange={this.onFloraDensityChanged.bind(this)}
                        />
                    </div>
                    <Checkbox checked={placeOnNavMesh} onChange={this.onPlaceOnNavMeshChanged.bind(this)}>Place on Nav Mesh</Checkbox>
                    <Checkbox checked={ignoreClips} onChange={this.onIgnoreClipsChanged.bind(this)}>Ignore Clips</Checkbox>

                    <ChromePicker color={'#ffffff'} onChangeComplete={this.onColorChanged.bind(this)} />
                </Space>

                <div style={{ float: 'right' }}>
                    <ReactDataGrid
                        minHeight={height * 0.9}
                        maxHeight={height * 0.9}
                        minWidth={width - 270}
                        maxWidth={width - 270}
                        rowKey={'fullPath'}
                        enableCellSelect={true}
                        rowSelection={{ 
                            showCheckbox: true, 
                            enableShiftSelect: true,
                            onRowsSelected: this.onRowsSelected.bind(this),
                            onRowsDeselected: this.onRowsDeselected.bind(this),
                            selectBy: {
                                indexes: this.state.selectedIndexes
                            }
                        }}
                        columns={columns}
                        rowGetter={this.getRows.bind(this)}
                        rowsCount={this.state.rows.length}
                        getSubRowDetails={this.getSubRowDetails.bind(this)}
                        onCellExpand={this.onCellExpand.bind(this)}
                        onAddSubRow={this.onAddSubRow.bind(this)} 
                    />
                </div>
            </div>
		</Rnd>);
    }

	/**
	 * Called when window is resized.
	 * We keep track of it to resize the data grid as well.
	 * 
	 * @param {*} e 
	 * @param {*} direction 
	 * @param {*} ref 
	 */
    onResize(e, direction, ref/*, delta, position*/) {
        this.setState({
            width: ref.offsetWidth,
            height: ref.offsetHeight,
        });
    }

    /**
     * Called when the density slider changed.
     * Tell game code this value changed to update the place flora ability.
     * 
     * @param {*} changedValue 
     */
    onFloraDensityChanged(changedValue) {
        this.props.gameApi.setPlaceToolDensity(changedValue);
    }

    /**
     * Handle checkbox changing for "place on nav mesh"
     * @param {*} e 
     */
    onPlaceOnNavMeshChanged(e) {
        this.setState({ placeOnNavMesh: e.target.checked });
        this.props.gameApi.setPlaceToolPlaceOnNavMesh(e.target.checked);
    }
    
    /**
     * Handle checkbox changing for "ignore clips"
     * @param {*} e 
     */
    onIgnoreClipsChanged(e) {
        this.setState({ ignoreClips: e.target.checked });
        this.props.gameApi.setPlaceToolAttribute('ignoreclips', e.target.checked);
    }

    /**
     * Handles changes for color picker.
     */
    onColorChanged(colorResult) {
        this.props.gameApi.setPlaceToolAttribute('color', `${colorResult.rgb.r} ${colorResult.rgb.g} ${colorResult.rgb.b}`);
    }

    /**
     * Returns rows for model list.
     * 
     * @param {*} i 
     */
    getRows(i) {
        return this.state.rows[i];
    }

    /**
     * Returns sub row information if any.
     * 
     * @param {*} rowItem 
     */
    getSubRowDetails(rowItem) {
        const children = rowItem.children || (rowItem.isFolder ? [{}] : []);
        const isExpanded = this.state.expanded[rowItem.fullPath] ? this.state.expanded[rowItem.fullPath] : false;
        return {
          group: children && children.length > 0,
          expanded: isExpanded,
          // Just add dummy data to make react-data-grid think it has children
          children: children,
          field: 'model',
          treeDepth: rowItem.treeDepth || 0,
          siblingIndex: rowItem.siblingIndex,
          numberSiblings: rowItem.numberSiblings
        };
    }
    
    /**
     * Expands a cell.
     * @param {*} args 
     */
    onCellExpand(args) {
        const cachedRecords = this.state.subFolderModels[args.rowData.fullPath];

        if (cachedRecords) {
            // Expand the cell with the cached records
            this.processExpandCell(args, cachedRecords);
        } else {
            this.props.gameApi.listModels(args.rowData.fullPath, (records) => {
                // Cache the parse records
                const subFolderModels = Object.assign({}, this.state.subFolderModels);
                subFolderModels[args.rowData.fullPath] = records;
                this.setState({
                    subFolderModels: subFolderModels,
                });

                // Expand the cell
                this.processExpandCell(args, records);
            });
        }
    }

    /**
     * Does the real expanding of a cell.
     * 
     * @param {*} args 
     * @param {*} records 
     */
    processExpandCell(args, records) {
        let rows = this.state.rows.slice(0);
        let rowKey = args.rowData.fullPath;
        let rowIndex = rows.indexOf(args.rowData);
        let subRows = records;

        // Ensure rows has the right children at this point
        rows[rowIndex].children = records;
    
        let expanded = Object.assign({}, this.state.expanded);
        if (expanded && !expanded[rowKey]) {
            expanded[rowKey] = true;
            this.updateSubRowDetails(subRows, args.rowData.treeDepth);
            rows.splice(rowIndex + 1, 0, ...subRows);
        } else if (expanded[rowKey]) {
            expanded[rowKey] = false;
            rows.splice(rowIndex + 1, subRows.length);
        }
    
        this.setState({ expanded: expanded, rows: rows });
    }
    
    updateSubRowDetails(subRows, parentTreeDepth) {
        let treeDepth = parentTreeDepth || 0;
        subRows.forEach((sr, i) => {
            sr.treeDepth = treeDepth + 1;
            sr.siblingIndex = i;
            sr.numberSiblings = subRows.length;
        });
    }
    
    onAddSubRow() {
    }

    onRowsSelected(rows) {
        rows = rows.filter(r => !r.row.isFolder);
        const newSelectedPaths = this.state.selectedPaths.concat(
            rows.map(r => r.row.fullPath)
        );
        this.setState({
            selectedIndexes: this.state.selectedIndexes.concat(
                rows.map(r => r.rowIdx)
            ),
            selectedPaths: newSelectedPaths,
        });
        this.props.gameApi.setPlaceToolAssets(newSelectedPaths);
    }
    
    onRowsDeselected(rows) {
        const rowIndexes = rows.map(r => r.rowIdx);
        const rowPaths = rows.map(r => r.row.fullPath);
        const newSelectedPaths = this.state.selectedPaths.filter(
            p => rowPaths.indexOf(p) === -1
        );
        this.setState({
            selectedIndexes: this.state.selectedIndexes.filter(
                i => rowIndexes.indexOf(i) === -1
            ),
            selectedPaths: newSelectedPaths,
        });
        this.props.gameApi.setPlaceToolAssets(newSelectedPaths);
    }

}
