import React from 'react';
import { Component } from 'react';
import { Rnd } from 'react-rnd';
import ReactDataGrid from 'react-data-grid';
import update from 'immutability-helper';

const columns = [
	{
		key: 'attribute',
		name: 'Attribute'
	},
	{
		key: 'value',
		name: 'Value',
		editable: true
	}
];

/**
 * Panel that allows modifying properties of the selected flora props.
 */
export default class FloraPropertiesPanel extends Component {
	constructor(props) {
		super(props);

		this.state = {
			width: 500,
			height: 500,
			
			rows: [],
		};
	}

	componentDidMount() {
		this.refreshProperties();
	}

	componentDidUpdate(prevProps/*, prevState, snapshot*/) {
		if (prevProps.editorSelection !== this.props.editorSelection) {
			this.refreshProperties();
		}
	}

	refreshProperties() {
		this.props.gameApi.getProperties((entries) => {
			this.setState({
				rows: Object.values(entries)
			});
		});
	}

	render() {
		const { width, height } = this.state;
		const { editorSelection } = this.props;

		const numSelected = editorSelection.length;

		const windowStyle = {
			display: 'flex',
			justifyContent: 'start',
			flexDirection: 'column',
			border: 'solid 1px #ddd',
		};

		return (<Rnd
			dragHandleClassName="windowHandle"
			default={{
				x: 600,
				y: 100,
				width: width,
				height: height,
			}}
			style={windowStyle}
			className="card"
			minWidth={500}
			minHeight={500} 
			maxWidth={2000}
			maxHeight={2000}
			onResize={this.onResize.bind(this)}
		>
			<div className="windowHandle">{ numSelected ? `Selected ${numSelected} flora entities` : 'No flora entities selected' }</div>
			<div className="card-content">
				<ReactDataGrid
					minHeight={height * 0.9}
					maxHeight={height * 0.9}
					minWidth={width * 0.9}
					maxWidth={width * 0.9}
					rowKey={'attribute'}
					enableCellSelect={true}
					columns={columns}
					rowGetter={this.getRows.bind(this)}
					rowsCount={this.state.rows.length}
					onGridRowsUpdated={this.handleGridRowsUpdated.bind(this)}
				/>
			</div>
		</Rnd>);
	}

	/**
	 * Called when window is resized.
	 * We keep track of it to resize the data grid as well.
	 * 
	 * @param {*} e 
	 * @param {*} direction 
	 * @param {*} ref 
	 */
	onResize(e, direction, ref/*, delta, position*/) {
		this.setState({
			width: ref.offsetWidth,
			height: ref.offsetHeight,
		});
	}

	/**
	 * Returns rows for model list.
	 * 
	 * @param {*} i 
	 */
	getRows(i) {
		return this.state.rows[i];
	}

	/**
	 * Update on rows edited
	 */
	handleGridRowsUpdated({fromRow, toRow, updated}) {
		let rows = this.state.rows.slice();

		for (let i = fromRow; i <= toRow; i++) {
			let rowToUpdate = rows[i];
			let updatedRow = update(rowToUpdate, {$merge: updated});

			this.props.gameApi.applyProperties([[updatedRow.attribute, updatedRow.value]]);

			rows[i] = updatedRow;
		}
	
		this.setState({ rows: rows });
	}
}
