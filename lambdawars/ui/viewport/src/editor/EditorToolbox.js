import React from 'react';
import WarsComponent from '../WarsComponent';

import NavMeshPanel from './NavMeshPanel';
import FloraListPanel from './FloraListPanel';
import FloraPropertiesPanel from './FloraPropertiesPanel';

import './toolbox.scss';

import spacerImg from '../../images/flora_editor/spacer.png';
import floraEditorBg from '../../images/flora_editor/box_bg.png';
import floraEditorBgSelected from '../../images/flora_editor/box_bg_select.png';

import floraSelectIcon from '../../images/flora_editor/select_tool.png';
import floraTransformIcon from '../../images/flora_editor/transform_tool.png';
import floraRotateIcon from '../../images/flora_editor/rotate_tool.png';

import floraOpenIcon from '../../images/flora_editor/open_flora_tool.png';
import floraOpenPropertiesIcon from '../../images/flora_editor/open_edit_tool.png';

import navOpenIcon from '../../images/flora_editor/open_nav_tool.png';
import navOpenCoverIcon from '../../images/flora_editor/open_cover_tool.png';

import saveBtnIcon from '../../images/flora_editor/save_flora_tool.png';

/**
 * Toolsbox ingame editor.
 * 
 * This renders the vertical toolstrip with buttons and manages
 * which tool is active.
 */
export default class EditorToolbox extends WarsComponent {
    constructor(props) {
        super(props);
        
        this.state = {
            activeMode: 'select',
            showFloraProperties: false,
            editorSelection: [],
        };
    }

    /**
     * Called by UI code to make a new tool active.
     */
    setActiveMode(mode) {
        // Update our component with new state
        this.setState({
            activeMode: mode,
        });
        // Inform game code of the new state
        this.gameApi.setActiveMode(mode);
    }

    /**
     * Called by game code when the editor selection changes.
     */
    editorSelectionChanged(editorSelection) {
        this.setState({
            editorSelection: editorSelection
        });
    }

    render() {
        const { activeMode, showFloraProperties, editorSelection } = this.state;

        return(<div>
            <div id="editor_toolbox">
            <span id="editor_toolbox_mapname"></span>
            <div id="editor_toolbox_select_div">
                <img 
                    id="editor_toolbox_select" 
                    src={activeMode === 'select' ? floraEditorBgSelected : floraEditorBg} 
                    title="Select" alt="Select"
                    onClick={() => this.clickSelectTool()}
                />
                <img id="editor_toolbox_select_icon" src={floraSelectIcon} alt="Select"/>
            </div>
            <div id="editor_toolbox_transform_div">
                <img 
                    id="editor_toolbox_transform"  src={activeMode === 'translate' ? floraEditorBgSelected : floraEditorBg} 
                    title="Transform" alt="transform"
                    onClick={() => this.clickTranslateTool()}
                />
                <img id="editor_toolbox_transform_icon" src={floraTransformIcon} alt="transform"/>
            </div>
                
            <div id="editor_toolbox_rotate_div">
                <img 
                    id="editor_toolbox_rotate" 
                    src={activeMode === 'rotate' ? floraEditorBgSelected : floraEditorBg}
                    title="Rotate" alt="Rotate"
                    onClick={() => this.clickRotateTool()}
                />
                <img id="editor_toolbox_rotate_icon" src={floraRotateIcon} alt="Rotate"/>
            </div>
            
            <img  className="spacer" src={spacerImg} />
            
            <div id="editor_toolbox_flora_div">
                <img 
                    id="editor_toolbox_flora" 
                    src={activeMode === 'flora' ? floraEditorBgSelected : floraEditorBg} 
                    title="Open Flora List" alt="flora"
                    onClick={() => this.clickOpenFloraTool()}
                />
                <img id="editor_toolbox_flora_icon" src={floraOpenIcon} alt="flora"/>
            </div>
            
            <div id="editor_toolbox_properties_div">
                <img 
                    id="editor_toolbox_properties" 
                    src={floraEditorBg} 
                    title="Show Properties" alt="properties"
                    onClick={() => this.clickToggleShowFloraProperties()}
                />
                <img id="editor_toolbox_properties_icon" src={floraOpenPropertiesIcon} alt="flora"/>
            </div>
            
            <img  className="spacer" src={spacerImg} />

            <div id="editor_toolbox_navmesh_div">
                <img 
                    id="editor_toolbox_navmesh" 
                    src={activeMode === 'navmesh' ? floraEditorBgSelected : floraEditorBg} 
                    title="Open Nav Mesh Tool" alt="navmesh"
                    onClick={() => this.clickOpenNavMeshTool()}
                />
                <img id="editor_toolbox_navmesh_icon" src={navOpenIcon} alt="navmesh"/>
            </div>
            
            <div id="editor_toolbox_cover_div">
                <img 
                    id="editor_toolbox_cover" 
                    src={activeMode === 'cover' ? floraEditorBgSelected : floraEditorBg} 
                    title="Open Cover Tool" alt="cover"
                    onClick={() => this.clickOpenCoverTool()}
                />
                <img id="editor_toolbox_cover_icon" src={navOpenCoverIcon} alt="cover"/>
            </div>
            
            <img  className="spacer" src={spacerImg} />
            
            <div id="editor_toolbox_savemap_div">
                <img 
                    id="editor_toolbox_savemap" 
                    src={floraEditorBg} 
                    title="Save Map" alt="save"
                    onClick={() => this.clickSave()}
                />
                <img id="editor_toolbox_savemap_icon" src={saveBtnIcon} alt="save"/>
            </div>
            
            </div>

            {activeMode === 'flora' ? <FloraListPanel gameApi={this.gameApi} /> : null}

            {showFloraProperties ? <FloraPropertiesPanel gameApi={this.gameApi} editorSelection={editorSelection} /> : null}

            {activeMode === 'navmesh' ? <NavMeshPanel gameApi={this.gameApi} /> : null}
        </div>);
    }

    clickSelectTool() {
        this.setActiveMode('select');
    }

    clickTranslateTool() {
        this.setActiveMode('translate');
    }

    clickRotateTool() {
        this.setActiveMode('rotate');
    }

    /**
     * Activates flora place tool and shows the flora panel with models to select.
     */
    clickOpenFloraTool() {
        this.setActiveMode('flora');
        window.interface.serverCommand('wars_abi editor_tool_flora');
    }

    /**
     * Toogle showing flora properties panel.
     */
    clickToggleShowFloraProperties() {
        this.setState({
            showFloraProperties: !this.state.showFloraProperties,
        });
    }

    /**
     * Open the navigation mesh tool.
     */
    clickOpenNavMeshTool() {
        this.setActiveMode('navmesh');
    }

    /**
     * Activate the place cover tool.
     */
    clickOpenCoverTool() {
        this.setActiveMode('cover');
        window.interface.serverCommand('wars_abi editor_tool_cover');
    }

    /**
     * Save changes to .wars file.
     */
    clickSave() {
        window.interface.serverCommand('wars_editor_save');
    }
}