import React from 'react';
import { Component } from 'react';
import { Rnd } from 'react-rnd';

import { Space, Button, Checkbox, Menu, Dropdown, InputNumber, Typography } from 'antd';

/**
 * Panel that allows building the navigation mesh,
 * tweak the settings and visualize the mesh.
 */
export default class NavMeshPanel extends Component {
	constructor(props) {
		super(props);

		this.state = {
			meshName: '',
			draw_navmesh: false,
			draw_server: false,
			cellsize: -1,
			cellheight: -1,
			tilesize: -1
		};
	}

    componentDidMount() {
		this.updateMeshSettings('');
	}
	
	updateMeshSettings(meshName) {
		this.props.gameApi.getMeshSettings(meshName, (settings) => {
			this.setState({
				meshName: settings.debug_mesh,
				draw_navmesh: settings.draw_navmesh,
				draw_server: settings.draw_server,
				cellsize: settings.cellsize,
				cellheight: settings.cellheight,
				tilesize: settings.tilesize,
			});
		});
	}

	render() {
		const { meshName, draw_navmesh, draw_server, cellsize, cellheight, tilesize } = this.state;

		const windowStyle = {
			display: 'flex',
			justifyContent: 'start',
			flexDirection: 'column',
			border: 'solid 1px #ddd',
		};

		return (
			<Rnd
				dragHandleClassName="windowHandle"
				default={{
					x: 100,
					y: 100,
					width: 200,
					height: 400,
				}}
				style={windowStyle}
				className="card"
				minWidth={200}
				minHeight={400} 
				maxWidth={2000}
				maxHeight={2000}
			>
				<div className="windowHandle"></div>
				<div className="card-content">
					<Space direction="vertical">
						<Button type="primary" onClick={this.buildNavMesh.bind(this)}>Build</Button>

						<Dropdown
							id={meshName}
							overlay={this.renderMeshSelectionMenu()}
						><span>{meshName}</span></Dropdown>

						<Checkbox checked={draw_navmesh} onChange={this.onDrawNavMeshChanged.bind(this)}>Draw Nav Mesh</Checkbox>
						<Checkbox checked={draw_server} onChange={this.onServerNavMeshChanged.bind(this)}>Server Nav Mesh</Checkbox>

						<Typography.Text>Cell size:</Typography.Text>
						<InputNumber
							value={cellsize}
							onChange={this.onCellSizeChanged.bind(this)}
						></InputNumber>

						<Typography.Text>Cell height:</Typography.Text>
						<InputNumber
							value={cellheight}
							onChange={this.meshSetCellHeight.bind(this)}
						></InputNumber>

						<Typography.Text>Tile size:</Typography.Text>
						<InputNumber
							value={tilesize}
							onChange={this.onTileSizeChanged.bind(this)}
						></InputNumber>
					</Space>
				</div>
			</Rnd>
		);
	}

	buildNavMesh() {
		window.interface.serverCommand('recast_build\n');
	}

	renderMeshSelectionMenu() {
		return (
			<Menu onClick={this.onSelectMesh.bind(this)}>
				{this.props.meshes.map((item) => (<Menu.Item key={item.value}>{item.value}</Menu.Item>))}
			</Menu>
		);
	}

	onSelectMesh(item) {
		this.setState({
			meshName: item.key,
		});
		window.interface.clientCommand(`recast_debug_mesh ${item.key}\n`);
		this.updateMeshSettings(item.key);
	}

	onDrawNavMeshChanged(e) {
		this.setState({ draw_navmesh: e.target.checked });
		window.interface.clientCommand(`recast_draw_navmesh ${e.target.checked ? '1' : '0'}`);
	}

	onServerNavMeshChanged(e) {
		this.setState({ draw_server: e.target.checked });
		window.interface.clientCommand(`recast_draw_server ${e.target.checked ? '1' : '0'}`);
	}

	onCellSizeChanged(value) {
		this.setState({ cellsize: value });
		this.props.gameApi.meshSetCellSize(this.state.meshName, value);
	}

	meshSetCellHeight(value) {
		this.setState({ cellheight: value });
		this.props.gameApi.meshSetCellHeight(this.state.meshName, value);
	}

	onTileSizeChanged(value) {
		this.setState({ tilesize: value });
		this.props.gameApi.meshSetTileSize(this.state.meshName, value);
	}
}

NavMeshPanel.defaultProps = {
	meshes: [
		{ label : 'human', value : 'human' },
		{ label : 'medium', value : 'medium' },
		{ label : 'large', value : 'large' },
		{ label : 'verylarge', value : 'verylarge' },
		{ label : 'air', value : 'air' },
	]
};
