import { HeaderBar } from './core/header';
import { Chat } from './core/chat';
import { PlayerStatusPanel, WaitingForPlayersPanel, PostGamePanel, MessageBox, Objectives, Overrun } from './core';
import { ToolsUnitPanel, ToolsAbilityPanel, ToolsAttributePanel } from './sandbox';
import { RebelsHud, CombineHud } from './hud';
import { EditorToolbox } from './editor';

export const registry = {
    // Header bar
    HeaderBar,

    // Multiplayer related
    Chat,
    PlayerStatusPanel,
    WaitingForPlayersPanel,
    PostGamePanel,
    Overrun,

    // Hud
    RebelsHud,
    CombineHud,

    // Single Player UI
    MessageBox,
    Objectives,

    // Tools
    ToolsUnitPanel,
    ToolsAbilityPanel,
    ToolsAttributePanel,
    EditorToolbox,
};
