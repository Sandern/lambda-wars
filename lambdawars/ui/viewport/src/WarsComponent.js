import { Component } from 'react';

/**
 * All Lambda Wars components should derive from this.
 */
class WarsComponent extends Component {
    constructor(props) {
        super(props);

        this.state = {
            visible: props.visible,
        };

        // Create reference to interface to game instance of this component.
        // It's globally set based on the name.
        this.gameApi = window[`${props.name}_obj`];
    }

    componentDidMount() {
        // Sets a reference in the game code code this component, used for calling methods.
        this.gameApi.componentDidMount(this);
    }

    componentWillUnmount() {
        this.gameApi.componentWillUnmount(this);
    }

    componentDidCatch(/*error, info*/) {
        // Prevent component from crashing other viewport elements
    }

    /**
     * Called by game code to show or hide the component, without removing it from the dom.
     * @param {*} visible 
     */
    setVisible(visible) {
        this.setState({
            visible: visible,
        });
    }
}

export default WarsComponent;
