import React from 'react';
import ReactDOM from 'react-dom';
import Viewport from './Viewport';

import 'antd/dist/antd.css';
import '../sass/viewport.scss';

// This function is called by the game process after the js bindings are created
// Use this to initialize services which depend on those bindings
function init_viewport(translations) {
	ReactDOM.render(<Viewport translations={translations}/>, document.getElementById('root'));
}
	
window['init_viewport'] = init_viewport;
