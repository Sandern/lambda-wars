/**
 * Pads given string value with zeros if needed.
 * Used for displaying time.
 * 
 * @param {*} value 
 * @param {*} length 
 */
export const zeroPad = (value, length = 2) => {
    const strValue = String(value);
    return strValue.length >= length ? strValue : ('0'.repeat(length) + strValue).slice(length * -1);
};
