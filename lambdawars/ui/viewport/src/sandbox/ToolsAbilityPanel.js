import React from 'react';
import WarsComponent from '../WarsComponent';

import $ from 'jquery';
import '../../jqwidgets/jqx-all';

import './tools.scss';

const theme = 'wars';

/**
 * Ability panel.
 * 
 * TODO: convert to an actual react component. Preferably ditch jqx widgets.
 */
export default class ToolsAbilityPanel extends WarsComponent {
    constructor(props) {
        super(props);

        // Setup data
        this.abilities = props.abilities;
        
        this.source =
        {
            localdata: this.getAbilities(),
            datafields:
            [
                { name: 'name', type: 'string' },
            ],
            datatype: "array"
        };
        this.dataAdapter = new $.jqx.dataAdapter(this.source);
    }

    componentDidMount() {
        super.componentDidMount();

        this.turnInJqWidget();
    }

    componentDidUpdate() {
        this.turnInJqWidget();
    }

    componentWillUnmount() {
        super.componentWillUnmount();
    }

    turnInJqWidget() {
        const container = $('#abilitypanel_container');

        const dataAdapter = this.dataAdapter;

        // Create the window and content
        container.jqxWindow({
            showCollapseButton: true, keyboardCloseKey: 'none', minHeight: 200, minWidth: 200, maxWidth: 2000, maxHeight: 2000, height: 350, width: 200, theme: theme, autoOpen: false,
            initContent: function () {
            
                // Grid
                container.find("#abilitypanel_abilityGrid").jqxGrid(
                {
                    width: '100%',
                    height: '285px',
                    source: dataAdapter,
                    theme: theme,
                    showfilterrow: true,
                    filterable: true,
                    columns: [
                        { text: 'Name', datafield: 'name', width: '100%', filtertype: 'textbox', filtercondition: 'contains' },
                    ],
                    rowsheight : 17,
                    pagerheight : 20,
                    columnsheight : 20,
                    selectionmode : 'singlerow', //'multiplerows',
                });
                
                // Place button
                $("#abilitypanel_executeButton").jqxButton({ width: '100px', height: '20px', theme: theme});
                
                $('#abilitypanel_executeButton').bind('click', function (/*event*/) {
                    var index = $("#abilitypanel_abilityGrid").jqxGrid('getselectedrowindex');
                    var datarow = $("#abilitypanel_abilityGrid").jqxGrid('getrowdata', index);
                    window['interface'].serverCommand('wars_abi ' + datarow['name']);
                });
            }
        });

        // Use close event to update the visibility state
        container.on('close', (/*event*/) => { 
            this.gameApi.onSetVisible(false);
        });
        
        container.on('resizing', function (event) { 
            $("#abilitypanel_abilityGrid").jqxGrid('height', event.args.height - 40 - 25);
        }); 
    }

    getAbilities() {
        var data = new Array();
        
        this.abilities.sort();

        for (var i = 0; i < this.abilities.length; i++) {
            var row = {};
            
            row["name"] = this.abilities[i];
            data[i] = row;
        }

        return data;
    }

    clearList() {
        this.abilities = [];
        this.source.localdata = this.getAbilities();
        //$("#abilitypanel_abilityGrid").jqxGrid('updatebounddata');
    }

    addAbility(name) {
        this.abilities.push(name);
        this.source.localdata = this.getAbilities();
        //$("#abilitypanel_abilityGrid").jqxGrid('updatebounddata');
    }

    addAbilities(names) {
        this.abilities.push.apply(this.abilities, names)
        this.source.localdata = this.getAbilities();
        //$("#abilitypanel_abilityGrid").jqxGrid('updatebounddata');
    }

    // Listen to space key
    onSpacePressed() {
        var rowindexes = $('#abilitypanel_abilityGrid').jqxGrid('getselectedrowindexes');
        if( rowindexes.length == 0 )
        {
            console.log('ability panel execute: nothing selected');
            return;
        }
        var index = rowindexes[Math.floor(Math.random()*rowindexes.length)];
        var datarow = $("#abilitypanel_abilityGrid").jqxGrid('getrowdata', index);

        window['interface'].serverCommand('wars_abi ' + datarow['name']);
    }

    setVisible(state) {
        const container = $('#abilitypanel_container');

        if( container.is(":visible") == state )
            return;
            
        if( state ) {
            container.jqxWindow('open');
        } else {
            container.jqxWindow('close');
        }
    }
    
    render() {
        return (
            <div id="abilitypanel_container">
                <div id="windowHeader">
                    <span>
                        Abilities
                    </span>
                </div>
                <div id="windowContent">
                    <div id="abilitypanel_abilityGrid">
                    </div>
                    <div id="abilitypanel_footer">
                        <div id='abilitypanel_placeButtonCon'><input type="button" value="Execute" id='abilitypanel_executeButton'/></div>
                    </div>
                </div>
            </div>
        );
    }
}
