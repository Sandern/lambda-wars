import React from 'react';
import WarsComponent from '../WarsComponent';

import $ from 'jquery';
import '../../jqwidgets/jqx-all';

import './tools.scss';

const theme = 'wars';

/**
 * Attribute panel.
 * 
 * TODO: convert to an actual react component. Preferably ditch jqx widgets.
 */
export default class ToolsAttributePanel extends WarsComponent {
    constructor(props) {
        super(props);

        this.grids = {};

		// Setup data
		this.abilities = this.props.abilities;
		
		// Create window
		this.source =
		{
			localdata: this.abilities,
			datafields:
			[
				{ name: 'name', type: 'string' },
			],
			datatype: "array"
		};
		this.dataAdapter = new $.jqx.dataAdapter(this.source);
		
		// List of tabs with grid
		// Can get the grid element by appending the name to "attributepanel_grid"
		this.gridNames = [
			'Info',
			'Class',
			'Instance'
		];
    }

    componentDidMount() {
        super.componentDidMount();

        this.turnInJqWidget();
    }

    componentDidUpdate() {
        this.turnInJqWidget();
    }

    componentWillUnmount() {
        super.componentWillUnmount();
    }

    turnInJqWidget() {
        const container = $('#attributepanel_container');

        const dataAdapter = this.dataAdapter;

        const element = this;

		// Create the window and content
		container.jqxWindow({
			showCollapseButton: true, keyboardCloseKey: 'none', minHeight: 200, minWidth: 200, maxWidth: 2000, maxHeight: 2000, height: 500, width: 700, theme: theme, autoOpen: false,
			initContent: function () {
				// Splitter
				container.find('#attributepanel_contentSplitter').jqxSplitter({ theme: theme, width: '100%', height: '100%', panels: [{ size: '25%', min: 100 }, {min: 200, size: '75%'}] });
				
				// Grid
				container.find("#attributepanel_grid").jqxGrid(
				{
					width: '100%',
					height: '100%',
					source: dataAdapter,
					theme: theme,
					sortable: true,
					showfilterrow: true,
					filterable: true,
					columns: [
						{ text: 'Name', datafield: 'name', width: '100%', filtertype: 'textbox', filtercondition: 'contains' },
					],
					rowsheight : 17,
					pagerheight : 20,
					columnsheight : 20,
					selectionmode : 'singlerow',
				});
				
				// Attribute tabs
				container.find("#attributepanel_contentTabs").jqxTabs({ theme: theme, animationType: 'fade', height: '100%' });
				
				// Create attribute grids for each tab
				for (var i = 0; i < element.gridNames.length; i++) {
					var gridName = element.gridNames[i];
					var grid = container.find("#attributepanel_grid"+gridName);
					grid.gridname = gridName;
				
					// Create a new adapter
					var contentsource =
					{
						localdata: new Array(),
						datafields:
						[
							{ name: 'attribute', type: 'string' },
							{ name: 'value', type: 'string' },
						],
						datatype: "array"
					};
					var adapter = new $.jqx.dataAdapter(contentsource);
					adapter.attributes = {} // Map for each access to entries
					adapter.contentsource = contentsource; // Not sure which property it gets set to, so just set my own...
					adapter.refreshtimeout = null;
					
					if( element.gridNames[i] == 'Info' )
					{
						adapter.getattributescmd = 'abiinfo_requestall'
						adapter.setattributecmd = 'abiinfo_setattr'
					}
					else if( element.gridNames[i] == 'Class' )
					{
						adapter.getattributescmd = 'classinfo_requestall'
						adapter.setattributecmd = 'classinfo_setattr'
					}
					else
					{
						adapter.getattributescmd = '';
						adapter.setattributecmd = '';
					}

					grid.jqxGrid(
					{
						width: '100%',
						height: '100%',
						source: adapter,
						theme: theme,
						sortable: true,
						showfilterrow: false,
						filterable: true,
						editable: true,
						columns: [
							{ text: 'Attribute', datafield: 'attribute', width: '50%', filtertype: 'textbox', filtercondition: 'contains', editable: false },
							{ text: 'Value', datafield: 'value', width: '50%', filterable: false },
						],
					});
					
					// Respond to cell edit changes
					grid.on('cellendedit', function (event) {
						var args = event.args;
						var rowIndex = args.rowindex;
						var cellValue = args.value;
						var oldValue = args.oldvalue;
						var grid = event.args.owner;
						
						if( cellValue == oldValue )
							return;
						
						var abirowindex = $('#attributepanel_grid').jqxGrid('getselectedrowindex');
						var datarow = $("#attributepanel_grid").jqxGrid('getrowdata', abirowindex);
						
						var adapter = grid.source;
						if( adapter.setattributecmd == '' )
							return;
						
						const attribute = adapter.contentsource.localdata[rowIndex]['attribute']
						window['interface'].serverCommand(adapter.setattributecmd + ' ' + datarow['name'] + ' ' + attribute + ' "' + cellValue + '"\n')
					});
					
					grid.jqxGrid('refresh');
					
					element.grids[gridName] = grid;
				}
				
				// Load data on select
				$('#attributepanel_grid').on('rowselect', function (event) {
					var args = event.args; 
					var row = args.rowindex;
					var datarow = $("#attributepanel_grid").jqxGrid('getrowdata', row);
					
					for (var i = 0; i < element.gridNames.length; i++) {
						var gridName = element.gridNames[i];
						var grid = element.grids[gridName];
						var adapter = grid.jqxGrid('source');
						element.clearAttrList(gridName)
						if( adapter.getattributescmd == '' )
							continue;
						window['interface'].serverCommand(adapter.getattributescmd + ' ' + datarow['name'] + ' 0\n')
					}
				});
			}
		});

        // Use close event to update the visibility state
        container.on('close', (/*event*/) => { 
            this.gameApi.onSetVisible(false);
        });
    }

    setVisible(state) {
        const container = $('#attributepanel_container');

        if( container.is(":visible") == state )
            return;
            
        if( state ) {
            container.jqxWindow('open');
        } else {
            container.jqxWindow('close');
        }
    }

    // Functions for abilities list
    clearList() {
        this.abilities = [];
        this.source.localdata = this.abilities;
    }

    addAbility(name) {
        this.abilities.push(name);
        this.source.localdata = this.abilities;
    }

    addAbilities(names) {
        this.abilities.push.apply(this.abilities, names)
        this.source.localdata = this.abilities;
    }

    // Functions for grids in tabs
    clearAttrList(tabname) {
        const container = $('#attributepanel_container');
        var grid = container.find("#attributepanel_grid"+tabname);
        var adapter = grid.jqxGrid('source');
        if( adapter === undefined )
            return;
        adapter.contentsource.localdata = [];
        if( adapter.refreshtimeout == null )
            adapter.refreshtimeout = setTimeout(this.refreshAttributeGrid, 100, this, tabname);
    }

    addAttribute(tabname, entry) {
        const container = $('#attributepanel_container');
        var grid = container.find("#attributepanel_grid"+tabname);
        var adapter = grid.jqxGrid('source');
        if( adapter === undefined )
            return;
            
        var localdata = adapter.contentsource.localdata;
        var found = false;
        for( var i = 0; i < localdata.length; i++ )
        {
            if( localdata[i]['attribute'] == entry['attribute'] )
            {
                localdata[i] = entry;
                found = true;
                break;
            }
        }
        
        if( !found )
        {
            adapter.contentsource.localdata.push(entry);
        }
        if( adapter.refreshtimeout == null )
            adapter.refreshtimeout = setTimeout(this.refreshAttributeGrid, 100, this, tabname);
    }

    addAttributes(tabname, entries) {
        for( var i = 0; i < entries.length; i++ )
        {
            this.addAttribute(tabname, entries[i]);
        }
    }

    // Delays refreshing the grid
    refreshAttributeGrid(element, tabname) {
        const container = $('#attributepanel_container');
        var grid = container.find("#attributepanel_grid"+tabname);
        var adapter = grid.jqxGrid('source');
        if( adapter === undefined )
            return;
        adapter.refreshtimeout = null;
        grid.jqxGrid({ source: adapter });
    }
    
    render() {
        return (
            <div id="attributepanel_container">
                <div id="windowHeader">
                    <span>
                        Attribute Modifier
                    </span>
                </div>
                <div id="windowContent">
                    <div id="attributepanel_contentSplitter">
                        <div id="attributepanel_gridContainer">
                            <div id="attributepanel_grid">
                            </div>
                        </div>
                        <div id="attributepanel_contentGridContainer">
                            <div id="attributepanel_contentTabs">
                                <ul>
                                    <li>Info</li>
                                    <li>Class</li>
                                    <li>Instance</li>
                                </ul>
                                <div id="attributepanel_gridInfo">
                                </div>
                                <div  id="attributepanel_gridClass">
                                </div>
                                <div  id="attributepanel_gridInstance">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
