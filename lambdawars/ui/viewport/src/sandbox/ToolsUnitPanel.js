import React from 'react';
import WarsComponent from '../WarsComponent';

import $ from 'jquery';
import '../../jqwidgets/jqx-all';

import './tools.scss';

const theme = 'wars';

/**
 * Unit panel.
 * 
 * TODO: convert to an actual react component. Preferably ditch jqx widgets.
 */
export default class ToolsUnitPanel extends WarsComponent {
    constructor(props) {
        super(props);

        this.units = props.units;

        this.source =
        {
            localdata: this.getUnits(),
            datafields:
            [
                { name: 'name', type: 'string' },
            ],
            datatype: "array"
        };
        this.dataAdapter = new $.jqx.dataAdapter(this.source);
    }

    componentDidMount() {
        super.componentDidMount();

        this.turnInJqWidget();
    }

    componentDidUpdate() {
        this.turnInJqWidget();
    }

    componentWillUnmount() {
        super.componentWillUnmount();
    }

    turnInJqWidget() {
        const container = $('#unitpanel_container');

        const dataAdapter = this.dataAdapter;

        // Create the window and content
        container.jqxWindow({
            showCollapseButton: true, keyboardCloseKey: 'none', minHeight: 200, minWidth: 200, maxWidth: 2000, maxHeight: 2000, height: 350, width: 200, theme: theme, autoOpen: false,
            initContent: function () {
                // Grid
                $(container).find("#unitpanel_unitGrid").jqxGrid(
                    {
                        width: '100%',
                        height: '285px',
                        source: dataAdapter,
                        theme: theme,
                        showfilterrow: true,
                        filterable: true,
                        columns: [
                            { text: 'Name', datafield: 'name', width: '100%', filtertype: 'textbox', filtercondition: 'contains' },
                        ],
                        rowsheight : 17,
                        pagerheight : 20,
                        columnsheight : 20,
                        selectionmode : 'singlerow', //'multiplerows',
                    });
                    
                    // Place button
                    $("#unitpanel_placeButton").jqxButton({ width: '100px', height: '20px', theme: theme});
                    
                    $('#unitpanel_placeButton').bind('click', function (/*event*/) {
                        const rowindexes = $('#unitpanel_unitGrid').jqxGrid('getselectedrowindexes');
                        if( rowindexes.length == 0 )
                        {
                            console.log('unit panel spawn: nothing selected');
                            return;
                        }
                        const index = rowindexes[Math.floor(Math.random()*rowindexes.length)];
                        const datarow = $("#unitpanel_unitGrid").jqxGrid('getrowdata', index);
                
                        const playerdata = $("#unitpanel_playersDropdown").jqxDropDownList('getSelectedItem'); 
                        let command = 'wars_abi ' + datarow['name'];
                        if( playerdata.value != " " ) {
                            command += ' owner=' + playerdata.value;
                        }
                            
                        window['interface'].serverCommand(command);
                    });
                    
                    // Dropdown players
                    const source = [
                        { label : '-', value : ' ' },
                        { label : 'n', value : '0' },
                        { label : 'e', value : '1' },
                        { label : 'p1', value : '2' },
                        { label : 'p2', value : '3' },
                        { label : 'p3', value : '4' },
                        { label : 'p4', value : '5' },
                        { label : 'p5', value : '6' },
                        { label : 'p6', value : '7' },
                        { label : 'p7', value : '8' },
                        { label : 'p8', value : '9' },
                        { label : 'p9', value : '10' },
                        { label : 'p10', value : '11' },
                        { label : 'p11', value : '12' },
                        { label : 'p12', value : '13' },
                    ];
                    // Create a jqxDropDownList
                    $("#unitpanel_playersDropdown").jqxDropDownList({ source: source, width: '50px', height: '20px', selectedIndex: 0, enableBrowserBoundsDetection: true, theme: theme});
            }
        });

        // Use close event to update the visibility state
        container.on('close', (/*event*/) => { 
            this.gameApi.onSetVisible(false);
        });
        
        container.on('resizing', function (event) { 
            $("#unitpanel_unitGrid").jqxGrid('height', event.args.height - 40 - 25);
        });
    }

    getUnits() {
        const data = new Array();
        
        this.units.sort();

        for (let i = 0; i < this.units.length; i++) {
            const row = {};
            
            row["name"] = this.units[i];
            data[i] = row;
        }

        return data;
    }

    clearList() {
        this.units = [];
        this.source.localdata = this.getUnits();
    }

    addUnit(name) {
        this.units.push(name);
        this.source.localdata = this.getUnits();
    }

    addUnits(names) {
        this.units.push.apply(this.units, names)
        this.source.localdata = this.getUnits();
    }

    // Listen to space key
    onSpacePressed() {
        var rowindexes = $('#unitpanel_unitGrid').jqxGrid('getselectedrowindexes');
        if( rowindexes.length == 0 )
        {
            console.log('unit panel spawn: nothing selected');
            return;
        }
        var index = rowindexes[Math.floor(Math.random()*rowindexes.length)];
        var datarow = $("#unitpanel_unitGrid").jqxGrid('getrowdata', index);
        
        var playerdata = $("#unitpanel_playersDropdown").jqxDropDownList('getSelectedItem'); 
        
        window['interface'].serverCommand('unit_create ' + datarow['name'] + ' ' + playerdata.value);
    }

    setVisible(state) {
        const container = $('#unitpanel_container');

        if( container.is(":visible") == state )
            return;
            
        if( state ) {
            container.jqxWindow('open');
        } else {
            container.jqxWindow('close');
        }
    }
    
    render() {
        return (
            <div id="unitpanel_container">
                <div id="windowHeader">
                    <span>
                        Units
                    </span>
                </div>
                <div id="windowContent">
                    <div id="unitpanel_unitGrid">
                    </div>
                    <div id="unitpanel_footer">
                        <div id='unitpanel_playersDropdown'></div>
                        <div id='unitpanel_placeButtonCon'><input type="button" value="Place" id='unitpanel_placeButton'/></div>
                    </div>
                </div>
            </div>
        );
    }
}
